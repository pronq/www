<?php

/* |--------------------------------------------------------------------------
  | Detect The Application Environment
  |--------------------------------------------------------------------------
  |
  | Laravel takes a dead simple approach to your application environments
  | so you can just specify a machine name for the host that matches a
  | given environment, then we will automatically detect it for you.
  |
 */
$env = $app->detectEnvironment(function() {
	$enviroment_name = '';
	$setEnv = is_file(__DIR__.'/../.environment.env') ? trim(file_get_contents(__DIR__.'/../.environment.env')) : '';
	if (isset($_SERVER['HTTP_HOST'])) {
		$host_data = parse_url("http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
		
		if (!empty($setEnv)) {
			$enviroment_name = $setEnv;
		} else {
			if (strpos($host_data['host'], 'dev') !== false) {
				$enviroment_name = 'dev';
			} else if (strpos($host_data['host'], 'test') !== false) {
				$enviroment_name = 'test';
			} else if (strpos($host_data['host'], 'stage') !== false) {
				$enviroment_name = 'stage';
			} else {
				$enviroment_name = 'live';
			}
		}
	}
	// when running artisan commands
	else {
		$enviroment_name = $setEnv;
	}

	putenv('APP_ENV='.$enviroment_name);

	
	/////////////////////////////////////////////////////////////////
	// load site env file
	if ($enviroment_name == 'dev') {
		if (file_exists(__DIR__ . "/../resources/.dev.env")) {
			Dotenv::load(__DIR__ . "/../resources/", '.dev.env');
		} else {
			Dotenv::load(__DIR__ . "/../resources/", '.test.env');
		}
	} else if (!empty($enviroment_name)) {
		Dotenv::load(__DIR__ . "/../resources/", ".$enviroment_name.env");
	}
	Dotenv::load(__DIR__ . "/../resources/", ".env");
	
	/////////////////////////////////////////////////////////////////
	// overrides
	if(isset($_GET['APP_DEBUG']) && $_GET['APP_DEBUG'] == 'true') {
		putenv('APP_DEBUG=true');
	}

});
