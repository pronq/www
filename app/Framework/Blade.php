<?php

namespace App\Framework;

use Illuminate\View\Compilers\BladeCompiler;

class Blade extends BladeCompiler
{
    /**
     * Compile the include statements into valid PHP.
     *
     * @param  string  $expression
     * @return string
     */
    protected function compileInclude($expression)
    {
		return '<?php echo Lang::parseInclude'.$expression.'; ?>';
    }
}