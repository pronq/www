<?php

namespace App\Framework;

use Illuminate\View\Factory;
use Illuminate\View\View;

class ViewFactory extends Factory
{
	/**
	 * Get the evaluated view contents for the given view.
	 *
	 * @param  string  $view
	 * @param  array   $data
	 * @param  array   $mergeData
	 * @return \Illuminate\View\View
	 */
	public function make($view, $data = array(), $mergeData = array())
	{
		if (isset($this->aliases[$view])) $view = $this->aliases[$view];

		$view = $this->normalizeName($view);

		// find the correct localized version of the blade
		
		// set up the translation arrays
		
		$path = $this->finder->find($view);

		$data = array_merge($mergeData, $this->parseData($data));

		$this->callCreator($view = new View($this, $this->getEngineFromPath($path), $view, $path, $data));

		return $view;
	}
}