<?php
namespace App\Framework;

use Lang;
use App\Facades\Resources;
use Request;
use Illuminate\Routing\UrlGenerator as UrlGeneratorParent;

class UrlGenerator extends UrlGeneratorParent
{
	///////////////// New Methods (not in parent) /////////////////

	/*
	* uses base url to get path to images directory
	*/
	public function image($path = '', $secure = null) {
        $cdn_url = env('STATIC_CDN_URL');
		if ($cdn_url) {
			return $cdn_url . 'assets/images/' . trim($path, "/");
		}		
		return $this->asset('images/' . trim($path, "/"), $secure);
	}

	/*
	* base url, non-secure by default
	*/
	public function toResource($resourceId, $ifEmptyRequestUrl = true) {
        $parameters = 'origin_url='.Request::fullUrl();
        $resource = Resources::getById($resourceId);
        if(is_array($resource) && isset($resource['expired_id'])){
            $parameters = $parameters."&id=".$resource['expired_id'];
            unset($resource['expired_id']);
        }
        $requestResourceUrl = false;
        if($ifEmptyRequestUrl){
            $requestResourceUrl = "/request-a-resource/?".$parameters;
        }
		return !empty($resource) ? $this->to($resource->getUrl()) : $requestResourceUrl;
	}

	/*
	* Like to(), but skips localization. Basically just adds the host.
	* In core Laravel, this is asset(), but we already overwrote that to always point to assets/
	* 
	*/
	public function toRaw($path) {
        
		if ($this->isValidUrl($path)) {
            return $path;
        }
		
        $scheme = $this->getScheme(null);
        $root = $this->getRootUrl($scheme);
		
        if (($queryPosition = strpos($path, '?')) !== false) {
            $query = mb_substr($path, $queryPosition);
            $path = mb_substr($path, 0, $queryPosition);
        } else {
            $query = '';
        }
		
		$base = $this->trimUrl($root, $path);
		
		// add trailing slash unless it has a file extension
		$ds = preg_match('/\.\w{3,}$/',$base) ? '' : '/';
		
        return $base.$ds.$query;
	}


    /*
     * Creates a canonical link for the request path. Uses query string in url excluding cache & debug params. Sorts query vars alphabetically
     * @params array  $query_vars Optionally pass an array of query vars to use instead of query string in url
     * @return string $canonical_view_path
     * @see ViewServiceProvider
     */
    public function canonical($query_vars = [])
    {
        $request_path = preg_replace('/(\/|^)index$/', '$1', preg_replace('/\.(html|php)$/', '', Request::getPathInfo()));

        if(empty($query_vars)) {
            $query_vars = Request::except(['clear_view_cache', 'APP_DEBUG']);
        }

        if(!empty($query_vars)) {
            // remove any filters set to 'all' to avoid duplicate content
            if(($key = array_search('all', $query_vars)) !== false) {
                unset($query_vars[$key]);
            }
            ksort($query_vars);
            $request_path = $request_path . '?' . http_build_query($query_vars);
        }

        $canonical_view_path = url($request_path, array(), true);

        return $canonical_view_path;
    }




	///////////////// Methods copied/edited from parent ////////////////////
	
    /**
     * Generate an absolute URL to the given path.
     *
     * @param  string  $path
     * @param  mixed  $extra
     * @param  bool|null  $secure
     * @return string
     */
    public function to($path, $extra = [], $secure = null)
    {
        // First we will check if the URL is already a valid URL. If it is we will not
        // try to generate a new one but will simply return the URL as is, which is
        // convenient since developers do not always have to check if it's valid.
        if ($this->isValidUrl($path)) {
            return $path;
        }

        $scheme = $this->getScheme($secure);

        $extra = $this->formatParameters($extra);

        $tail = implode('/', array_map(
            'rawurlencode', (array) $extra)
        );

        // Once we have the scheme we will compile the "tail" by collapsing the values
        // into a single string delimited by slashes. This just makes it convenient
        // for passing the array of parameters to this URL as a list of segments.
        $root = $this->getRootUrl($scheme);

        if (($hashPosition = strpos($path, '#')) !== false) {
            $hash = mb_substr($path, $hashPosition);
            $path = mb_substr($path, 0, $hashPosition);
        } else {
            $hash = '';
        }
		
        if (($queryPosition = strpos($path, '?')) !== false) {
            $query = mb_substr($path, $queryPosition);
            $path = mb_substr($path, 0, $queryPosition);
        } else {
            $query = '';
        }
		
		$lang_code = Lang::pathLang('/');
		// turn this into a localized URL
		if ( $lang_code ) {
			$path = trim(Lang::pathWithoutLang($path), "/");
			
			// ignore minifier
			if ( strpos($path,'min') !== 0 ) {
				
				// ignore files that exist in public unless they have localized versions
				if( !file_exists(public_path($path)) || is_dir(public_path($path)) || file_exists(public_path($lang_code . "/" . $path)) ) { 
					$path = $lang_code . "/" . $path;
				}
			}
		}
		
		$base = $this->trimUrl($root, $path, $tail);
		
		// add trailing slash unless it has a file extension
		$ds = preg_match('/\.\w+$/',$base) ? '' : '/';

        return $base.$ds.$query.$hash;
    }
	
    /**
     * Generate a URL to an application asset.
     *
     * @param  string  $path
     * @param  bool|null  $secure
     * @return string
     */
    public function asset($path, $secure = null)
    {
        if ($this->isValidUrl($path)) {
            return $path;
        }

        // Once we get the root URL, we will check to see if it contains an index.php
        // file in the paths. If it does, we will remove it since it is not needed
        // for asset paths, but only for routes to endpoints in the application.
        $root = $this->getRootUrl($this->getScheme($secure));

        return $this->removeIndex($root).'/assets/'.trim($path, '/');
    }

    /**
     * Get the scheme for a raw URL.
     *
     * @param  bool|null  $secure
     * @return string
     */
    protected function getScheme($secure)
    {
		if ( is_null($secure) ) {
            if($_SERVER["HTTP_HOST"] == "wwwtest.microfocus.com" || $_SERVER["HTTP_HOST"] == "wwwstage.microfocus.com" || $_SERVER["HTTP_HOST"] == "www.microfocus.com") {
                $this->forceSchema = 'https://';
                $this->cachedSchema = 'https://';
                return 'https://';
            }

            if ( is_null($this->cachedSchema) ) {
                if ( isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' ) {
					$this->cachedSchema = 'https://';
				} else {
					$this->cachedSchema = $this->forceSchema ?: $this->request->getScheme().'://';
				}
            }

            return $this->cachedSchema;
		}
		
        return $secure ? 'https://' : 'http://';

    }

}