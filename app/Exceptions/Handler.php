<?php namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
//use Bugsnag;

class Handler extends ExceptionHandler {

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		/*'Symfony\Component\HttpKernel\Exception\HttpException'*/
		HttpException::class,
		ModelNotFoundException::class
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $e)
	{

		/* commenting out bugsnag for now
		// report non-fatal exception to Bugsnag
		if ( !$e instanceof \Symfony\Component\Debug\Exception\FatalErrorException ) {
			Bugsnag::notifyException($e);
		} else {
			// bugsnag can't handle fatal exceptions, so we manually handle those here
		}
		*/
		return parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
		if (!config('app.debug')) {
			if (!$this->isHttpException($e)) $e = new \Symfony\Component\HttpKernel\Exception\HttpException(500);
		}
		return parent::render($request, $e);
	}

}
