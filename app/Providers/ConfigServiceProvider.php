<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Dotenv;

class ConfigServiceProvider extends ServiceProvider {

	/**
	 * Overwrite any vendor / package configuration.
	 *
	 * This service provider is intended to provide a convenient location for you
	 * to overwrite any "vendor" or package configuration that you may want to
	 * modify before the application handles the incoming request / command.
	 *
	 * @return void
	 */
	public function register()
	{
		config([
			//
		]);

		// load .env file found in the site path
		#Dotenv::load(public_path() . '/..');

	}

}
