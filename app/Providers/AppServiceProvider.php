<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Framework\UrlGenerator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
		$this->app->bind('url',function($app) {
			return new UrlGenerator($app['router']->getRoutes(),$app['request']);
		},true);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
