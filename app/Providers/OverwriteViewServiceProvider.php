<?php
namespace App\Providers;

use App\Framework\Blade;
use Illuminate\View\Engines\CompilerEngine;

// The whole purpose of this file is to change the framework to use our BladeCompiler instead of the default
// Everything is exactly copied over from the base class except Blade is used instead of BladeCompiler
class OverwriteViewServiceProvider extends \Illuminate\View\ViewServiceProvider {
	
    /**
     * Register the Blade engine implementation.
     *
     * @param  \Illuminate\View\Engines\EngineResolver  $resolver
     * @return void
     */
    public function registerBladeEngine($resolver)
    {
        $app = $this->app;

        // The Compiler engine requires an instance of the CompilerInterface, which in
        // this case will be the Blade compiler, so we'll first create the compiler
        // instance to pass into the engine so it can compile the views properly.
        $app->singleton('blade.compiler', function ($app) {
            $cache = $app['config']['view.compiled'];

            return new Blade($app['files'], $cache);
        });

        $resolver->register('blade', function () use ($app) {
            return new CompilerEngine($app['blade.compiler']);
        });
    }
	
}
