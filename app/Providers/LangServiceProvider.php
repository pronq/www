<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\Language;

class LangServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
        $this->app->singleton('lang', function ($app) {
            return new Language();
        });
	}

}
