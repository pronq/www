<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Request;
use Blade;
use View;
use Lang;
use URL;

class ViewServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services. Extending view functionality
	 *
	 * @return void
	 */
	public function boot() {
		
		Blade::directive('extra_translate',function($expression) {
			return '<?php $extra_translate = Lang::parseTranslationFile'.$expression.'; ?>';
		});
		
		
		// setup our view composer to handle translation keys
		$GLOBALS['view_translate'] = array();
		View::composer('*', function($view) {
			$view_name = $view->getName();
			if ( empty($view_name) ) return;

			$view_data = $view->getData();
			if (!empty($view_data['customTranslationPath'])) {
				$view_name = $view_data['customTranslationPath'];
			}
			$parsed_view = preg_replace('/::[a-zA-Z-]{2,5}(\.|\/)/', '::', $view_name);
			$view_arr = explode('::', $parsed_view);
			$view_type = $view_arr[0];

			$possible_view_paths = array(
				'custom' => 1,
				'content' => 1,
			);
			if (strpos($view_type, "errors") !== false || isset($possible_view_paths[$view_type])) {
				if (!empty($view_data['translate'])) {
					if (!empty($view_data['view_translate'])) {
						$GLOBALS['view_translate'] = $view_data['view_translate'];
					} else {
						$GLOBALS['view_translate'] = $view_data['translate'];
						$view->with("view_translate", $view_data['translate']);
					}
				} else {
					$translate = Lang::parseTranslationFile(str_replace('.', '/', $view_name));
					if (!empty($translate)) {
						$GLOBALS['view_translate'] = $translate;
						$view->with("translate", $translate);
						$view->with("view_translate", $translate);
					} else {
						$view->with("translate", array());
						$view->with("view_translate", array());
					}
				}

				if (empty($view_data['canonical_view_path'])) {
					$view->with("canonical_view_path", URL::canonical());
				}

			}
			if ($view_type == 'includes') {
				$translate = Lang::parseTranslationFile(str_replace('.', '/', $view_name));
				if (!empty($translate)) {
					$view->with("translate", $translate);
				} else {
					$view->with("translate", array());
				}
				if (!empty($GLOBALS['view_translate']) && empty($view_data['view_translate'])) {
					$view->with("view_translate", $GLOBALS['view_translate']);
				}
			}

			$globalNav = $this->loadGlobalNav();
			$view->with("global_nav", $globalNav);
			
			// for header/footer, reset the language from the path
			if ( $view_type == 'layouts' && Lang::currentLang() == Lang::getLangConfig("default") ) {
				Lang::setCurrentLang(Lang::pathLang());
			}
			//echo "{$view_type}::{$view_name}\n";
			
		});


	}

	/**
	 * Load the global nav from PNX and store it in APC Cache
	 *
	 * @return void
	 */
	private function loadGlobalNav() {
		$apcGlobalNavKey = "GLOBAL_NAV";
        $url = getenv($apcGlobalNavKey);
        
		if (apcu_exists($apcGlobalNavKey) && !apcu_fetch($apcGlobalNavKey) == '') {
			//echo "cached";
			return json_decode(apcu_fetch($apcGlobalNavKey), true);
		}
		else {
			//echo "not-cached";
	        $ch = curl_init();
	        curl_setopt($ch, CURLOPT_URL, $url);
	
	        // set headers
	        $headers = array('Content-type: application/json');
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	
	        // return transfer as string
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 5);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 5); //timeout in seconds
	
	        $response = curl_exec($ch);
	
	        // store the response info including the HTTP status
	        // 400 and 500 status codes indicate an error
	        $responseInfo = curl_getinfo($ch);
	        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	
	        curl_close($ch);
	
	        if ($httpCode > 400) {
	            $response = '';
	        }
	        
	        // modify hard coded www links for environment
	        if(getenv('GLOBAL_TEST') == "test") {
		        $response = str_replace("https:\/\/www.microfocus.com","https:\/\/wwwtest.microfocus.com",$response);
	        }
	        else if(getenv('GLOBAL_TEST') == "stage") {
		        $response = str_replace("https:\/\/www.microfocus.com","https:\/\/wwwstage.microfocus.com",$response);
	        }
	        
	        apcu_store( $apcGlobalNavKey, $response, getenv('CACHE_LENGTH') );
	        
	        return json_decode($response, true);
        }
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register() {

		// add custom views directory
		view()->addLocation(public_path('../resources/views'));

		// set custom view namespaces
		view()->addNamespace('content', public_path('../resources/views/content'));
		view()->addNamespace('custom', public_path('../resources/views/custom'));
		view()->addNamespace('emails', public_path('../resources/views/emails'));
		view()->addNamespace('errors', public_path('../resources/views/errors'));
		view()->addNamespace('includes', public_path('../resources/views/includes'));
		view()->addNamespace('layouts', public_path('../resources/views/layouts'));
	}

}
