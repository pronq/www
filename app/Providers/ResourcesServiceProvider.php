<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\Resources;

class ResourcesServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('resources', function ($app) {
            return new Resources();
        });
    }

}
