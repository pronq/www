<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// run all routes through our check language filter
Route::when('*', 'checkLanguage');
// determine our current BU
if ( isset($_SERVER['HTTP_HOST']) ) {
	// determine our language code
	$languages = Config::get('lang.languages');
	$lang = in_array(Request::segment(1), $languages) ? Request::segment(1) : Config::get('lang.default');
	$lang_prefix = $lang == Config::get('lang.default') ? null : $lang;
	// determine bu from url
	$host_arr = explode('.', $_SERVER['HTTP_HOST']);
	$bu = count($host_arr) > 2 ? $host_arr[1] : $host_arr[0];
    // set the sessions if they aren't set yet
    if ( !Session::has('lang') ) {
    	Session::put('lang', $lang);
    }
    if ( !Session::has('bu') ) {
    	Session::put('bu', $bu);
    }
    // share our values
    View::share('lang', $lang);
    View::share('bu', $bu);
    View::share('layout', $bu . '.layouts.');
    // set the locale
    $lang_map = Config::get('lang.map');
    L4gettext::setLocale($lang_map[$lang]);
    // init gettext translation now
    Translate::init($lang_map[$lang]);
	Route::group(array('prefix' => $lang_prefix), function() use ($bu) {
		// load the BU specific routes
		require app_path() . '/routes/' . $bu . '.php';
	});
}

// declare global, cross-bu routes
Route::get('change-language/{lang}/{url?}', function($lang, $url = null) {
	$url_parsed = parse_url(URL::previous());
	$host = $url_parsed['host'];
	$lang_prefix = $lang == Config::get('lang.default') ? null : '/' . $lang;
	$path = isset($url_parsed['path']) ? $url_parsed['path'] : '/';
	// override path if we have it set by url parameter
	if ( !is_null($url) ) {
		$previous_url_parsed = parse_url(base64_decode($url));
		$path = isset($previous_url_parsed['path']) ? $previous_url_parsed['path'] : '/';
	}
	$path_parsed = preg_replace('/^\/+/', '', preg_replace('/(' . implode('|', Config::get('lang.languages')) . ')/', '', $path));
	$new_path = '/' . ($path == '/' ? '' : preg_replace('/\/$/', '', $path_parsed));
	$new_url = 'http://' . $host . $lang_prefix . $new_path;
	Session::put('lang', $lang);
	return Redirect::to($new_url);
});


