<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * 'id' => int 1
 * 'taxonomy_group' => int 1
 * 'value_name' => string 'Marketing' (length=9)
 * 'business_unit' => null
 * 'obsolete' => int 0
 * 'old_id' => string 'fzs7qyly' (length=8)
 * 'group_name' => string '' (length=0)
 * 'display_name' => null
 */

/*
Common Field IDs:
8 = Purpose
9 = Type
138 = Solution
139 = Product
144 = Event
145 = Market Play
163 = Campaign
*/

class TaxonomyValue extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'smrl_taxonomy_value';
	
	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

	static public function get_market_plays_by_bu($bu) {
		return TaxonomyValue::where('smrl_taxonomy_value.taxonomy_field',145)
		  ->where('smrl_taxonomy_value.obsolete','<>',1)
		  ->where('smrl_taxonomy_value.business_unit',$bu)
		  ->leftJoin('smrl_resource_section','smrl_resource_section.taxonomy_value_id','=','smrl_taxonomy_value.id')
		  ->orderBy('smrl_taxonomy_value.value_name')
		  ->select('*','smrl_taxonomy_value.id as taxonomy_value_id','smrl_resource_section.section_key as section_id')
		  ->get();
		//$query = "
		//	SELECT * from smrl_taxonomy_value AS TaxonomyValue
		//		LEFT JOIN smrl_resource_section AS SMRLSection
		//			ON (SMRLSection.taxonomy_value_id = TaxonomyValue.id)
		//		WHERE TaxonomyValue.taxonomy_field = 145 AND TaxonomyValue.obsolete != '1' AND TaxonomyValue.business_unit = '$bu'
		//		ORDER BY TaxonomyValue.value_name ASC
		//";
	}
	
	static public function get_solutions_by_bu($bu) {
		return TaxonomyValue::where('smrl_taxonomy_value.taxonomy_field',138)
		  ->where('smrl_taxonomy_value.obsolete','<>',1)
		  ->where('smrl_taxonomy_value.business_unit',$bu)
		  ->leftJoin('smrl_resource_section','smrl_resource_section.taxonomy_value_id','=','smrl_taxonomy_value.id')
		  ->orderBy('smrl_taxonomy_value.value_name')
		  ->select('*','smrl_taxonomy_value.id as taxonomy_value_id','smrl_resource_section.section_key as section_id')
		  ->get();
		//$query = "
		//	SELECT * from smrl_taxonomy_value AS TaxonomyValue
		//		LEFT JOIN smrl_resource_section AS SMRLSection
		//			ON (SMRLSection.taxonomy_value_id = TaxonomyValue.id)
		//		WHERE TaxonomyValue.taxonomy_field = 138 AND TaxonomyValue.obsolete != '1' AND TaxonomyValue.business_unit = '$bu'
		//		ORDER BY TaxonomyValue.value_name ASC
		//";
	}
	
	static private function get_generic_product_type_by_bu($id, $bu) {
		return TaxonomyValue::where('smrl_taxonomy_value.taxonomy_field',$id)
		  ->where('smrl_taxonomy_value.obsolete','<>',1)
		  ->where('smrl_taxonomy_value.business_unit',$bu)
		  ->leftJoin('smrl_resource_section','smrl_resource_section.taxonomy_value_id','=','smrl_taxonomy_value.id')
		  ->orderBy('smrl_taxonomy_value.value_name')
		  ->select('*','smrl_taxonomy_value.id as taxonomy_value_id','smrl_resource_section.section_key as section_id')
		  ->get();
		//$query = "
		//	SELECT * from smrl_taxonomy_value AS TaxonomyValue
		//		LEFT JOIN smrl_resource_section AS SMRLSection
		//			ON (SMRLSection.taxonomy_value_id = TaxonomyValue.id)
		//		WHERE TaxonomyValue.taxonomy_field = $id AND TaxonomyValue.obsolete != '1' AND TaxonomyValue.business_unit = '$bu'
		//		ORDER BY TaxonomyValue.value_name ASC
		//";
	}
	static public function get_campaigns_by_bu($bu) { return static::get_generic_product_type_by_bu(163, $bu); }
	static public function get_events_by_bu($bu) { return static::get_generic_product_type_by_bu(144, $bu); }
	
	static public function get_available_formats() {
		return TaxonomyValue::where('smrl_taxonomy_value.taxonomy_field',9)
		  ->where('smrl_taxonomy_value.obsolete','<>',1)
		  ->orderBy('smrl_taxonomy_value.value_name')
		  ->get();
		//$query = "
		//	SELECT * from smrl_taxonomy_value AS TaxonomyValue
		//		WHERE TaxonomyValue.taxonomy_field = 9 AND TaxonomyValue.obsolete != '1'
		//		ORDER BY TaxonomyValue.value_name
		//";
	}
	
	static public function get_available_topics() {
		return TaxonomyValue::where('smrl_taxonomy_value.taxonomy_field',8)
		  ->where('smrl_taxonomy_value.obsolete','<>',1)
		  ->orderBy('smrl_taxonomy_value.value_name')
		  ->whereNotNull('old_id')
		  ->get();
		//$query = "
		//	SELECT * from smrl_taxonomy_value AS TaxonomyValue
		//		WHERE TaxonomyValue.taxonomy_field = 8 AND TaxonomyValue.obsolete != '1' AND TaxonomyValue.old_id IS NOT NULL
		//		ORDER BY TaxonomyValue.value_name
		//";
	}
	
	static public function get_available_category_purpose() {
		return static::get_generic_taxonomy_value(1);
	}
	static public function get_available_category_type() {
		return static::get_generic_taxonomy_value(8);
	}
	static public function get_available_content_category() {
		return static::get_generic_taxonomy_value(9);
	}
	static public function get_available_taxonomy_solutions() {
		return static::get_generic_taxonomy_value(138);
	}
	static public function get_available_taxonomy_products() {
		return static::get_generic_taxonomy_value(139);
	}
	static public function get_available_market_plays() {
		return static::get_generic_taxonomy_value(145);
	}
	static public function get_available_campaigns() {
		return static::get_generic_taxonomy_value(163);
	}
	static public function get_available_industries() {
		return static::get_generic_taxonomy_value(6);
	}
	static public function get_available_events() {
		return static::get_generic_taxonomy_value(144);
	}
	static public function get_available_languages() {
		return static::get_generic_taxonomy_value(158);
	}
	static public function get_available_contributor_group() {
		return static::get_generic_taxonomy_value(12);
	}
	static private function get_generic_taxonomy_value($taxonomy_id) {
		return TaxonomyValue::where('smrl_taxonomy_value.taxonomy_field',$taxonomy_id)
		  ->where('smrl_taxonomy_value.obsolete','<>',1)
		  ->orderBy('smrl_taxonomy_value.value_name')
		  ->get();
		
//		$query = "
//			SELECT * from smrl_taxonomy_value AS TaxonomyValue
//				WHERE TaxonomyValue.taxonomy_field =$taxonomy_id AND TaxonomyValue.obsolete != '1' $bu_string
//				ORDER BY TaxonomyValue.value_name
//		";
		return $this->query($query);
	}

}
