<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * 'id' => int 1
 * 'section' => null
 * 'title' => string 'ISV Key Resources' (length=17)
 * 'url' => string 'https://teaming.innerweb.novell.com/ssf/a/c/p_name/ss_forum/p_action/1/binderId/74238/action/view_permalink/seen_by_gwt/1/entityType/folderEntry/entryId/138760/url_created_by_teaming/1' (length=184)
 * 'priority' => null
 */

class Resource extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'smrl_resource';
	
	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
	
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','url','section','priority'];
}
