<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * 'section_key' => string 'fy11_launch' (length=255)
 * 'short_description' => string 'FY11 Launch' (length=255)
 * 'taxonomy_value_id' => int 0 (If this field is not empty, than the section maps directly to a taxonomy field/value
 */

class Section extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'smrl_resource_section';
	
	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
	
	static public function get_resources($section_key) {
		return Resource::where([
			'section'=>$section_key
		])->orderBy('priority')
		  ->get();
		//$query = "
		//	SELECT * FROM resource AS Resource
		//		WHERE 
		//			Resource.section = '$section_id'
		//		ORDER BY 
		//			Resource.priority ASC
		//";
	}
	
	static public function get_section($section_key) {
		return Section::where([
			'smrl_resource_section.section_key' => $section_key,
		])->leftJoin('smrl_taxonomy_value','smrl_resource_section.taxonomy_value_id','=','smrl_taxonomy_value.id')
		  ->select('*','smrl_taxonomy_value.id as taxonomy_value_id','smrl_resource_section.section_key as section_id')
		  ->first();
		//$query = "
		//	SELECT * FROM smrl_resource_section AS Section
		//		LEFT JOIN taxonomy_value AS TaxonomyValue
		//			ON (TaxonomyValue.id = Section.taxonomy_value)
		//		WHERE Section.id = :section_id
		//";
	}
	
}
