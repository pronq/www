<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * 'id' => int 11
 * 'product_name' => string (length=255)
 * 'product_key' => string (length=255)
 * 'business_unit' => string (length=255)
 * 'group_name' => string (length=255)
 */

class Product extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'smrl_product';
	
	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
	
	static public function get_products_by_bu_by_group($bu, $group_name) {
		return Product::where('business_unit',$bu)
		  ->where('group_name',$group_name)
	      ->where('hidden',0)
          ->orderBy('product_name')
		  ->get();
		//$query = "
		//	SELECT * from smrl_product AS Product
		//		WHERE smrl_product.business_unit = '$bu' AND smrl_product.group_name = '$group_name'
		//		ORDER BY smrl_product.product_name ASC
		//";
	}
	
	static public function get_products_by_bu($bu) {
		return Product::where('business_unit',$bu)
	      ->where('hidden',0)
          ->orderBy('product_name')
		  ->get();
		//$query = "
		//	SELECT * from smrl_product AS Product
		//		WHERE business_unit = '$bu'
		//		ORDER BY product_name ASC
		//";
	}

}
