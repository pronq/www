<?php namespace App\Services;

use Request;
use Lang;
use URL;
use App;
use Config;

class BaseService extends Service {

	/*
	 * compile our less file
	 */
	public function compileLess($site = null) {
		$status = true;
		// prevent compiling on production environment
		if ( App::environment('live','stage') ) {
			$message = 'Oops! LESS compilation is not permitted on the production environment!';
			$status = false;
		} else {
			// compile the css file based on our _main.less file
			$less = new \lessc;
			$less->setPreserveComments(false);

			$less_path = public_path('../resources/less');
			$css_path = public_path('assets/css');

			$result = false;
			foreach (scandir($less_path) as $file) {
				if ('.' === $file) continue;
				if ('..' === $file) continue;
				if ('_' == substr($file, 0, 1)) continue;

				$file_path_info = pathinfo($file);
				$file_name = $file_path_info['filename'];

				$result = $less->compileFile("$less_path/$file", "$css_path/$file_name.css");
			}

			// display result message if we didn't throw an exception
			if ( $result ) {
				$message = "Success! The '/assets/css/main.css' file has been compiled.";
			} else {
				$message = "Oops! Something went wrong while attemting to compile the main.less file.";
			}
		}
		return [
			'status' => $status,
			'message' => $message
		];
	}


	/*
	 * change our language
	 */
	public function changeLanguage($lang, $country_code, $country_name, $url = null) {
		// get the lang prefix
		$lang_prefix = $lang == Lang::getLangConfig("default") ? '' : '/' . $lang;

		// remove lang from url's that should never have a lang in them
		$url_data = Lang::isLanglessPath($lang_prefix, $url);
		$new_url = $url_data['url'];

		session()->put('lang', $lang);
		session()->put('country_code', $country_code);

		$countries = Lang::getLangConfig("countryCombinations");
		if (isset($countries["$lang|$country_code"])) {
			$english_country = $countries["$lang|$country_code"];
		} else if (isset($countries["$lang_prefix|$country_code"])) {
			$english_country = $countries["$lang_prefix|$country_code"];
		} else {
			$english_country = $country_name;
		}

		$country_name = "";
		if (is_array($english_country)) {
			session()->put('english_country', $english_country[0]);
			session()->put('country_name', $english_country[1]);
			$country_name = $english_country[1];
		} else {
			session()->put('english_country', $english_country);
			session()->put('country_name', $english_country);
			$country_name = $english_country;
		}

		// still set cookies to support legacy systems
		setcookie ( "user_language" , "$lang" , time() + 3600 , "/" ,  ".microfocus.com");
		setcookie ( "user_country" , "   $country_name" , time() + 3600 , "/" ,  ".microfocus.com");

		return $new_url;
	}


	/*
	 * change our language
	 */
	public function setCountry($country_code) {
		return session()->put('country_code', $country_code);
	}

	/*
	 * Get countries list
	 */
	function getCountriesForLang($lang='en-us') { // DREW TODO - should not be set to en-us by default but rather the sites default lang
		$suggestedCountries = Lang::getLangConfig("suggestedCountries");
		$countries = array('primary' => array(), 'secondary' => array());

		$i = 0;
		foreach($suggestedCountries as $key => $combo) {
			$comboKeys = explode("|",$key);
			if($comboKeys[0] == $lang){
				$countries['primary'][$i] = array(
					'lang_code' => $comboKeys[0],
					'country_code' => $comboKeys[1],
					'name' => $combo["1"],
				);

				$i++;
			}
		}

		$combinations = Lang::getLangConfig("countryCombinations");
		$j = 0;
		foreach($combinations as $key => $combo) {
			$comboKeys = explode("|",$key);

			//if this option is in the suggested countries list, exclude it
			if(array_key_exists($key, $suggestedCountries))
				continue;

			if($lang != null && strpos($key, $lang) !== false) {
				$countries['secondary'][$j] = array(
					'lang_code' => $comboKeys[0],
					'country_code' => $comboKeys[1],
					'name' => $combo["1"],
				);

				$j++;
			}
		}

		return $countries;
	}

	/*
	 * determine view to serve based on current route
	 */
	public function getDynamicView($request_path = null) {
		if(is_null($request_path)) {
			$request_path = $this->get_request_path();
		} else {
			$request_path = preg_replace('/\.html$/', '', $request_path);
		}

		// compile less on page load if we are local
		if ( App::environment('test','dev') ) {
			$this->compileLess();
		}


		///////////////////////////////////////////////////////////////
		// do the apc view caching
		//--------------------------------------------
		$clear_view_cache = isset($_GET['clear_view_cache']) ? true : false;
		if ( $clear_view_cache ) {
			$this->clear_view_apcu_cache();
		}
		$catch_all_apcu_key = $this->get_view_cache_apcu_key();
		// don't show cached pages to logged in users
                if (!\Session::has('is_logged_in') || \Session::get('is_logged_in') == false) {
                    // only return APC cache if we are NOT on local and the cache exists
                    if ( !App::environment('test','dev') && apcu_exists($catch_all_apcu_key) ) {
                            return apcu_fetch( $catch_all_apcu_key );
                    }
                }
		// end apc cache --------------------------------------------


		$en_path = trim(Lang::pathWithoutLang($request_path), '');
		$en_view_path = "content::" . Lang::getLangConfig("default") . $en_path;
		$view_path = 'content::' . Lang::currentLang() . $en_path;
		$index_view_path = $view_path . "/index";
		$en_index_view_path = $en_view_path . "/index";
		$path_segments = explode('/',$en_path);
		$content = '';

		// check if current route matches excluded views 
		if(is_file(public_path('../resources/views/excludes.php'))){
			$excluded_view_urls = include public_path('../resources/views/excludes.php');
			if(in_array($en_path,$excluded_view_urls)){
				return false;
			}
		}
		
		if ( view()->exists($index_view_path) ) {
			$content = view($index_view_path)->__toString();
		} else if ( view()->exists($view_path) && ($path_segments[sizeof($path_segments)-1] != 'index') ) {
			$content = view($view_path)->__toString();
		} else if ( view()->exists($en_index_view_path) ) {
			$content = view($en_index_view_path)->__toString();
		} else if ( view()->exists($en_view_path) && ($path_segments[sizeof($path_segments)-1] != 'index') ) {
			$content = view($en_view_path)->__toString();
		}
		if ( !empty($content) ) {
                        // don't create cached pages for logged in users
                        if (!\Session::has('is_logged_in') || \Session::get('is_logged_in') == false) {
                            apcu_store( $catch_all_apcu_key, $content, getenv('CACHE_LENGTH'));
						}
			return $content;
		} else {
			return false;
		}
		

	}

	public function get_lang_paths() {
		if (empty($this->lang_paths)) {
			$request_path = $this->get_request_path();
			if ( Lang::onValidPath() ) { // means we are on a lang path that exists in lang config file
				$view_path = "content::" . $request_path;
				$first_path_segment = Request::segment(1);
				$raw_english_path = substr($request_path, strlen($first_path_segment) + 1); // remove the first part of the path
			} else { // we are not on a lang path so just try for english
				$view_path = "content::" . Lang::getLangConfig("default") . "/" . $request_path;
				$raw_english_path = $request_path;
			}
			$this->lang_paths = compact('view_path', 'raw_english_path');
		}

		return $this->lang_paths;
	}

	public function get_request_path() {
		if (empty($this->request_path)) {
			$this->request_path = preg_replace('/\.html$/', '', Request::path());
		}

		return $this->request_path;
	}

	public function get_view_cache_key_prefix() {
		$curr_country_code = Lang::currentCountryCode();
		return "catch_all_apcu_key_{$_SERVER['HTTP_HOST']}_$curr_country_code";
	}

	public function get_view_cache_apcu_key() {
		$request_path = trim($this->get_request_path(),"/");

		$apcu_cache_prefix = $this->get_view_cache_key_prefix();
		$catch_all_apcu_key = "{$apcu_cache_prefix}$request_path";

		return $catch_all_apcu_key;
	}

	public function clear_view_apcu_cache() {
		$en_path = Lang::pathWithoutLang($this->get_request_path());
		$apcu_cache_prefix = $this->get_view_cache_key_prefix();

		// Delete this page and all localized versions:
		$all_lang_paths = Lang::getAllLangPathsByEnPath($en_path);
		$delete_keys = array();
		foreach ($all_lang_paths as $curr_path) {
			$curr_path = trim($curr_path,"/");
			$delete_keys[] = "{$apcu_cache_prefix}$curr_path";
		}
		return $this->clear_apcu_cache($delete_keys);
	}

	/*
	 * Returns true/false for whether the apc cache is fine to use
	 */
	public function check_cache($apcu_key) {
		
		$clear_view_cache = isset($_GET['clear_view_cache']) ? true : false;
		if ( $clear_view_cache ) {
			$this->clear_apcu_cache($apcu_key);
			return false;
		}

		// don't show cached pages to logged in users
		if (!\Session::has('is_logged_in') || \Session::get('is_logged_in') == false) {
			// only return APC cache if we are NOT on local and the cache exists
			if ( !App::environment('test','dev') && apcu_exists($apcu_key) ) {
					return true;
			}
		}
		
		return false;
	}

	/*
	 * Stores the apc cache if it should
	 */
	public function store_cache($apcu_key,$content) {
		
		// don't cache logged in users
		if (!\Session::has('is_logged_in') || \Session::get('is_logged_in') == false) {
			// only cache if we are NOT on local
			if ( !App::environment('test','dev') ) {
					apcu_store( $apcu_key, $content, getenv('CACHE_LENGTH') );
			}
		}
		
	}

	public function clear_apcu_cache($keys) {
		if(getenv('CACHE_SERVER')) {
			$ch = curl_init();
			$servers = explode(';',getenv('CACHE_SERVER'));
			foreach($servers as $server) {
				curl_setopt($ch, CURLOPT_URL, 'http://'.$server.'/clearCache');
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($ch, CURLOPT_POSTFIELDS, array('keys'=>json_encode($keys)));
				$response = curl_exec($ch);
			}
			curl_close($ch);
		} else {
			if(is_array($keys)) {
				foreach($keys as $key) {
					apcu_delete($key);
				}
			} else {
				apcu_delete($keys);
			}
		}
	}


}
