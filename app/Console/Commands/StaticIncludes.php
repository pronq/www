<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Config;
use BaseController;

class StaticIncludes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'static:includes {delete?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate static include files from previous require-include route';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
		$this->site = env('SITE');
		$this->lang = Config::get('lang');
		$this->default_language = $this->lang[$this->site]['default'];
		$this->languages = array_keys($this->lang[$this->site]['languages']);
		
		$this->includes = [
			'main_head',
			'nav/header',
			'nav/footer'
		];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
		if ($this->argument('delete')) {
			$this->delete_includes();
		}
		else {
			foreach($this->includes as $i) {
				foreach ($this->languages as $lang) {
					for ($j=0; $j<=1; $j++) {
						$lang_path = ($lang == $this->default_language ? '' : "/$lang");
						$output_path = public_path("require-include/".base64_encode($i)."/html/${j}${lang_path}/");
						$this->info($output_path);
						//thirdPartyRequireInclude($path, $ext, $cache = true, $lang = '')
						$data = \App\Http\Controllers\BaseController::thirdPartyRequireInclude(base64_encode($i),'html',1,$lang);
						if (!empty($data)) {
							if ($i == 'main_head') {
								$data = $this->create_main_head_output($data);
							}
							if (!is_dir($output_path)) {
								mkdir($output_path, 0775, true);
							}
							file_put_contents("$output_path/index.php", $data, LOCK_EX);
						}
					}
				}
			}
		}
	}
	
	public function create_main_head_output($data) {
		$output_data = '<?php
$include_string = <<<STR
' . $data . '
STR;
';
		$regex_data = '
if (isset($_GET["no-jquery"]) && !empty($_GET["no-jquery"])) {
$include_string = preg_replace(\'/<script src=".*\/jquery.*\.js"><\/script>/\', "", $include_string);
$include_string = preg_replace(\'/vendor\/jquery.*?\.min.js,/\', "", $include_string);
}
echo $include_string;';
		return $output_data . $regex_data;
	}
	
	 /**
     * Handle delete option.
	 * Deletes the static include files
     *
     * @return void
     */
	public function delete_includes() {
		$this->rrm_dir(public_path('require-include'));
	}
	
	 /**
     * utility function to delete files/directories.
     *
     * @return void
     */
	private function rrm_dir($dir) {
		foreach (glob("{$dir}/*") as $file) {
			if (is_dir($file)) {
				$this->rrm_dir($file);
			}
			else {
				unlink($file);
				$this->info("removed file: {$file}");
			}
		}
		rmdir($dir);
		$this->info("removed directory: {$dir}");
	}

}

