<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class LessCompile extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'less:compile';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Compile our LESS files to a static CSS file.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire(\App\Services\BaseService $base_service)
	{
		$result = $base_service->compileLess($this->argument('site'));
		if ( $result['status'] ) {
			$this->info($result['message']);
		} else {
			$this->error($result['message']);
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['site', InputArgument::REQUIRED, 'Provide the name of the site you want to compile LESS for.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [];
	}

}
