<?php namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see App\Helpers\Resources
 */
class Resources extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'resources'; }

}