<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// get includes route - for third party includes
// type can be "css-asset", "js-asset", "content" (such as header or footer)
// key is the path to the asset or include - must be url encoded (maybe base64'd)?
// ext - can be js, html, or json = if js then will include a script that pulls in the html
// optionally pass in a lang = if you don't the lang will default to the cookie or default lang
Route::get('require-css/{manifest_name}/{ext}/{cache?}', ['uses' => 'BaseController@thirdPartyRequireCss']);
Route::get('require-js/{manifest_name}/{ext}/{cache?}', ['uses' => 'BaseController@thirdPartyRequireJs']);
Route::get('require-include/{content_path}/{ext}/{cache?}/{lang?}', ['uses' => 'BaseController@thirdPartyRequireInclude']);
//Route::get('require-body/{layout_path}/{view_path}/{ext}/{cache?}/{lang?}', ['uses' => 'BaseController@thirdPartyRequireContent']);
//Route::get('require-layout/{layout_path}/{view_path}/{ext}/{cache?}/{lang?}', ['uses' => 'BaseController@thirdPartyRequireContent']);

// generate static include files
Route::get('/admin/generate-includes', function() {
	try {
		Artisan::call('static:include');
		echo Artisan::output();
	} catch (Exception $e) {
		Response::make($e->getMessage(), 500);
	}
});

// delete the static include files
Route::get('/admin/delete-includes', function() {
	try {
		Artisan::call('static:include delete');
		echo Artisan::output();
	} catch (Exception $e) {
		Response::make($e->getMessage(), 500);
	}
});

// form builder transport route
Route::post('form-builder-transport', ['uses' => 'BaseController@formBuilderTransport']);
Route::get('formlookup/{key}', ['uses' => 'BaseController@formLookup']);

// Clear All Cache
Route::get('apc-clear-cache', ['uses' => 'BaseController@clearCache']);
Route::post('clearCache', ['uses' => 'BaseController@clearCacheKey']);

// change language route
Route::get('get-country-list/{lang}', ['uses' => 'BaseController@getCountriesForLang']);
Route::get('change-language/{lang}/{country_code}/{country_name}/{url?}', ['uses' => 'BaseController@changeLanguage']);
Route::get('set-country/{country_code}', ['uses' => 'BaseController@setCountry']);

// compile less into main.css
Route::get('compile-less', ['uses' => 'BaseController@compileLess']);

//Route::get('support-and-services/{filter}', ['uses' => "SupportController@filter"]);

// ip lookup
Route::get('demandbase-data', ['uses' => 'BaseController@demandbaseData']);

// Image Library JSON for Email Builder
Route::get('email/image-library/get/{dir?}', ['uses' => 'EmailBuilderImageLibraryController@getListing']);

// Trial downloads: LogDownload
Route::post('log-trial-download', ['uses' => 'TrialDownloadsController@logDownload']);

// heritage trial downloads: heritage mf, novell, borland download page (iframes)
Route::get('product-trials/{product}', ["uses" => "TrialDownloadsController@download"])->where('product', '.+');
Route::get('product-downloads/{product}', ["uses" => "TrialDownloadsController@download"])->where('product', '.+');
Route::get('en-GB/Products/{product}', ["uses" => "TrialDownloadsController@borland_download"])->where('product', '.+');
Route::get('e{iframeUrl}', ["uses" => "TrialDownloadsController@attachmate_download"])->where('iframeUrl', '\.aspx?.+');
Route::get('eval/download2.php', ['uses' => 'TrialDownloadsController@novell_download']);

Route::get('media/container/{assetID}', ["uses" => "MediaBinController@index"])->where('assetID', '.+');

// Documentation
		Route::get('documentation/{param1}', array("as"=>"documentation", "uses"=>"DocumentationController@index"))->where(['param1' => '.+']);

// Regular pages to not cache

// subscriptions API
Route::post('subscriptions-transport', ["uses" => "SubscriptionsController@submit"]);
Route::post('subscriptions-lookup', ["uses" => "SubscriptionsController@lookup"]);
// simple non-formbuilder form that sends an email
Route::post('send-form', ['uses' => 'EmailController@send']);

///////////////////////////////////////////////////////////////////////////
// microfocus specific routes
$microfocusRoutes = function() {
	// microfocus non translatable routes go here

};
Route::group(['domain' => 'microfocus.{tld}'], $microfocusRoutes);
Route::group(['domain' => '{subdomain}.microfocus.{tld}'], $microfocusRoutes);


////////////////////////////////////////////////////////
// Routes for things that need translation
$needsLangRoutes = function() {
	// search route
	Route::get('search/{path?}', ['uses' => 'SearchResultsController@getSearchResultsPage'])->where('path', '[a-z]+');


	///////////////////////////////////////////////////////////////////////////
	// microfocus specific routes
	$microfocusLangRoutes = function() {
		// microfocus translatable routes go here
		// resources
		Route::get('resources', ["uses" => "ResourcesController@index"]);
		Route::get('resources/asset/{resource}', ["uses" => "ResourcesController@gate"])->where(['resource' => '.+']);
		Route::get('resources/thank-you/{resource}', ["uses" => "ResourcesController@thankyou"])->where(['resource' => '.+']);
		Route::get('resources/{type}/{resource}', ["uses" => "ResourcesController@subgate"])->where(
			['type' => '^(articles|briefs|brochures|data-sheets|flash-point-papers|flyers|guides|top-reasons|white-paper|ebook)'],
			['resource' => '.+']
		);
		Route::get('resources/{type}/thank-you/{resource}', ["uses" => "ResourcesController@subthankyou"])->where(
			['type' => '^(articles|briefs|brochures|data-sheets|flash-point-papers|flyers|guides|top-reasons|white-paper|ebook)'],
			['resource' => '.+']
		);
		
		// resource asset pages for case-studies, flash-point-papers, fliers, top-reasons, briefs, brochures, data-sheets
		// Route::get('resources/{type}/{resource}', ['uses'=>'ResourceController@index'])->where(
		// 	['type' => '^(articles|briefs|brochures|data-sheets|flash-point-papers|flyers|guides|top-reasons)'],
		// 	['resource' => '.+']
		// );
		
		Route::get('promo/thank-you/{resource}', ["uses" => "ResourcesController@promoThankyou"])->where(['resource' => '.+']);
		Route::get('campaign/download/{resource}', ["uses" => "ResourcesController@campaignGate"])->where(['resource' => '.+']);
		Route::get('campaign/thank-you/{resource}', ["uses" => "ResourcesController@campaignThankyou"])->where(['resource' => '.+']);

		// subscriptions
		//Route::get('communities/subscription', ["uses" => "SubscriptionsController@index"]);
		Route::get('company/subscription', ["uses" => "SubscriptionsController@index"]);

		// communities
		Route::get('communities', ["uses" => "CommunitiesController@index"]);

		// support
		Route::get('support-and-services/', ['uses' => "SupportController@index"]);
		Route::get('training', ['uses' => "SupportController@training"]);
		Route::get('support-and-services/{handbook1}', ['uses' => "SupportController@handbook"])->where(['handbook1' => 'technical-handbook' ]);
		Route::get('support-and-services/{filter}', ['uses' => "SupportController@filter"])->where(['filter' => '.+']);

		// Press Room
		Route::get('about/press-room', ["uses"=>"PressRoomArticlesController@index"]);
		Route::get('about/press-room/article/{param1}', array("as"=>"about.pressroom.articles", "uses"=>"PressRoomArticlesController@article"))->where(['param1' => '.+']);
		Route::get('about/press-room/articles', ["uses"=>"PressRoomArticlesController@articlesList"]);
        Route::get('about/press-room/feed', ["uses"=>"PressRoomArticlesController@feed"]);

		
		//Route::get('documentation/{product}/', array("as"=>"documentation", "uses"=>"DocumentationController@index"))->where('product', '[a-zA-Z0-9\/\-]+');


		// Success stories
		Route::get('success/stories/{param1}', array("uses"=>"SuccessStoryController@story"))->where(['param1' => '.+']);

		// new trial download process
		Route::get('products/{product}/trial/download/{readme?}', ["uses" => "TrialDownloadsController@downloadReadme"])->where('product', '[a-zA-Z0-9\/\-]+');
		Route::get('products/{product}/trial/{thanks?}', ["middleware" => "auth", "uses" => "TrialDownloadsController@landingThankYou"])->where('product', '[a-zA-Z0-9\/\-]+');

		// thank you page for trial downloads
		Route::get('products/{product}/trial-download/thank-you', ["uses" => "TrialDownloadsController@thankYou"])->where('product', '[a-zA-Z0-9\/\-]+');

		Route::get('partners/app-rehost-providers/project-matrix', ['uses' => 'PartnersController@projectMatrix']);
		Route::get('partners/app-rehost-providers/download-all/', ['uses' => 'PartnersController@downloadAll']);

		// products a-z
		Route::get('products', ["uses" => "ProductsController@index"]);
		Route::get('search-products', ["uses" => "ProductsController@searchProducts"]);
		
		Route::get('email/{any}', ['uses' => 'EmailBuilderImageLibraryController@noBlade'])->where('any', '.+');
		
		// feedback page
		Route::get('feedback', ['uses' => 'FeedbackController@index']); 
		Route::post('feedback', ['uses' => 'FeedbackController@index']);

		Route::post('about/section-508-compliance', ['uses' => 'EmailController@compliance508']);

	};
	Route::group(['domain' => 'microfocus.{tld}'], $microfocusLangRoutes);
	Route::group(['domain' => '{subdomain}.microfocus.{tld}'], $microfocusLangRoutes);
};
Route::group(array(
	'prefix' => '{lang}',
	'where' => ['lang' => '[a-z-]{2,5}\/?'],
	'middleware' => ['verifyLanguage', 'frameAncestors']
), $needsLangRoutes);
Route::group(array(
	'middleware' => ['verifyLanguage', 'frameAncestors']
), $needsLangRoutes);

// SMRL Routes
Route::get('/sales-marketing-resources', function () { return view('content/sales-marketing-resources/products/advanced_search'); });
Route::get('/sales-marketing-resources/products/view_product/{product_key}/{bu?}', function ($product_key, $bu = '') { 
	$product = \App\Model\Product::where('product_key',$product_key)->get();
	$resources = \App\Model\Section::get_resources($product_key);
	$section_data = \App\Model\Section::get_section($product_key);
	$available_formats = \App\Model\TaxonomyValue::get_available_formats();
	$available_topics = \App\Model\TaxonomyValue::get_available_topics();
	
	if($product->count() > 0) {
		$product = $product->first();
		$bu = $product["business_unit"];
	}
	else {
		// set the layout by bu based on the section
		if (!empty($section_data['business_unit'])) {
			$bu = $section_data['business_unit'];
		} 
		if (empty($bu)) {
			$bu = 'cross_bu';
		}
		
		if ($bu == 'novell' || $bu == 'netiq') $bu = 'microfocus';
	}
	
	return view('content/sales-marketing-resources/products/view_product', compact('product_key', 'product', 'resources', 'section_data', 'available_formats', 'available_topics', 'bu') );
});
Route::get('/sales-marketing-resources/products/mediabin_query', function () { 
	$content = \App\Helpers\MediaBin::searchMetadataCSV(json_decode(\Request::input('data'),true));
	return response($content)
		->header('Content-Type', 'text/csv; charset=utf-8')
		->header('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
		->header('Content-Disposition', 'attachment; filename=export.csv')
		->header('Expires', '0')
		->header('Content-Transfer-Encoding', 'binary')
		->header('Content-Description', 'File Transfer')
		->header('Pragma', 'public');
});
Route::post('/sales-marketing-resources/products/mediabin_query', function () { 
	return json_encode(\App\Helpers\MediaBin::searchMetadata(json_decode(\Request::input('data'),true)));
});
Route::get('/sales-marketing-resources/container/{assetId}/{format?}', function ($assetId, $format = 'html') { 
	$query = array(
	  "assetId"=>$assetId,
	  "returnItems"=>[
	  "Product","Name","Title","Description",env('URL_VAR'),"Format","Business Unit","Asset Type","Language"
	  ],
	  "language"=>	array()
	);

	$results = \App\Helpers\MediaBin::searchContainer($query);
	
	if(env('DOCSOURCE') == "mediabin" && $results->primaryAssetId != $assetId) {
		// redirect to path for parent asset container page
		return redirect('/sales-marketing-resources/container/'.$results->primaryAssetId);
	}

	$assets = array();
	if(env('DOCSOURCE') == "docrep") {
		foreach ( $results as $result ) {
			$assets[$result->language][] = $result;
		}
	}
	else {
		foreach ( $results->results as $result ) {
			$assets[$result->language][] = $result->associatedAssets;
		}
	}
	
	ksort($assets);
	
	if($format == "ajax") {
		return view('content/sales-marketing-resources/container/ajax', compact('assets','assetId') );
	}
	
	return view('content/sales-marketing-resources/container/index', compact('assets','assetId') );
});
Route::get('/sales-marketing-resources/cdnkey', function ($url = null) { 
	$cdnkey = \App\Helpers\MediaBin::generateToken($url);
	return $cdnkey; 
});

// catch all route
Route::get('{any}', ['middleware' => ['verifyLanguage', 'frameAncestors'], 'uses' => 'BaseController@catchAll'])->where('any', '^(?!\.)((?!(\/\.|\.\/|\/\/)).)*(?<!\.)$');
