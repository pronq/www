<?php namespace App\Http\Controllers;

use Lang;
use TrialDownload;
use Request;
use URL;
use App\Helpers\Logging;

class TrialDownloadsController extends Controller {

	/*
	 * The download page & read me pages - pulls all the files, license keys, sys req, read me, etc from web service
	 */
	public function downloadReadme() {

		Lang::setCurrentLang(Lang::pathLang());

		$data = [];
		$template = 'readme';
		$request_path = $this->getProductPathFromRequestPath();

		if( strpos(Request::path(), "readme") === false ) {
			// get translation files
			$data['translate'] = Lang::parseTranslationFile('content::trial-downloads/download-page', 'content::products/' .$request_path . '/trial-download/index');
			$template = 'download-page';

			if(empty($data['translate']['productName']))
				abort(404);
		}

		$data['downloadCode'] = filter_input(INPUT_GET, 'downloadCode', FILTER_SANITIZE_STRING);
		if(!empty($data['downloadCode'])) {
			// pull trial info from TD api for this downloadCode (created with CreateTrial method on form submit & passed through email to download link)
			$trial_info = TrialDownload::getFiles($data['downloadCode']);

			if($trial_info['returnCode'] == 'EXPIRED') {
				$data['expired'] = true;
			}

			$data['product'] = [];
			if(!empty($trial_info['data'])) {
				$data['product'] = $trial_info['data'];

				if(!empty($data['product']['summary']['readMe']) || !empty($data['product']['summary']['description']))
					$data['read_me_url'] = URL::to('/products/' . $request_path . '/trial/download/readme?downloadCode=' . $data['downloadCode']);

			} else {
				// show generic text if no product info came back?
				//	abort(404);
				Logging::info('trial_downloads', 'trial-downloads.log', 'No data received from getFiles for: ' . Request::fullUrl());
			}

			return Lang::view("trial-downloads.{$template}")->with($data);

		} else {
			Logging::info('trial_downloads', 'trial-downloads.log', 'No download code or translation files for: ' . Request::fullUrl());
			abort(404);
		}
	}

	/*
	 *  Handles the landing AND thank you page based upon url passed through the routes.
	 */
	public function landingThankYou() {
		Lang::setCurrentLang(Lang::pathLang());

		$segments = Request::segments();
		$product = $this->getProductPathFromRequestPath();
		$translate = Lang::parseTranslationFile('content::/products/' . $product . '/trial-download/index');

	    if(!empty($product) && !empty($translate) && (end($segments) === 'trial' || end($segments) === 'thanks') && !Request::getQueryString()) {

			$template = 'landing-page';
			$pageType = '_landing';

			if( strpos(Request::path(), "thanks") !== false ) {
				$template = 'thank-you';
				$pageType = '_ty';
			}

            if( !empty($translate['meta_title' . $pageType] ) ){
                $translate[ 'meta_title' ] = $translate['meta_title' . $pageType ];
            }
            if( !empty($translate['meta_description' . $pageType] ) ){
                $translate[ 'meta_description' ] = $translate['meta_description' . $pageType ];
            }
            if( !empty($translate['meta_keywords' . $pageType] ) ){
                $translate[ 'meta_keywords' ] = $translate['meta_keywords' . $pageType ];
            }

            if( array_key_exists('suite-page', $translate) ) {
            	$template = 'suite-landing';
            	return Lang::view("trial-downloads.{$template}")->with(compact('product', 'translate'));
            }

			return Lang::view("trial-downloads.{$template}")->with(compact('product', 'translate'));

		} else {
			abort(404);
		}

    }

	/*
	 * Extracts a product path from the current url request path ie: /products/zenworks/service-desk/trial-download/thanks would return zenworks/service-desk
	 *
	 * @return  string
	 */
	private function getProductPathFromRequestPath() {

		return preg_replace("/^(?:\w\w\-\w\w)?(?:\/)?(?:products\/)(?'product'.+)(?:\/trial)(?:.+)?/", "$1", Request::path());
	}

	/*
	 * Thank you page you get redirected to after successful form submit
	 */
	public function thankYou() {

		Lang::setCurrentLang(Lang::pathLang());

		$product = $this->getProductPathFromRequestPath();

		if(!empty($product)) {
			return Lang::view("trial-downloads.thankyou")->with(array('product' => $product));
		} else {
			abort(404);
		}
	}

	/*
	 * Logs file downloads to td web service
	 */
	public function logDownload() {
		return TrialDownload::logDownload();
	}

	/***** HERITAGE DOWNLOAD PAGES (iFramed content) *****/
	public function download() {

		return Lang::view("trial-downloads.download");
	}

	public function borland_download() {

		return Lang::view("trial-downloads.borland-download");
	}

	public function attachmate_download() {

		return Lang::view("trial-downloads.attachmate-download");
	}

	public function novell_download() {

		return Lang::view("trial-downloads.novell-download");
	}
}