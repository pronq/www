<?php namespace App\Http\Controllers;

use Illuminate\Routing\Route;
use Illuminate\Http\Request;
use Lang;
use Resources;

class FeedbackController extends Controller {
	
	
	public function index(Request $request) {
		
		//$filters = Resources::getFilters();
		
        if( !\App::environment('test','dev') && apcu_exists('products_'.Lang::currentCode()) ) {
            $products = apcu_fetch('products_'.Lang::currentCode());
        } else {
            $products = Lang::parseTranslationFile('content::products/index');
            apcu_store( 'products_'.Lang::currentCode(), $products, getenv('CACHE_LENGTH'));
        }
		
		$success = false;
		$error = array();
		
		$translate = Lang::parseTranslationFile('content::feedback/index');
		$input_prompt = $translate['input_prompt'];
			
		if (isset($_POST['submit'])) {
			// Build the notification message			
			$feedbackType = "Other";
			if (!empty($_POST['type']))  $feedbackType = $_POST['type'];
			
			$intro = "A user has filled out the Feedback form - $feedbackType - on www.microfocus.com\n";
			$intro .= "Their details are as follows:\n\n";
			$message = "";
			
			// validate the form
			if(empty($_POST['fname']) || $_POST['fname'] == $input_prompt) $error["fname"] = true;
			if(empty($_POST['lname']) || $_POST['lname'] == $input_prompt) $error["lname"] = true;
			if(empty($_POST['email']) || $_POST['email'] == $input_prompt) $error["email"] = true;
			if(empty($_POST['type'])) $error["type"] = true;
			if(empty($_POST['desc']) || $_POST['desc'] == $input_prompt) $error["desc"] = true;
			if(empty($_POST['accept'])) $error["accept"] = true;
			
			if (!empty($_POST['fname']))  $message .= "First Name: " .  $_POST['fname'] . "\n";
			if (!empty($_POST['lname']))  $message .= "Last Name: " .  $_POST['lname'] . "\n";
			if (!empty($_POST['email']))  $message .= "Email: " .  $_POST['email'] . "\n";
			if (!empty($_POST['type']))  $message .= "Feedback Type: " .  $_POST['type'] . "\n";
			if (Lang::currentCode() != Lang::getLangConfig("default")) $message .= "Language: " . Lang::currentName() . "\n";
			if ($feedbackType == "Website" && !empty($_POST['url']))  $message .= "Page or Website: " .  $_POST['url'] . "\n";
			if ($feedbackType == "Other" && !empty($_POST['wut']))  $message .= "What are you providing feedback on?: " .  $_POST['wut'] . "\n";
			if ($feedbackType == "Product" && !empty($_POST['product']))  $message .= "Product: " .  $_POST['product'] . "\n";
			if (!empty($_POST['desc']))  $message .= "Feedback Description: " .  $_POST['desc'] . "\n";
			
			if (count($error) == 0) {
				$message = $intro . $message;
				
				$mailheaders = 'Bcc: adam@ekragency.com' . "\r\n";
				
				$to = "entitlements@microfocus.com";
				if (!empty($_POST['type']) && $_POST['type'] == "Website") $to = "microfocus.webmaster@microfocus.com";
			
				//mail ("adam@elikirk.com", "Micro Focus Feedback form - $feedbackType" , $message);
				mail ($to, "Micro Focus Feedback form" , $message, $mailheaders);
				
				$success = true;
			}
		}
		
		
		return Lang::view("feedback/index")->with('products_translate',$products)->with('success', $success)->with('error', $error);
	}
	
	
	
}
