<?php namespace App\Http\Controllers;

use Config;
use Lang;
use Asset;
use Log;
use Route;
use Request;
use URL;

class BaseController extends Controller {
	/*
	  |--------------------------------------------------------------------------
	  | Base Controller
	  |--------------------------------------------------------------------------
	 */



	/*
	 * return third party includes
	 *	name: the name as the asset
	 *	ext - can be js, html, or json = if js then will include a script that pulls in the html
	 *	cache (optional) - results are cached by default for 24 hours
	 */
	public function thirdPartyRequireCss($name, $ext, $cache = true) {
		$apcu_cache_key = "third_party_require_cache_css_{$name}_{$ext}";
		if ($cache == true) {
			if (apcu_exists($apcu_cache_key)) {
				return apcu_fetch($apcu_cache_key);
			}
		} else {
			apcu_delete($apcu_cache_key);
		}
		
		$result = Asset::getCss($name);
		if (!empty($result)) {
			if ($cache == true) {
				apcu_store($apcu_cache_key, $result, getenv('CACHE_LENGTH')); // store for 24 hours
			}
			return $result;
		}
		
		return abort(404);
	}
	
	/*
	 * return third party includes
	 *	name: the name as the asset
	 *	ext - can be js, html, or json = if js then will include a script that pulls in the html
	 *	cache (optional) - results are cached by default for 24 hours
	 */
	public function thirdPartyRequireJs($name, $ext, $cache = true) {
		$apcu_cache_key = "third_party_require_cache_js_{$name}_{$ext}";
		if ($cache == true) {
			if (apcu_exists($apcu_cache_key)) {
				return apcu_fetch($apcu_cache_key);
			}
		} else {
			apcu_delete($apcu_cache_key);
		}
		
		$result = Asset::getJs($name);
		if (!empty($result)) {
			if ($cache == true) {
				apcu_store($apcu_cache_key, $result, getenv('CACHE_LENGTH')); // store for 24 hours
			}
			return $result;
		}
		
		return abort(404);
	}
	
	
	/*
	 * return third party includes
	 *	path is the path to the include - must be base64 encoded
	 *	ext - can be js, html, or json = if js then will include a script that pulls in the html
	 *	cache (optional) - results are cached by default for 24 hours
	 *	lang (optional) = if you don't pass the lang will default to the cookie or default lang
	 */
	public static function thirdPartyRequireInclude($path, $ext, $cache = true, $lang = '') {
		$cache = true;
		// get the lang
		if (empty($lang) || (!empty($lang) && !Lang::isAnyLang($lang))) {
			$lang = Lang::currentCode();
		}
		
		$extra_data = array();
		if (!empty($_GET)) {
			$extra_data = $_GET;
			$cache_key_addition = implode('_', array_map(
					function($v, $k) { 
						return sprintf("%s-%s", $k, $v);
					},
					$extra_data,
					array_keys($extra_data)
			));
		}
		
		// decode the path
		$real_path = base64_decode($path);
		
		URL::forceSchema('https');	
		
		// return cached version if applicable
		$apcu_cache_key = "third_party_require_cache_section_{$real_path}_{$ext}_{$lang}";
		if (!empty($extra_data)) {
			$apcu_cache_key .= "_{$cache_key_addition}";
		}
		
		if ($cache == true) {
			if (apcu_exists($apcu_cache_key)) {
				return apcu_fetch($apcu_cache_key);
			}
		} else {
			apcu_delete($apcu_cache_key);
		}
		
		Lang::setCurrentLang($lang);
		
		// get the include based on the path and cache results
		$result = Lang::parseInclude($real_path, $extra_data);
		
		if (!empty($result)) {

			if ((strpos($real_path, 'main_head') > -1) && isset($_GET['no-jquery']) && !empty($_GET['no-jquery'])) {
				$result = preg_replace('/<script src=".*\/jquery.*\.js"><\/script>/', '', $result);
			}
			
			$result = str_replace("=\"/", "=\"".getenv('DOMAIN_ROOT')."/", $result);

			if ($cache == true) {
				apcu_store($apcu_cache_key, $result, getenv('CACHE_LENGTH')); // store for 24 hours
			}

			return $result;
		}
		
		return abort(404);
	}	

	
//	public function serve_legacy_files() {
//		$filepath = strtok($_SERVER["REQUEST_URI"],'?');
//		if(strpos($filepath, "common") !== false) {
//			$filepath = $_SERVER['DOCUMENT_ROOT'] . "/.." . str_replace("common", "legacy", $filepath);
//			if(file_exists($filepath)) {
//				$fp = fopen($filepath, 'rb');
//				
//				$path_info = pathinfo($filepath);
//				if(isset($path_info['extension'])) {
//					switch ($path_info['extension']) {
//						case "js": $content_type="application/javascript"; break;
//						case "pdf": $content_type="application/pdf"; break;
//						case "exe": $content_type="application/octet-stream"; break;
//						case "zip": $content_type="application/zip"; break;
//						case "doc": $content_type="application/msword"; break;
//						case "xls": $content_type="application/vnd.ms-excel"; break;
//						case "ppt": $content_type="application/vnd.ms-powerpoint"; break;
//						case "gif": $content_type="image/gif"; break;
//						case "png": $content_type="image/png"; break;
//						case "jpe": case "jpeg":
//						case "jpg": $content_type="image/jpg"; break;
//						default: 
//							// get mime type data
//							$finfo = finfo_open(FILEINFO_MIME_TYPE);
//							$content_type = finfo_file($finfo, $filepath);
//							break;
//					}
//				}
//				
//				header("Content-Type: $content_type");
//				header("Content-Length: " . filesize($filepath));
//				fpassthru($fp);
//				exit;
//			}
//		}
//		
//		return abort(404);
//	}
	
	public function clearCache() {
		apcu_clear_cache();
		return "Cleared";
	}
	
	/*
	 * Delete a group of apc cache keys:
	 */
	public function clearCacheKey(Request $request) {
		$keys = json_decode($_POST['keys']);
		if(is_array($keys)) {
			foreach($keys as $key) {
				apcu_delete($key);
			}
		} else {
			apcu_delete($keys);
		}
		return 'cleared';
	}
	
	public function formBuilderTransport() {
		$response = Asset::formBuilderTransport();
		return $response;
	}
	
	public function formLookup($key) {
		$form = new \App\Helpers\FormBuilder();
		dd($form->load_config_cache($key));
	}

	/*
	 * Change the current site language
	 */
	public function changeLanguage($lang, $country_code, $country_name, $url = null) {
		$new_url = $this->baseService->changeLanguage($lang, $country_code, $country_name, $url);
		return redirect($new_url);
	}
	
	/*
	 * Change the current site language
	 */
	public function setCountry($country_code) {
		if ($this->baseService->setCountry($country_code)) {
			return array('success' => false);
		} else {
			return array('success' => true);
		}
	}

	/*
	 * Get countries list for language select popup
	 */
	function getCountriesForLang($lang='en-us') {
		$countries = $this->baseService->getCountriesForLang($lang);
		return $countries;
	}


	/*
	 * This function is hit if no other routes match
	 * The purpose of this function is to serve static views
	 * depending on language - if no file is found 404 is returned
	 */

	public function catchAll() {
		// attemp to find a view that matches our current route
		$view = $this->baseService->getDynamicView();
		if ( $view ) {
			return $view;
		}
		/* set to the same as suse
		// No view found, first try redirecting without the language folder
		if(preg_match('/^(\/\w\w-\w\w)\//','/'.Request::path(),$m)) {
			$eng = str_replace($m[1],'',Request::fullUrl());
			apcu_store('404::'.$eng,true);
			return redirect($eng,301);
		}
		*/
		
		// No view found, see if the content exists in public without language prefix
		if ( preg_match('/^\w\w-\w\w$/',Request::segment(1)) ) {
			$url_parsed = parse_url(Request::fullUrl());
			$try_path = preg_replace('/^\/?\w\w-\w\w\//','/',$url_parsed['path']);
			if ( is_file(public_path($try_path)) ) {
				// Prevent potential infinite redirect (see Middleware/VerifyLanguage)
				apcu_store('404::'.Lang::pathWithoutLang(Request::path()),true);
				
				list( $host, $path ) = explode(Request::segment(1),Request::fullUrl());
				return redirect(URL::toRaw($path),301);
			}
		}
		
		// no view was found, so we throw a 404
		return \App::abort(404);
	}

	/*
	 * Change the current site language
	 */
	public function compileLess() {
		$result = $this->baseService->compileLess();
		dd($result['message']);
	}

	/*
	 * Demandbase IP lookup ajax
	 */
	public function demandbaseData() {
		$demandbase_obj = \App\Helpers\Demandbase::getInstance();
		$demandbase_data = $demandbase_obj->getUserData();
		return $demandbase_data;
	}

	/*
	 * Workaround to avoid caching:
	 */
	public function noCache() {
		$_GET['clear_view_cache'] = true;
		return $this->catchAll();
	}
		

}
