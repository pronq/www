<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Route;
use Illuminate\Http\Request;
use Lang;
use App\Helpers\DocumentationHelper;

//(cgraham) made a few changes so we could move over all legacy doc product landing pages from
// micro focus supportline en masse. Still supports the nice newer style that Brian set up,
// but allows the legacy pages to look okay until/if someone has the chance to update them.
// Also supports a documentation/product/version folder structure where both the product and
// version folders contain a landing page

class DocumentationController extends Controller {

	public function index(Request $request) {
		//process request  - prep it for a documentation product or version page
		$request_path = $request->path();
		$request_path_parts = explode("/", $request_path);
		$first_item = current($request_path_parts);
	
		//Removing the language portion of the path (if we have one) for now.
		//We want all pages to display the English content since that's all we have	currently.
		//Language selection on the pages themselves need to be addressed?
		if($first_item !== 'documentation') {  //going to assume this means we have a language code - for now
			array_shift($request_path_parts);
		}
		
		$last_item = array_pop($request_path_parts);
		
		if(!preg_match('/^(index|documentation)[.](htm|html|php)$/i',$last_item)) {
			array_push($request_path_parts,$last_item);
		}

		$request_path = implode('/',$request_path_parts);
		$canonical_view_path = url($request_path, array(), true);
		array_shift($request_path_parts);//remove 'documentation'
		$doc_path = implode('/',$request_path_parts);
		$hmf_path = DocumentationHelper::get_version_page_path($doc_path) . '/newbury-page.html';//to check for legacy product version page

		$is_product_page = count(explode("/", $doc_path)) == 1;
		$page_type = file_exists($hmf_path) ? 'newbury' : ($is_product_page ? 'product' : 'standard');

		// Grab the content for this product page
		if($page_type == 'newbury') {
			$article = DocumentationHelper::get_hmf_product_body_innerhtml($hmf_path,$doc_path);
		}
		else {
			$article = DocumentationHelper::get_documentation_listings($doc_path);
		
			if(!empty($article)) {
				$vtitle = $is_product_page ? "" : $article['product_name'];
				$article['breadcrumbs'] = DocumentationHelper::get_hmf_breadcrumbs($hmf_path,$doc_path,$vtitle);
				$article['meta_title'] = $article['product_name'] . ' - Documentation';
//				echo '<pre>' . print_r($article, true) . '</pre>';
//				echo '<pre>' . json_encode($article) . '</pre>';
			}
		}
		
		// No content? Throw a 404 page up
		if(empty($article)) {
			return abort('404');
		}
		
		// Set the blade we're going to use
		return view('content::'.Lang::getLangConfig("default").'/documentation/index')->with([
			'translate' => $article,
			'type' => $page_type,
			'canonical_view_path' => $canonical_view_path
			]
		);
	}

//	public function article(Request $request) {
//		$translate = Lang::parseTranslationFile('content::' . Lang::currentCode() . '/' . Lang::getLangLessPathByAnyPath($request->path()));
//		if(empty($translate)) {
//			return abort('404');
//		}
//
//		return Lang::view('documentation/index')->with(array('translate' => $translate));
//	}

}
