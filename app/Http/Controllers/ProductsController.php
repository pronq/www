<?php
namespace App\Http\Controllers;

use Input;
use Lang;
use Request;
use Illuminate\Pagination\LengthAwarePaginator;
use URL;

class ProductsController extends Controller {

    public $products;
    public $translate;


    /*
     * Pulls all products from the cache or from the products translation file
     */
    private function getProducts() {

        if( !\App::environment('test','dev') && apcu_exists('products_'.Lang::pathLang()) ) {
            $this->translate = apcu_fetch('products_'.Lang::pathLang());
        } else {
            $this->translate = Lang::parseTranslationFile('content::products/index');
            apcu_store( 'products_'.Lang::pathLang(), $this->translate, getenv('CACHE_LENGTH'));
        }

        $this->products = $this->translate['products'];
    }

    private static function array_unshift_assoc(&$arr, $key, $val) {
        $arr = array_reverse($arr, true);
        $arr[$key] = $val;
        $arr = array_reverse($arr, true);
        return $arr;
    }

    /*
	 * Products A-Z page
	 */
    public function index() {
        Lang::setCurrentLang(Lang::pathLang());

        if (empty($this->products)) {
            $this->getProducts();
        }


        $productsCol = collect($this->products);


        $input = Input::all();

        $translationFile = Lang::parseTranslationFile('content::products/index');

        // solution filter
        $solutions = array(
            'analytics-and-big-data' => 'Analytics & Big Data',
            'application-development-comma-test-and-delivery' => 'Application Dev, Test & Delivery',
            'business-continuity' => 'Business Continuity',
            'cobol' => 'COBOL',
            'collaboration' => 'Collaboration',
            'information-management-and-governance' => 'Information Management & Governance',
            'it-operations-management' => 'IT Operations Management',
            'mainframe' => 'Mainframe',
            'security' => 'Security',
        );
        asort($solutions);
        self::array_unshift_assoc($solutions, 'all', $translationFile['chooseSolution']);		//'Choose Solution'
        $solutionFilter = isset($input['solution']) ? $input['solution'] : 'all';
        $solFilterParts = ucwords(str_replace('-', ' ', $solutionFilter));
        $solFilterReplacements = array("And" => "&", "Cobol" => "COBOL", "For" => "for", "It" => "IT", " Comma" => ",");
        $solutionFilterForFilter = str_replace(array_keys($solFilterReplacements), array_values($solFilterReplacements), $solFilterParts);

        // information filter
        $informations = array(
            'all' => $translationFile['all'],			// 'Choose Information'
            'freetriallink' => $translationFile['freetriallink'],	//'Trial Downloads'
//			'datasheetlink' => 'FAQs',
            'featurelink' => $translationFile['featurelink'],		//'Features'
            'howtobuylink' => $translationFile['howtobuylink'],		//'How to Buy'
            'techspeclink' => $translationFile['techspeclink'],		//'Tech Specs'
        );
        $informationFilter = isset($input['information']) ? $input['information'] : 'all';
        if (!isset($informations[$informationFilter])) {
            $informationFilter = 'all';
        }


        // letter filter
        $letters = array_combine(range('a', 'z'), range('A', 'Z'));
        $letterFilter = isset($input['letter']) ? $input['letter'] : 'all';
        if (!isset($letters[$letterFilter])) {
            $letterFilter = 'all';
        }


        // get search param
        $query = isset($input['q']) ? $input['q'] : '';


        // get page #
        $page = LengthAwarePaginator::resolveCurrentPage();
        if(empty($page)) $page = 1;


        $filteredProductsCol = $productsCol->filter(function($item) use($solutionFilterForFilter) {
            if ($solutionFilterForFilter === 'All') {
                return true;
            } else {
                return isset($item['solution']) && $item['solution'] == $solutionFilterForFilter;
            }
        })->filter(function($item) use($informationFilter) {
            if ($informationFilter === 'all') {
                return true;
            } else {
                return !empty($item[$informationFilter]);
            }
        })->filter(function($item) use($letterFilter) {
            if ($letterFilter === 'all') {
                return true;
            } else {
                return !empty($item['title']) && strtolower($item['title'][0]) == $letterFilter;
            }
        })->filter(function($item) use($query) {
            if (empty($query)) {
                return true;
            } else {
                return stripos($item['title'], $query) !== false || stripos($item['description'], $query) !== false;
            }
        });


        ///////////////////////////////////////////
        // setup the pagination
        $limit = 15;
        $total = count($filteredProductsCol);
        $last_page = round($total / $limit);
        $offset = ($page-1) > $last_page ? $last_page : ($page-1);
        $page = $offset + 1;
        $currFilteredProducts = $filteredProductsCol->slice($offset * $limit, $limit)->all();
        $filteredProducts = new LengthAwarePaginator($currFilteredProducts, count($filteredProductsCol), $limit);
        $curr_lang = Lang::onValidPath();
        if ( empty($curr_lang) ) {
            $curr_lang = Lang::currentCode();
        }
        $filteredProducts->setPath('/' . $curr_lang . '/products');
        foreach ($input as $name => $curr_input) {
            if ($name == 'page') { continue; }
            $filteredProducts->addQuery($name, $curr_input);
        }

        $canonical_view_path_arr = array();
        if (!empty($solutionFilter) && $solutionFilter != 'all') {
            $canonical_view_path_arr['solution'] = $solutionFilter;
        }
        if (!empty($informationFilter) && $informationFilter != 'all') {
            $canonical_view_path_arr['information'] = $informationFilter;
        }
        if (!empty($letterFilter) && $letterFilter != 'all') {
            $canonical_view_path_arr['letter'] = $letterFilter;
        }
        if (!empty($query)) {
            $canonical_view_path_arr['q'] = $query;
        }
        if (!empty($page)) {
            $canonical_view_path_arr['page'] = $page;
        }

        $canonical_view_path = URL::canonical($canonical_view_path_arr);

        return Lang::view("products/index")->with(compact('filteredProducts', 'page', 'totalPages', 'solutions', 'informations', 'letters', 'solutionFilter', 'informationFilter', 'letterFilter', 'query', 'canonical_view_path'));
    }

    /*
	 * Returns a JSON array of products whose titles match the search parameter.
	 */
    public function searchProducts() {

        Lang::setCurrentLang(Lang::pathLang());

        if (empty($this->products)) {
            $this->getProducts();
        }

        $returnProducts = array();

        if (isset($_GET['search']) && strlen($_GET['search']) > 0) {
            foreach($this->products as $prod) {
                if (stripos($prod['title'], $_GET['search']) !== false) $returnProducts[] = $prod;
            }
            if (count($returnProducts) == 0) return '{"Error": "No results found"}';
            return json_encode($returnProducts);
        }

        return json_encode($this->products);

    }

}
