<?php
namespace App\Http\Controllers;

use Input;
use Lang;
use Illuminate\Http\Request;

class SearchResultsController extends Controller {

    public function getSearchResultsPage(Request $request,$path='index') {
		
		// remove '@' symbol from search term if email address - violates google terms of service
		if (!empty($request->input('term'))) {
			$pattern = '/([a-zA-Z0-9_\.-]+)@([\da-zA-Z\.-]+)\.([a-zA-Z\.]{2,6})/';
			preg_match($pattern, $request->input('term'), $matches);
			if (!empty($matches)) {
				$term = str_replace('@', '', $request->input('term'));
				return redirect($request->path().'?term='.$term);
			}
		}
        
        $search_path = '/search';
        $google_cse_id = env('GOOGLE_CSE_ID');
        
        if (env('SITE') == 'microfocus' && $path == 'communities') {
            $google_cse_id = env('COMMUNITIES_CSE_ID');
            $search_path .= '/'.$path;
        }
        
        $lang_code = Lang::currentLang();
        
        $lang = substr($lang_code, 0, 2);
        if ($lang_code == 'zh-tw') {
          $lang = 'zh-Hant';
        }
        else if ($lang_code == 'zh-cn') {
          $lang = 'zh-Hans';
        }

		$request_path = preg_replace('/\.html$/', '', $request->path());
 		$canonical_view_path = url($request_path, array(), true);
		$canonical_view_path = preg_replace('/(\/|^)index$/','$1',preg_replace('/\.(html|php)$/','',$canonical_view_path));
		if(isset($_GET['term'])) {
			$canonical_view_path .= '?term='.$_GET['term'];
		}
		
        return Lang::view('search/index')->with('lang', $lang)->with('gcse_id', $google_cse_id)->with('search_path', $search_path)->with('canonical_view_path', $canonical_view_path);
    }
}