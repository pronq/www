<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Route;
use Illuminate\Http\Request;
use Lang;
use App\Helpers\PressRoomHelper;

class PressRoomArticlesController extends Controller {

    public function index(Request $request) {
        
        $basedir = PressRoomHelper::get_basedir();
		
		// get and sort listings 
        $inthenews = PressRoomHelper::get_press_listings('in-the-news');
        $articles = PressRoomHelper::get_press_listings('article');
        $opinion = PressRoomHelper::get_press_listings('opinion');

        // get the first element of each listings
        if(isset($inthenews)) $recent_news['inthenews'] = reset($inthenews);
        if(isset($articles)) {
            $recent_year_articles = reset($articles);
            if(isset($recent_year_articles[0])) $recent_news['articles'] = $recent_year_articles[0];
        };
        if(isset($opinion)) $recent_news['opinion'] = reset($opinion);

        return view('content::'.Lang::getLangConfig("default").'/about/press-room/index')->with(['recent_news'=>$recent_news]);
    }
	
	public function articlesList(Request $request) {
		
		$basedir = PressRoomHelper::get_basedir();
		
		// get listings 
		$inthenews = PressRoomHelper::get_press_listings('in-the-news');
		$opinion = PressRoomHelper::get_press_listings('opinion');
		$articles = PressRoomHelper::get_press_listings('article');

		$request_path = preg_replace('/\.html$/', '', $request->path());
 		$canonical_view_path = url($request_path, array(), true);
		$canonical_view_path = preg_replace('/(\/|^)index$/','$1',preg_replace('/\.(html|php)$/','',$canonical_view_path));
        return view('content::'.Lang::getLangConfig("default").'/about/press-room/articles')->with(['inthenews'=>$inthenews,'opinion'=>$opinion,'articles'=>$articles,'canonical_view_path' => $canonical_view_path]);
    }
	
    public function feed(Request $request) {
		// read through the directories, build the listing from English then translate into current language
		
		$basedir = PressRoomHelper::get_basedir();

        $sort = function($a,$b) {
            $adate = strtotime($a['article_date']);
            $bdate = strtotime($b['article_date']);
            return $adate == $bdate ? 0 : ($adate < $bdate ? 1 : -1);
        };

        $articles = array();
        foreach(scandir($this->basedir.'/article') as $dir) {
            if(strpos($dir,'.')===false) {
                $articles[$dir] = array();
                foreach(scandir($basedir.'/article/'.$dir) as $file) {
                    if(preg_match('/^(.+)\.html$/',$file,$m)) {
                        $path = 'about/press-room/article/'.$dir.'/'.$m[1];
                        $translate = Lang::parseTranslationFile('content::'.$path);
                        $translate['article_link'] = '/'.$path;
                        $articles[$dir][] = $translate;
                    }
                }
                usort($articles[$dir], $sort);
            }
        }
        krsort($articles);

//        spit out data in xml rss format
        echo '<rss version="2.0"><channel><title>Micro Focus Press Channel</title><link>http://www.microfocus.com</link><description>Get the latest on Micro Focus.</description><language>en-us</language>';
        foreach($articles as $article) {
            foreach ($article as $item) {
                echo '<item>';
                echo '<title>'.$item['article_title'].'</title>';
                echo '<pubDate>'.$item['article_date'].'</pubDate>';
                echo '<description>'.$item['meta_description'].'</description>';
                echo '<link>http://www.microfocus.com'.$item['article_link'].'</link>';
                echo '</item>';
            }
        }
        echo '</channel></rss>';
        exit;
    }

    public function article(Request $request) {
        $translate = Lang::parseTranslationFile('content::' . Lang::currentCode() . '/' . Lang::getLangLessPathByAnyPath($request->path()));
		if(empty($translate)) {
			return abort('404');
		}

		return Lang::view('PressRoomArticles/index')->with(array('translate' => $translate));
    }

}
