<?php
namespace App\Http\Controllers;

use Input;
use Lang;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use URL;
use File;
use Response;

class PartnersController extends Controller {

    public $projects;
    public $translate;

    /*
     * Pulls all projects from the cache or from the projects translation file
     */
    private function getProjects() {
	
		$lang = Lang::currentLang();

        if( !\App::environment('test','dev') && apcu_exists($lang.'_projects') ) {
            $this->translate = apcu_fetch($lang.'_projects');
        } else {
            $this->translate = Lang::parseTranslationFile('content::partners/app-rehost-providers/project-matrix');
            apcu_store( $lang.'_projects', $this->translate, getenv('CACHE_LENGTH'));
        }

        $this->projects = $this->translate['migrationprojects'];
    }
	
	/*
	 * Donload all projects in the matrix
	 */
	public function downloadAll(Request $request) {
		if (empty($this->projects)) {
			$this->getProjects();
		}

		$storage_path = storage_path();
		$download_dir_path = $storage_path.'/download_files';
		if (!file_exists($download_dir_path)) {
			$result = File::MakeDirectory($download_dir_path);
		}
		$file_name = $download_dir_path.'/project_matrix.csv';
		if (file_exists($file_name)) {
			unlink($file_name);
		}
		$header_arr = array(
			'"Company"',
			'"Country"',
			'"Market"',
			'"Orig Environment"',
			'"New Environment"',
			'"Details"',
		);
		
		File::append($file_name, implode(',', $header_arr)."\n");
		foreach ($this->projects as $project => $val) {
			$line_arr = array(
				'"'.$val['companyname'].'"',
				'"'.$val['country'].'"',
				'"'.$val['market'].'"',
				'"'.$this->clean_tabs_newline($val['origenvironment']).'"',
				'"'.$this->clean_tabs_newline($val['newenvironment']).'"',
				'"'.$this->clean_tabs_newline($val['details']).'"',
			);
			File::append($file_name, implode(",", $line_arr)."\n");
		}
		
		return Response::download($file_name, 'project_matrix.csv', ['Content-Type: text/csv']);

	}
	
	public function clean_tabs_newline($string) {
		$clean_string = preg_replace('/[\r\n]/',',',$string);
		$clean_string = strip_tags($clean_string);
		$clean_string = preg_replace('/\t{1,}+/', '', $clean_string);
		$clean_string = trim($clean_string);
		$clean_string = preg_replace('/^,/', '', $clean_string);
		$clean_string = rtrim($clean_string, ',');
		$clean_string = preg_replace('/,,/',',',$clean_string);
		$clean_string = str_replace('"', '""', $clean_string);
		return $clean_string;
	}

    /*
     * Partners App Rehost Providers Project Matrix page
     */
    public function projectMatrix(Request $request) {

        if(empty($this->projects)) {
            $this->getProjects();
        }

        $projectsCol = collect($this->projects);
        $input = Input::all();

        $markets = array(
            'address-management' => 'Address Management',
            'agriculture' => 'Agriculture',
            'auto-transport' => 'Auto Transport',
            'aviation' => 'Aviation',
            'banking' => 'Banking',
            'district-health-insurance' => 'District Health Insurance',
            'education' => 'Education',
            'financial-services' => 'Financial Services',
            'government' => 'Government',
            'human-services-for-disabled' => 'Human Services for Disabled',
            'industrial' => 'Industrial',
            'insurance' => 'Insurance',
            'logistics' => 'Logistics',
            'manufacturing' => 'Manufacturing',
            'medical' => 'Medical',
            'medical-healthcare' => 'Medical/Healthcare',
            'news-agency' => 'News Agency',
            'other' => 'Other',
            'software-services' => 'Software Services',
            'telecoms' => 'Telecoms',
            'utilities' => 'Utilities',
            'wholesale-retail' => 'Wholesale/Retail',
        );
        asort($markets);
        $markets = array('all' => 'Choose Market') + $markets;
        $marketFilter = isset($input['market']) ? $input['market'] : 'all';
        $marketFilterParts = ucwords(str_replace('-', ' ', $marketFilter));
        $marketFilterReplacements = array("And" => "&amp;", "For" => "for", "Wholesale Retail" => "Wholesale/Retail", "Medical Healthcare" => "Medical/Healthcare");
        $marketFilterForFilter = str_replace(array_keys($marketFilterReplacements), array_values($marketFilterReplacements), $marketFilterParts);

        $countries = array(
            'africa' => 'Africa',
            'australia' => 'Australia',
            'austria' => 'Austria',
            'belgium' => 'Belgium',
            'brazil' => 'Brazil',
            'canada' => 'Canada',
            'denmark' => 'Denmark', 
            'france' => 'France',
            'germany' => 'Germany',
            'greece' => 'Greece',
            'holland' => 'Holland',
            'hungary' => 'Hungary',
            'india' => 'India',
            'indonesia' => 'Indonesia',
            'ireland' => 'Ireland',
            'israel' => 'Israel',
            'italy' => 'Italy',
            'japan' => 'Japan',
            'luxembourg' => 'Luxembourg',
            'malaysia' => 'Malaysia',
            'mexico' => 'Mexico',
            'netherlands' => 'Netherlands',
            'norway' => 'Norway',
            'n-ireland' => 'N Ireland',
            'not-disclosed' => 'Not Disclosed',
            'oman' => 'Oman',
            'pakistan' => 'Pakistan',
            'poland' => 'Poland',
            'portugal' => 'Portugal',
            'rsa' => 'RSA',
            'singapore' => 'Singapore',
            'spain' => 'Spain',
            'sweden' => 'Sweden',
            'switzerland' => 'Switzerland',
            'thailand' => 'Thailand',
            'turkey' => 'Turkey',
            'uk' => 'UK',
            'usa' => 'USA',
            'zimbabwe' => 'Zimbabwe'
        );

        asort($countries);
        $countries = array('all' => 'Choose Country') + $countries;
        $countriesFilter = isset($input['country']) ? $input['country'] : 'all';
        $countriesFilterParts = ucwords(str_replace('-', ' ', $countriesFilter));
        $countriesFilterReplacements = array("And" => "and", "Burkina Faso" => "Burkina-Faso", "Guinea Bissau" => "Guinea-Bissau", "Rsa" => "RSA", "Uk" => "UK", "Usa" => "USA");
        $countriesFilterForFilter = str_replace(array_keys($countriesFilterReplacements), array_values($countriesFilterReplacements), $countriesFilterParts);

        // get page #
        $page = LengthAwarePaginator::resolveCurrentPage();
        if(empty($page)) $page = 1;

        // filter the data
        $filteredProjectsCol = $projectsCol->filter(function($item) use($marketFilterForFilter) {
            if ($marketFilterForFilter === 'All') {
                return true;
            } else {
                return isset($item['market']) && $item['market'] === $marketFilterForFilter;
            }
        })->filter(function($item) use($countriesFilterForFilter) {
            if ($countriesFilterForFilter === 'All') {
                return true;
            } else {
                return isset($item['country']) && $item['country'] === $countriesFilterForFilter;
            }
        });

        // setup the pagination
        $limit = 15;
        $total = count($filteredProjectsCol);
        $last_page = round($total / $limit);
        $offset = ($page-1) > $last_page ? $last_page : ($page-1);
        $page = $offset + 1;
        $currFilteredProjects = $filteredProjectsCol->slice($offset * $limit, $limit)->all();
        $filteredProjects = new LengthAwarePaginator($currFilteredProjects, count($filteredProjectsCol), $limit);
        $curr_lang = Lang::onValidPath();
        if ( empty($curr_lang) ) {
            $curr_lang = Lang::currentCode();
        }
        $filteredProjects->setPath('/' . $curr_lang . '/partners/app-rehost-providers/project-matrix');
        foreach ($input as $name => $curr_input) {
            if ($name == 'page') { continue; }
            $filteredProjects->addQuery($name, $curr_input);
        }

        $canonical_view_path_arr = array();
        if (!empty($marketFilter) && $marketFilter != 'all') {
            $canonical_view_path_arr['market'] = $marketFilter;
        }
        if (!empty($countriesFilter) && $countriesFilter != 'all') {
            $canonical_view_path_arr['country'] = $countriesFilter;
        }
        if (!empty($page)) {
            $canonical_view_path_arr['page'] = $page;
        }
        $canonical_view_path = URL::canonical($canonical_view_path_arr);

        return view('content::en-us/partners/app-rehost-providers/project-matrix')->with(compact('filteredProjects', 'page', 'totalPages', 'markets', 'marketFilter', 'countries', 'countriesFilter', 'canonical_view_path'));

    }

}
