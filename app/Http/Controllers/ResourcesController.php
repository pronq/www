<?php
namespace App\Http\Controllers;

use Input;//allows you to read the html request variables
use Lang;
use Resources;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use URL;

class ResourcesController extends Controller {

    public function index() {

		$input = Input::all();//This gets all input the the request
		$resources = "";

		// if(!(isset($input['product']) || isset($input['solution']) || isset($input['type']) || isset($input['kewords']))){

			$productFilter = isset($input['product']) ? $input['product'] : 'all';
			$solutionFilter = isset($input['solution']) ? $input['solution'] : 'all';
			$typeFilter = isset($input['type']) && $input['type'] !== 'all' ? explode(",", ucwords(str_replace('-', ' ', $input['type']))) : 'all';
			$langFilter = isset($input['language']) ? $input['language'] : 'all';
			$keywordsFilter = isset($input['keywords']) ? $input['keywords'] : '';

			$query = '';
			if(empty($input['product']) && empty($input['solution']) && empty($input['type']) && empty($input['keywords']) && empty($input['language'])){
				$query["recent"] = "3";
				//echo "recent";
			}

			// product filter
			if($productFilter !== 'all') {
				$query["product"] = $productFilter;
			}

			// solution filter
			if($solutionFilter !== 'all') {
				$query["solution"] = $solutionFilter;
			}

			// type filter
			if($typeFilter !== 'all') {
				if($typeFilter === 'Faq') { $typeFilter = 'FAQ'; }
				$query["format"] = $typeFilter;
			}

			//lang filter
			if($langFilter !== 'all' && $langFilter !== 'en-us'){
				$query["language"] = $langFilter;
				Lang::setCurrentLang($langFilter);
			}

			//Keywords filter
			if(isset($keywordsFilter) && !empty($keywordsFilter)){
				$query["keyword"] = $keywordsFilter;
			}
			// dd($query);
			// get resources matching $query data
			//apc_clear_cache();

			$resourcesValues = Resources::getAll($query);
			// dd($resourcesValues);
			$resourcesValues = collect($resourcesValues)->collapse();
			//$resources = collect($resourcesArr)->sortBy('title', SORT_NATURAL|SORT_FLAG_CASE)->groupBy('type');
			//dd($resourcesValues);

			// get page #
			$page = LengthAwarePaginator::resolveCurrentPage();

			if(empty($page)) $page = 1;

			$limit = 20;
			$total = count($resourcesValues);
			$last_page = round($total / $limit);

			$offset = ($page - 1) > $last_page ? $last_page : ($page - 1);
			$page = $offset + 1;
			$currFilteredResources = $resourcesValues->slice($offset * $limit, $limit)->all();

			$resources = new LengthAwarePaginator($currFilteredResources, count($resourcesValues), $limit);
			// echo count($resources);
			$curr_lang = Lang::onValidPath();
			if ( empty($curr_lang) ) {
				$curr_lang = Lang::currentCode();
			}
			$resources->setPath('/' . $curr_lang . '/resources');

			if(!empty($page)){
				$input['page'] = $page;
			}

		//}
		// get filters
		$filters = Resources::getFilters();


        $data = array("resources" => $resources, 'input' => $input, 'resource_view' => Lang::view('resources/resource')->getName(), 'filters' => $filters);

        return Lang::view("resources/index")->with($data);
    }

    public function newIndex() {
    	$input = Input::all();	
    }

    /*
     * Resource gate page
     */
    public function gate(Request $request) {
        $translate = Lang::parseTranslationFile('content::'.$request->path());
		if(empty($translate)) {
			return view('errors.404');
		}
		$thankyou = preg_replace('/\/asset|white-paper\//','/thank-you/',$request->path());
		$resource = Resources::getById($translate['id']);

		if(is_array($resource) && isset($resource['expired_id'])){
			$parameters = 'origin_url='.$request->fullUrl();
			$parameters = $parameters."&id=".$resource['expired_id'];
			return redirect("/request-a-resource/?".$parameters); // resource not found so redirect to /request-a-resource/ page
		} 

		if(empty($translate['email_content'])) {
			$translate['email_content'] = $translate['thanks_content'];
		}

		$view = empty($translate['blade']) ? 'resources/gate' : $translate['blade'];
		return Lang::view($view)->with(['translate'=>$translate,'thankyou'=>$thankyou,'resource'=>$resource]);
    }

    /*
     * Resource thank-you page
     */
    public function thankyou(Request $request) {
		Lang::setCurrentLang(Lang::pathLang());
		$asset = preg_replace('/\/thank-you\//','/asset/',$request->path());
        $translate = Lang::parseTranslationFile('content::/resources/thank-you','content::'.$asset);
		if(empty($translate['thanks_content'])) {
			return view('errors.404');
		}
		$resources = Resources::getByIds($translate['id']);

		return Lang::view('/resources/thank-you')->with(['translate'=>$translate,'resources'=>$resources]);
    }

    /*
     * Resource subfolder gate page. Example: /resources/white-paper/byod
     */
    public function subgate(Request $request) {
        $translate = Lang::parseTranslationFile('content::'.$request->path());
		if(empty($translate)) {
			return view('errors.404');
		}
		// converts url "/resources/white-paper/byod" to "/resources/white-paper/thank-you/byod"
		$thankyou = preg_replace('/resources\/(.+)\/(.+)/','resources/$1/thank-you/$2',$request->path());
		$resource = Resources::getById($translate['id']);

		if(is_array($resource) && isset($resource['expired_id'])){
			$parameters = 'origin_url='.$request->fullUrl();
			$parameters = $parameters."&id=".$resource['expired_id'];
			return redirect("/request-a-resource/?".$parameters); // resource not found so redirect to /request-a-resource/ page
		} 

		if(empty($translate['email_content'])) {
			$translate['email_content'] = $translate['thanks_content'];
		}

		$view = empty($translate['blade']) ? 'resources/gate' : $translate['blade'];
		return Lang::view($view)->with(['translate'=>$translate,'thankyou'=>$thankyou,'resource'=>$resource]);
    }

    /*
     * Resource subfolder thank-you page. Example: /resources/white-paper/thank-you/byod
     */
    public function subthankyou(Request $request) {

		// since no translation file matches the url of this page, tell the system to translate anyway:
		Lang::setCurrentLang(Lang::pathLang());
		
		// converts url "/resources/white-paper/thank-you/byod" to "/resources/white-paper/byod"
		$asset = preg_replace('/\/thank-you\//','/',$request->path());
        
		// load default translation file and override with values from the translation file of the gate page
		$translate = Lang::parseTranslationFile('content::/resources/thank-you','content::'.$asset);
		if(empty($translate['thanks_content'])) {
			return view('errors.404');
		}
		$resources = Resources::getByIds($translate['id']);

		return Lang::view('/resources/thank-you')->with(['translate'=>$translate,'resources'=>$resources]);
    }

    /*
     * Promo thank-you page
     */
    public function promoThankyou(Request $request) {
		Lang::setCurrentLang(Lang::pathLang());
		$promo = preg_replace('/\/thank-you\//','/',$request->path());
        $translate = Lang::parseTranslationFile('/resources/thank-you','content::'.$promo);
		if(empty($translate['thanks_content'])) {
			return view('errors.404');
		}
		$resources = Resources::getByIds($translate['id']);

		return Lang::view('/resources/thank-you')->with(['translate'=>$translate,'resources'=>$resources]);
    }

    /*
     * Campaign thank-you page
     */
    public function campaignThankyou(Request $request) {
		Lang::setCurrentLang(Lang::pathLang());
		$promo = preg_replace('/\/thank-you\//','/download/',$request->path());
        $translate = Lang::parseTranslationFile('/resources/thank-you','content::'.$promo);
		if(empty($translate['thanks_content'])) {
			return view('errors.404');
		}
		$resources = Resources::getByIds($translate['id']);

		return Lang::view('/resources/thank-you')->with(['translate'=>$translate,'resources'=>$resources]);
    }

    /*
     * Campaign gate page
     */
    public function campaignGate(Request $request) {
        $translate = Lang::parseTranslationFile('content::'.$request->path());
		if(empty($translate)) {
			return view('errors.404');
		}

		$thankyou = URL::to(preg_replace('/\/download\//','/thank-you/',$request->path()));
		$email_content = empty($translate['email_content']) ? $translate['thanks_content'] : $translate['email_content'];
		
		$resource = Resources::getById($translate['id']);
		if(is_array($resource) && isset($resource['expired_id'])){
			$parameters = 'origin_url='.$request->fullUrl();
			$parameters = $parameters."&id=".$resource['expired_id'];
			return redirect("/request-a-resource/?".$parameters); // resource not found so redirect to /request-a-resource/ page
		} 
		
		$download = URL::to($resource->url);
		if(empty($download)) $email_content = null;

		$request_path = preg_replace('/\.html$/', '', $request->path());

		$viewPath = 'content::en-us/'.$request->path();
		//if (Lang::currentCode() != Lang::getLangConfig("default")) $viewPath = 'content::' . str_replace(Lang::currentCode(), 'en-us', $request->path());
		return Lang::view(Lang::pathWithoutLang($request_path))->with(['email_content'=>$email_content,'thankyou'=>$thankyou,'download'=>$download]);
    }




}
