<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Route;
use Illuminate\Http\Request;
use Lang;

class SuccessStoryController extends Controller {

    public function story(Request $request) {
		$request_path = preg_replace('/\.html$/', '', $request->path());
		$translate = Lang::parseTranslationFile('content::' . Lang::currentCode() . '/' . Lang::getLangLessPathByAnyPath($request_path));
		if(empty($translate)) {
			return abort('404');
		}

		return view('content::'.Lang::getLangConfig("default").'/success/stories')->with(array('translate' => $translate));
    }
}
