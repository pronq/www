<?php namespace App\Http\Controllers;

use Lang;
use Config;
use Illuminate\Http\Request;

class CommunitiesController extends Controller {

    /*
     * Grabs data from feeds in the feeds config and adds them to the communities page
     */
    public function index(Request $request) {
		Lang::setCurrentLang(Lang::pathLang());

        $feeds = Config::get('feeds');
        $feedData = [];

        foreach($feeds as $feed => $url) {
            // look for cached feed data
            $data = apcu_fetch( $feed );
            if(empty($data)) {
                //  get feed data
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 5);
                curl_setopt($ch, CURLOPT_TIMEOUT, 5); //timeout in seconds
                $feedString = trim(curl_exec($ch));
                curl_close($ch);

                if(!empty($feedString)) {
                    // parse xml into object
                    libxml_use_internal_errors(false);
                    $xml = simplexml_load_string($feedString, 'SimpleXMLElement', LIBXML_NOCDATA);

                    $feedPosts = [];
                    $idx = 0;
                    foreach($xml->channel->item as $item) {
                        // stop at 2 posts per feed
                        if($idx > 1)
                            break;

                        $itemObj = new \stdClass();
                        $namespaces = $item->getNameSpaces(true);
                        if(isset($namespaces['dc'])) {
                            $dc = $item->children($namespaces['dc']);
                            $itemObj->author = (string) $dc->creator;
                        }
                        $itemObj->title = (string) $item->title;
                        $itemObj->link = (string) $item->link;
                        $itemObj->pubDate = (string) $item->pubDate;

                        // look for image in description
                        preg_match('/(<div class="postAvatar"><img .+ \/><\/div>)/', $item->description, $matches);

                        if(count($matches) > 1) {
                            // we have an image
                            $itemObj->image = (string) $matches[0];
                            $item->description = str_replace($matches[0], '', (string) $item->description);
                        }
                        // trim to 5 ish lines
                        $itemObj->description = rtrim(strip_tags(substr((string) $item->description, 0, strrpos(substr((string) $item->description, 0, 194), ' '))), ', ');

                        $feedPosts[] = (object) $itemObj;
                        $idx++;
                    }
                    $feedData[$feed] = $feedPosts;
                    // store the feed in the cache
                    apcu_store( $feed, serialize($feedData[$feed]), getenv('CACHE_LENGTH')); // store for one day
                }
            } else {
                $feedData[$feed] = unserialize($data);
            }
        }

        $data = array("feedData" => $feedData, "feedView" => 'section.communities.feed');

        return Lang::view("communities.index")->with($data);
    }

}
