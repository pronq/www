<?php
namespace App\Http\Controllers;

use Lang;

class TranslateMessageController extends Controller {
	
	public function GetTranslatedMessage(Request $request) {
		
		if (!empty($_GET['lang'])) {
			Lang::setCurrentLang($_GET['lang']);
		}

		return Lang::view('TranslationMessage/index');
	}
	
}

?>