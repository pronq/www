<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Route;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Lang;

class SupportController extends Controller {


	// Copied from BaseController to mimic catchAll for $this->filter
	protected $baseService;
	public function __construct(\App\Services\BaseService $base_service) {
		$this->baseService = $base_service;
	}

	
	public function index(Request $request) {
		
		$translate = Lang::parseTranslationFile('content::support-and-services/index');

		usort($translate['products'],function($a,$b) {
			return strnatcmp($a['name'], $b['name']);
		});

		$solutions = array(); // sort products into solution buckets
		foreach ($translate['products'] as $i=>$product) {
			if(!empty($product['solution'])) {
				$solutions[$product['solution']][] = $product['name'];
			}
			if(!empty($product['courses-link']) || !empty($product['on-demand-link']) || !empty($product['certification-link'])) {
				if (!isset($translate['products'][$i]['training-link'])) {
					$translate['products'][$i]['training-link'] = '/training#'.rawurlencode($product['name']);
				}
			}
			if(!empty($product['tech-handbook-link'])&& strpos($product['tech-handbook-link'], ".pdf") == false) {
				$translate['products'][$i]['tech-handbook-link'] = $product['tech-handbook-link'] . '#' . rawurlencode($product['name']);
			}
			if(!empty($product['customer-support-handbook-link']) && strpos($product['customer-support-handbook-link'], ".pdf") == false) {
				$translate['products'][$i]['customer-support-handbook-link'] = $product['customer-support-handbook-link'] . '#' . rawurlencode($product['name']);
			}

		}
		ksort($solutions);

		$data = array("products" => $translate["products"], 'solutions' => $solutions);

        return view('content::en-us/support-and-services/index')->with($data);
	}
	
	public function filter(Request $request, $filter) {
		//dd($request, $filter);
		// Since this route was written as a catch all, first check if it belongs to another view:
		$view = $this->baseService->getDynamicView();
		if ( $view ) {
			return $view;
		}
		
		// for some reason URL params aren't working in our route setup:
		$filter = $request->segment(2);
		if($filter == 'support-and-services') {
			$filter = $request->segment(3);
		}

		$translate = Lang::parseTranslationFile('content::support-and-services/index');
		
		if(empty($filter) || !isset($translate['links-'.$filter])) {
			return abort('404');
		}
        //dd($translate['products']);
		foreach ($translate['products'] as $i=>$product) {
			if(!isset($product[$filter.'-link'])) {
				unset($translate['products'][$i]);
			}
			else if(!empty($product['customer-support-handbook-link']) && strpos($product['customer-support-handbook-link'], ".pdf") == false) {
				$translate['products'][$i]['customer-support-handbook-link'] = $product['customer-support-handbook-link'] . '#' . rawurlencode($product['name']);
			}


		}

		usort($translate['products'],function($a,$b) {

			return strnatcmp($a['name'], $b['name']);
		});
        
		$solutions = array(); // sort products into solution buckets
		foreach ($translate['products'] as $i=>$product) {
			if(!empty($product['solution'])) {
				$solutions[$product['solution']][$product['name']] = $product[$filter.'-link'];
			}
		}
		ksort($solutions);

		//dd($solutions);

		$data = array("support_translate" => $translate, 'solutions' => $solutions, 'filter' => $filter);
        
        return view('content::en-us/support-and-services/filter')->with($data);
	}
	
	public function training(Request $request) {
		
		$translate = Lang::parseTranslationFile('content::support-and-services/index');
		
		$solutions = array(); // sort products into solution buckets
		foreach ($translate['products'] as $i=>$product) {
			if(empty($product['courses-link']) && empty($product['on-demand-link']) && empty($product['certification-link'])) {
				unset($translate['products'][$i]);
			}
			elseif(!empty($product['solution'])) {
				$solutions[$product['solution']][] = $product['name'];
			}
		}
		ksort($solutions);

		usort($translate['products'],function($a,$b) {
			return strnatcmp($a['name'], $b['name']);
		});

		$data = array("support_translate" => $translate, 'solutions' => $solutions);

        return view('content::en-us/training')->with($data);
	}
	
	public function handbook(Request $request) {

		$productjson = json_decode(file_get_contents(public_path('../resources/translations/includes/en-us/support/handbook/portfolios.json')))->products;
		$products = array();
		foreach($productjson as $product) {
			$products[$product->portfolio][] = $product;
		}
		foreach($products as $i=>$product) {
			usort($products[$i],function($a,$b) {
				return strnatcasecmp($a->name,$b->name);
			});
		}

		$contentjson = file_get_contents(public_path('../resources/translations/includes/en-us/support/handbook/handbook.json'));
		$contentjson = json_decode(preg_replace('/<img src=\\\\"img\/([^"]+)\\\\"/','<img src=\\"/assets/images/Handbook/$1\\"',$contentjson),true);


		//Remove the language in the path
		$tempUrl = explode('/', $request->path());
		if(preg_match("/^\w{2}-\w{2}$/", $tempUrl[0])){
			unset($tempUrl[0]);
		}
		$tempUrl = implode('/', $tempUrl);
		// $tempUrl = '/' . $tempUrl;
		//dd('content::en-us/' . $tempUrl);

        return view('content::en-us/' . $tempUrl)->with(array('products'=>$products,'content'=>$contentjson));
	}

}
