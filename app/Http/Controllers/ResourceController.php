<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Route;
use Illuminate\Http\Request;
use Lang;

class ResourceController extends Controller {

    public function index(Request $request, $subdomain, $tld, $type, $resource) {
		
		$type = $this->getResourceTypeFromRequestPath($request);

		$request_path = preg_replace('/\.html$/', '', $request->path());
		$translate = Lang::parseTranslationFile('content::' . Lang::currentCode() . '/' . Lang::getLangLessPathByAnyPath($request_path));
		
		if(empty($translate)) {
			return abort('404');
		}

		return view('content::'.Lang::getLangConfig("default")."/resources/{$type}/index")->with(array('translate' => $translate));
    }
	
	/*
	 * Extracts a resource type from the current url request path ie: es-es/resources/flash-point-papers/data-center-workload-migration-made-easy would return flash-point-papers
	 *
	 * @return  string
	 */
	private function getResourceTypeFromRequestPath($request) {

		return preg_replace("/^(?:\w\w\-\w\w)?(?:\/)?(?:resources\/)(.+)\/.+/", "$1", $request->path());
	}
}
