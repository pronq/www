<?php
namespace App\Http\Controllers;

//use Input;
use File;
use Illuminate\Http\Request;
use Lang;

class EmailBuilderImageLibraryController extends Controller {
    
    // file types that can be returned (uses extension)
    private $allowed_file_types = array(
        'jpg',
        'png',
        'gif',
        'svn',
        'jpeg',
    );

    // directories that aren't allowed
    private $disallowed_dirs = array(
        '/',
        '.',
        '..'
    );
    
    
    // determine where to get listing from, cache or fresh
    public function getListing($this_dir = '') {
        if (isset($_GET['clear_cache'])) {
            apcu_delete($this_dir);
        }
        if (apcu_exists($this_dir)) {
            return response()->json(apcu_fetch($this_dir));
        }
        else {
            $dir_list = $this->getFreshListing($this_dir);
            apcu_store($this_dir, $dir_list, env('IMAGE_LIBRARY_CACHE_TTL'));
            return response()->json($dir_list);
        }
    }

    // get new file listing 
    private function getFreshListing($this_dir='') {
        
        $orig_dir = $this_dir;
        $this_dir = str_replace("_", "/", $this_dir);
        if (in_array($this_dir, $this->disallowed_dirs) || in_array($this_dir, $this->disallowed_dirs)) return;
        if (strpos($this_dir, '../') || strpos($this_dir, './') || strpos($this_dir, 'root')) return;
        if (!empty($this_dir)) $this_dir .= '/';
        
        $current_dir = $image_library_root = public_path() . env('IMAGE_LIBRARY_ROOT') . $this_dir;
        
        $files = File::files($current_dir);
        if (count($files) < 1) {
            $files = File::directories($current_dir);
        }
        sort($files);
        
        $dir_list = [];
        $dir_count = $image_count = 0;
        
        foreach ($files as $file) {
            $filesystem = new \Illuminate\Filesystem\FileSystem();
           
            $file_type = 'directory';
            if (is_dir($file)) {
                $item = [
                    'url' => env('IMAGE_LIBRARY_ROOT') . $this_dir . $filesystem->name($file),
                    'name' => $filesystem->name($file),
                    'type' => 'directory',
                ];
                $dir_count++;
            }
            
            else if (is_file($file)) {
                if (!in_array($filesystem->extension($file), $this->allowed_file_types)) continue;
                $file_type = 'file';
                $filename = $filesystem->name($file) . '.' . $filesystem->extension($file);
                $item = [
                    'url' => env('IMAGE_LIBRARY_ROOT') . $this_dir . $filename,
                    'name' => $this->format_name($filesystem->name($file)),
                    'type' => $filesystem->extension($file),
                    'filesize' => round($filesystem->size($file) / 1024, 2, PHP_ROUND_HALF_UP) . ' KB',
                    'last_modified' => date('Y-m-d H:i:s', $filesystem->lastModified($file)),
                ];
                $image_count++;
            }
            
            array_push($dir_list, $item);
        }
        
        $tmp_array = [
            'file_type' => $file_type,
            'image_count' => $image_count,
            'dir_count' => $dir_count,
            'image_type' => substr($this_dir, 0, strpos($this_dir, '/')),
            'image_class' => substr(strtolower($this_dir), 0, strpos($this_dir, '/')),
            'files' => $dir_list,
        ];
        
        return $tmp_array;
        
    }
    
    // format file name for display
    private function format_name($item) {
        return ucwords(preg_replace('/\..*$/', '', str_replace('_', ' ', $item)));
    }

	/*
	 * Just deliver the translation file as is without blade:
	 */
	public function noBlade(Request $request) {
//		$curr_lang = Lang::onValidPath();
//		if ( empty($curr_lang) ) {
//			$curr_lang = Lang::currentCode();
//		}
		$request_path = preg_replace('/\.html$/', '', $request->path());
		$request_path = preg_replace('/^\/?\w\w(-\w\w)?\//', '/', $request_path);
			
		$file = public_path('../resources/translations/content/' . Lang::currentLang() . Lang::pathWithoutLang($request_path) . '.html');
		
		if ( !file_exists($file) ) {
			$file = public_path('../resources/translations/content/' . Lang::getLangConfig("default") . Lang::pathWithoutLang($request_path) . '.html');
		}
		
		if ( !file_exists($file) ) {
			abort(404);
		}
		return file_get_contents($file);
	}
    
}