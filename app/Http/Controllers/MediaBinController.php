<?php namespace App\Http\Controllers;

use Illuminate\Routing\Route;
use Illuminate\Http\Request;
use Lang;
use Resources;

class MediaBinController extends Controller {
	
	
	public function index(Request $request, $assetID = NULL, $backupAssetID = NULL) {
		
		if ($assetID == Lang::currentLang()) $assetID = $backupAssetID;
        
		
		$query = array(
		  "assetId"=>$assetID,
		  "returnItems"=>[
		  "Product","Name","Title","Description",env('URL_VAR'),"Format","Business Unit","Asset Type","Language"
		  ],
		  "language"=>	array(),
		);

		$results = \App\Helpers\MediaBin::searchContainer($query);

		// $assetData will contain all the information about the asset.
		$assetData = array();
		foreach ( $results->results as $result ) {
			$assetData[$result->language][] = $result->associatedAssets;
		}
			
		
		
		return Lang::view("media/container/index")->with('assetData',$assetData);
	}
	
	
	
}
