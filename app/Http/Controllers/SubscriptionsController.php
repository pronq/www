<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Route;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Helpers\Eloqua;
use Lang;
/*
Form Name: EmailSubscriptionCenter -> third form
Post URL: action="https://s1065104700.t.eloqua.com/e/f2"
<input type="hidden" name="elqFormName" value="EmailSubscriptionCenter">
<input type="hidden" name="elqSiteID" value="1065104700">
"Opt-In" | "Opt-Out"
'emailAddress',
'country',
'Collaboration',
'COBOL_Development_&_Mainframe',
'Data_Center_Management',
'Endpoint_Management',
'File_&_Network_Services',
'Identity_&_Access_Management',
'Security_Management',
'Terminal_Emulation',
'Application_Deliver_&_Testing',
'Workload_Migration_&_Disaster_Recovery'
*/

class SubscriptionsController extends Controller {

    static public $fieldNames = array('collaboration' => 'Collaboration',
									'cdam' => 'COBOL_Development_&_Mainframe',
									'dcm' => 'Data_Center_Management',
									'em' => 'Endpoint_Management',
									'fans' => 'File_&_Network_Services',
									'iaam' => 'Identity_&_Access_Management',
									'sm' => 'Security_Management',
									'te' => 'Terminal_Emulation',
									'adat' => 'Application_Deliver_&_Testing',
									'wmadr' => 'Workload_Migration_&_Disaster_Recovery',
									'borland_technical_blog' => 'borland_technical_blog',
									'mf_technical_blog' => 'mf_technical_blog',
									'netiq_cool_solutions' => 'netiq_cool_solutions',
									'mf_cool_solutions' => 'mf_cool_solutions',
									'Information_Archiving' => 'Information_Archiving'
									);
	static public $fieldIds = array('100324' => 'collaboration',
									'100326' => 'cdam',
									'100325' => 'dcm',
									'100327' => 'em',
									'100328' => 'fans',
									'100329' => 'iaam',
									'100330' => 'sm',
									'100331' => 'te',
									'100332' => 'adat',
									'100333' => 'wmadr',
									'100358' => 'netiq_cool_solutions',
									'100365' => 'mf_cool_solutions',                  //100359
									'100360' => 'borland_technical_blog',
									'100361' => 'mf_technical_blog',
									'100366' => 'Information_Archiving'
									);
	
	public function index(Request $request) {
		Lang::setCurrentLang(Lang::pathLang());
		$e = new Eloqua();
		$toSelect = array();
		$contact = '';
		$email = '';
		
		// check for recID in the querystring, from an unsubscribe link
		if (!empty($request->has('recID'))) {
			$pattern = '/[A-Z]{5}0{2,11}(.*)/';
			$result = preg_match($pattern, $request->input('recID'), $matches);
			$visitorID = (!empty($matches[1]) ? $matches[1] : null);
			if (!empty($visitorID)) {
				$contact = $e->get_contact_info_byid($visitorID);
				$email = $contact->emailAddress;
				$toSelect = $this->format_toSelect_data($contact);
			}
		}
		// see if mf_email cookie exists and use that to query for contact information
		else if($request->cookie('mf_email')) {
			$contact = $e->get_contact_info($request->cookie('mf_email'));
			if(count($contact->elements) && isset($contact->elements[0]->fieldValues)) {
				$contact = $contact->elements[0];
				$email = $contact->emailAddress;
				$toSelect = $this->format_toSelect_data($contact);
			}
		}
		// if SID cookie exits query based on that 
		// google has SID cookie as well
		else if ($request->cookie('SID')) {
			$profile = $e->get_profile_by_guid($request->cookie('SID'));
			
			if (count($profile->elements) && isset($contact->elements[0]->visitorId)) {
				$visitorId = $contact->elements[0]->visitorID;
				$contact = $e->get_contact_info($visitorId);
				if (count($contact->elements) && isset($contact->elements[0]->fieldValues)) {
					$contact = $contact->elements[0];
					$email = $contact->emailAddress;
					$toSelect = $this->format_toSelect_data($contact);
				}
			}
		}

		$translate = Lang::parseTranslationFile('content::company/subscription');

        $data = array("translate" => $translate, 'toSelect' => $toSelect, 'email' => urldecode($email));

        return Lang::view('company/subscription')->with($data);
		
	}
	
	public function lookup(Request $request) {
		$errors = array();
		if(!$request->has('emailAddress') || !filter_var($request->input('emailAddress'), FILTER_VALIDATE_EMAIL)) {
			$errors[] = 'emailAddress';
		}
        
		if(count($errors)) {
			return json_encode(array('status'=>'error','fields'=>$errors));
		}
		
		$toSelect = array();
		$email = '';
		$e = new Eloqua();
		$contact = $e->get_contact_info($request->input('emailAddress'));

		if(count($contact->elements) && isset($contact->elements[0]->fieldValues)) {		
			$contact = $contact->elements[0];
			$email = $contact->emailAddress;
			$toSelect = $this->format_toSelect_data($contact);
		}
		
		return json_encode(array('status'=>'success','toSelect'=>$toSelect, 'email' => $email));
		
	}
	
	// put code into own function so can be used for all calls
	private function format_toSelect_data($contact) {
		$toSelect[] = 'uncheckAll';
		foreach($contact->fieldValues as $fv) {
			if (isset(static::$fieldIds[$fv->id]) && isset($fv->value)) {
				if ($fv->value == 'Opt-In') {
					//$toSelect[] = static::$fieldIds[$fv->id].'_in';
					$toSelect[] = 'checkAll'; // just one flag to indicate that you are subscribed
					break;
				}
			}
		}
		return $toSelect;
	}
	
	public function lookup_by_id(Request $request) {
		$e = new Eloqua();
		$contact = $e->get_contact_info_byid($request->input('id'));
		
		if(count($contact->elements) && isset($contact->elements[0]->fieldValues)) {	
			$toSelect = $this->format_toSelect_data($contact);
		}
		return json_encode(array('status'=>'success', $contact));
	}
	
    public function submit(Request $request) {
		$errors = array();
		if(!$request->has('emailAddress') || !filter_var($request->input('emailAddress'), FILTER_VALIDATE_EMAIL)) {
			$errors[] = 'emailAddress';
		}
        
		if(count($errors)) {
			return json_encode(array('status'=>'error','fields'=>$errors));
		}
		
		$form_data = array();

		// Subscribe or unsubscribe to all
		if ( $request->has('checkAll') ) {
			foreach(static::$fieldNames as $key => $name) {
				if(($key == 'borland_technical_blog')||($key == 'mf_technical_blog')||($key == 'netiq_cool_solutions')||($key == 'mf_cool_solutions')){
					$form_data[$name] = "";
				}
				else{
					$form_data[$name] = "Opt-In";
				}
			}
		}
		else if ( $request->has('uncheckAll') ) {
			foreach(static::$fieldNames as $key => $name) {
				$form_data[$name] = "Opt-Out";
			}
		}
		else {
			foreach(static::$fieldNames as $key => $name) {
				$form_data[$name] = "null";
			}
		}

		if ($request->has('utm_campaign')) {
			$form_data['opt-out_email_name'] = $request->input('utm_campaign');
		}

        $form_data['elqFormName'] = "EmailSubscriptionCenter";
        $form_data['elqSiteID'] = env('ELOQUA_SITE_ID') ?? '1065104700';
        $form_data['deliveryEmail'] = "8867";
        $form_data['emailAddress'] = $_REQUEST['emailAddress'];
		if (isset($_REQUEST['country']) && !empty($_REQUEST['country'])) {
			$form_data['country'] = $_REQUEST['country'];
		}

		$this->submit_form_to_eloqua($form_data);
		
		$response = new Response(json_encode(array('status'=>'success')));
		$response->withCookie(cookie()->forever('mf_email', $form_data['emailAddress']));
		return $response;
		//return json_encode(array('status'=>'success'));
    }

	private function submit_form_to_eloqua($form_data) {
		$url = env('ELOQUA_POST_URL') ?? 'https://secure.eloqua.com/e/f2.aspx';
		$timeout = 10;
		
		$post_string = http_build_query($form_data);
		$parts = parse_url($url);

		$fp = fsockopen($parts['host'], isset($parts['port']) ? $parts['port'] : 80, $errno, $errstr, $timeout);

		$out = "POST " . $parts['path'] . " HTTP/1.1\r\n";
		$out.= "Host: " . $parts['host'] . "\r\n";
		$out.= "Content-Type: application/x-www-form-urlencoded\r\n";
		$out.= "Content-Length: " . strlen($post_string) . "\r\n";
		$out.= "Connection: Close\r\n\r\n";
		if (isset($post_string)) {
			$out .= $post_string;
		}

		fwrite($fp, $out);
		fclose($fp);

		return true; // this async call is ignoring output of call.
	}
}
