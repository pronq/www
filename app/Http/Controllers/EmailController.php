<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use URL;

class EmailController extends Controller {
	
    public function compliance508(Request $request) {
		$main_email = env('508_EMAIL_ADDRESS');
        
		$errors = array();
		if(!$request->has('email') || !filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)) {
			$errors[] = 'email';
		}
		$required = array('firstName','lastName','company','phone','city','country','w_optin','products','accessibilityDoc','docDate');
		foreach($required as $field) {
			if(!$request->has($field)) {
				$errors[] = $field;
			}
		}
		
		if(count($errors)) {
			return json_encode(array('status'=>'error','fields'=>$errors));
		}
		
        $headers = array(
            'From' => "microfocus.com <MKTDigitalMarketing@microfocus.com>",
            'MIME-Version' => '1.0',
            'Content-Type' => 'text/plain; charset="utf-8"'
        );

        // Convert the array of header data into a single string.
        $headers_string = '';
        foreach($headers as $header_name => $header_value) {
            if(!empty($headers_string)) {
                $headers_string .= "\r\n";
            }
            $headers_string .= $header_name . ': ' . $header_value;
        }
		
		
		$message_string = 
            "First Name: ".$request->input('firstName')."\r\n" .
            "Last Name: ".$request->input('lastName')."\r\n" .
            "Company Name: ".$request->input('company')."\r\n" .
            "Phone: ".$request->input('phone')."\r\n" .
            "City: ".$request->input('city')."\r\n" .
            "Country: ".$request->input('country')."\r\n" .
            "Email: ".$request->input('email')."\r\n" .
            "Products: ".$request->input('products')."\r\n" .
            "Accessibility document: ".$request->input('accessibilityDoc')."\r\n" .
            "Document date: ".$request->input('docDate')."\r\n" .
            "Micro Focus Salesperson: ".$request->input('salesperson')."\r\n" .
            "Comments: ".$request->input('comments')."\r\n" .
            "";
		
		mail($main_email,"Section 508 VPAT Request",$message_string,$headers_string);
		
		return json_encode(array('status'=>'success'));
    }

    /*
     * This is a generic function that you can send a form to and it will send an email
     * Required hidden fields:  to - the email address you want to send to
     *                          subject - subject of the email you want to send
     *                          ty_page_url - thank you page url to redirect to after email is sent successfully
     * Optional hidden fields:  required - a list of required fields separated by a comma i.e. "first_name,last_name"
     *                          email_intro - a brief explanation of the purpose of the email
     * Building the email message assumes field names are lowercase with underscores in between field names that have multiple words ie: first_name
     * @use routes.php /send-form
     */
    public function send(Request $request) {

        $errors = array();
        $inputs = $request->all();

        if(isset($inputs['google_script_url']) && isset($inputs['google_spreadsheet_id']) && isset($inputs['google_sheet_name'])){
            $response = json_decode($this->postToGoogleSheet($inputs['google_script_url'], $inputs));
            unset($inputs['google_script_url']);
            unset($inputs['google_spreadsheet_id']);
            unset($inputs['google_sheet_name']);
            if($response->result == 'error'){
                abort(500);
            }
        }

        if(isset($inputs['required'])){
            $required = explode(',',$inputs['required']);
            array_push($required,'to','subject','ty_page_url');
        }
        else{
            $required = array('to','subject','ty_page_url');
        }
        
		foreach($required as $field) {
			if(!$request->has($field)) {
				$errors[] = $field;
			}
		}
		
		if(count($errors)) {
			return json_encode(array('status'=>'error','fields'=>$errors));
        }
        
        // TODO: add CSRF PROTECTION SO SOMEONE CAN'T JUST HIT THIS URL

        $to = $inputs['to'];
        $subject = $inputs['subject'];
        $ty_page_url = $inputs['ty_page_url'];

        unset($inputs['to']);
        unset($inputs['subject']);
        unset($inputs['ty_page_url']);
        unset($inputs['required']);

        $headers = array(
            'From' => "microfocus.com <MKTDigitalMarketing@microfocus.com>",
            'MIME-Version' => '1.0',
            'Content-Type' => 'text/html; charset="utf-8"'
        );

        // Convert the array of header data into a single string.
        $headers_string = '';
        foreach($headers as $header_name => $header_value) {
            if(!empty($headers_string)) {
                $headers_string .= "\r\n";
            }
            $headers_string .= $header_name . ': ' . $header_value;
        }

        //Send an email confirmation if all email confirmation inputs are set in the post request
        if(isset($inputs['email']) && (isset($inputs['email_confirmation_subject']) && !empty($inputs['email_confirmation_subject'])) && (isset($inputs['email_confirmation_message']) && !empty($inputs['email_confirmation_message']))){
            $inputs['email_confirmation_message'].= "<br><br>Sincerely,<br>Micro Focus";
            mail($inputs['email'],$inputs['email_confirmation_subject'],$inputs['email_confirmation_message'],$headers_string);
            unset($inputs['email_confirmation_subject']);
            unset($inputs['email_confirmation_message']);
        }

        // build the email message from the input fields
        $message_string = '';
        if(isset($inputs['email_intro'])){
            $message_string .= $inputs['email_intro']."<br>";
            unset($inputs['email_intro']);
        }
        foreach($inputs as $input_name => $value) {
            if (strpos($input_name, '_') !== false) {
                $input_name_parts = explode('_', $input_name);
                $i = 0;
                $len = count($input_name_parts);
                foreach($input_name_parts as $part) {
                    $message_string .= ucfirst($part) . ' ';
                    if ($i == $len - 1) {
                        rtrim($message_string);
                    }
                    $i++;
                }
                $message_string .= ': ' . $value . "<br>";
            } else {
                $message_string .= ucfirst($input_name) . ': ' . $value . "<br>";
            }
        }

        mail($to,$subject,$message_string,$headers_string);
        
        return redirect($ty_page_url);
    }

    public function postToGoogleSheet($scriptUrl, $data){

        /*  1. Create a new Google Sheet
            First, go to Google Sheets and Start a new spreadsheet with the Blank template.
            Rename it Email Subscribers. Or whatever, it doesn't matter.
            Put the following headers into the first row:
                A	        B	    C	...
            1	timestamp	email	...	

            2. Create a Google Apps Script
            Click on Tools > Script Editor… which should open a new tab.
            Rename it Submit Form to Google Sheets. Make sure to wait for it to actually save and update the title before editing the script.
            Now, delete the function myFunction() {} block within the Code.gs tab.
            Paste the following script in it's place and File > Save:

            function doPost (e) {
  
                var spreadSheetId = e.parameter['google_spreadsheet_id'];
                var sheetName = e.parameter['google_sheet_name'];
                
                var lock = LockService.getScriptLock()
                lock.tryLock(10000)

                try {
                    var doc = SpreadsheetApp.openById(spreadSheetId);
                    var sheet = doc.getSheetByName(sheetName)

                    var headers = sheet.getRange(1, 1, 1, sheet.getLastColumn()).getValues()[0]
                    var nextRow = sheet.getLastRow() + 1

                    var newRow = headers.map(function(header) {
                    return header === 'timestamp' ? new Date() : e.parameter[header]
                    })

                    sheet.getRange(nextRow, 1, 1, newRow.length).setValues([newRow])

                    return ContentService
                    .createTextOutput(JSON.stringify({ 'result': 'success', 'row': nextRow }))
                    .setMimeType(ContentService.MimeType.JSON)
                }

                catch (e) {
                    return ContentService
                    .createTextOutput(JSON.stringify({ 'result': 'error', 'error': e }))
                    .setMimeType(ContentService.MimeType.JSON)
                }

                finally {
                    lock.releaseLock()
                }
            }

            3. Add a new project trigger
            Click on Edit > Current project’s triggers.
            In the dialog click No triggers set up. Click here to add one now.
            In the dropdowns select doPost
            Set the events fields to From spreadsheet and On form submit
            Then click Save
            4. Publish the project as a web app
            Click on Publish > Deploy as web app….
            Set Project Version to New and put initial version in the input field below.
            Leave Execute the app as: set to Me(your@address.com).
            For Who has access to the app: select Anyone, even anonymous.
            Click Deploy.
            In the popup, copy the Current web app URL from the dialog.
            And click OK.

            paste web app URL in google_script_url hidden input
            set spreadsheet id in google_spreadsheet_id hidden input
            set sheet name in google_sheet_name hidden input

        */

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $scriptUrl);

        // return transfer as string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        $response = curl_exec($ch);

        // store the response info including the HTTP status
        // 400 and 500 status codes indicate an error
        $responseInfo = curl_getinfo($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        return $response;
    }


}
