<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers;
use App\User;
use Illuminate\Support\Facades\Auth;


class Authenticate {
    
    public function handle($request, Closure $next, $guard = null) {
        // This code will grab the Access Manager headers and create a user
        // session variable, as well as an is_logged_in session variable. These
        // headers will only be present on the servers once a user has authenticated
        // through Access Manager. It will never be present on a local environment.
        // These headers will also never be present in ngnix because it uses
        // the getallheaders function. You can spoof these headers by using a 
        // browser add on.
        
        if (function_exists('getallheaders')) {
            //if (!\Session::has('user')) {
                session(['is_logged_in' => false]);
                // access manager headers
                $am_headers = [
                    'X-webidsynchid',
                    'X-firstname',
                    'X-middlename',
                    'X-lastname',
                    'X-phone',
                    'X-email',
                    'X-address1',
                    'X-address2',
                    'X-address3',
                    'X-city',
                    'X-state',
                    'X-zip',
                    'X-country',
                    'X-company',
                    'X-contact_by_phone',
                    'X-contact_by_email',
                    'X-contact_by_mail',
                    'X-entitlementgranted',
                ];

               $headers = getallheaders();

            $user_headers = [];

            foreach ($am_headers as $header) {
                if (isset($headers[$header])) {
                    $user_headers[str_replace('X-', '', $header)] = $headers[$header];
                }
            }

            session(['user' => $user_headers]);

            if (isset($user_headers['webidsynchid']) && !empty($user_headers['webidsynchid']) ) {
                session(['is_logged_in' => true]);
                $user_full_name = ucfirst(strtolower($user_headers['firstname'])) . " " . ucfirst(strtolower($user_headers['lastname']));
                session(['username' => $user_full_name]);
            }

            //dd(session('user'));
        }

        return $next($request);
    }
}
