<?php

namespace App\Http\Middleware;

use Closure;
use Lang;
use Request;
use Route;
use URL;
use Cookie;

class VerifyLanguage {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {

		// if lang session vars aren't set, use ip to lookup and set w/demandbase
		if(!Cookie::get('user_language')) {
			$dbrequest = Request::create('demandbase-data', 'GET');
			$dbresponse = json_decode(Route::dispatch($dbrequest)->getContent());
			if(isset($dbresponse->lang) && $dbresponse->lang){
				$lang = $dbresponse->lang;
			}
			else{
				$lang = 'en-us';
			}
			Cookie::queue('user_language', $lang, 604800, null, '.microfocus.com', false, false); // store 1 week
		}

		$pathLang = Lang::pathLang();

		if ( $pathLang ) {

			$url = $request->fullUrl();

			// check for paths that don't need a language ie: selfreg
			$path_data = Lang::isLanglessPath($pathLang, $request->fullUrl());
			if($path_data['langless_path']) {
				$pathLang = 'en-us';
				$url = $path_data['url'];
			}

			
			// if a primary language, update the language in the session
			if ( Lang::isMainLang($pathLang) ) {
				session(['lang' => $pathLang]);
				Cookie::queue('user_language', $pathLang, 604800, null, '.microfocus.com', false, false);
			}

			// if en-us redirect to english
			if ( $pathLang == Lang::getLangConfig("default") ) {
				// remove 'en-us' from the URL if present
				$path = str_replace('en-us/', "", $url);
				return redirect(URL::toRaw($path));
			}
		} else {
			// if they have a language in their cookie, take them to that version instead
			if ( $request->cookie('user_language') && $request->cookie('user_language') != Lang::getLangConfig("default") ) {
				// prevent infinite 404 redirect
				if ( !apcu_exists('404::'.Lang::pathWithoutLang($request->path())) ) { 
					$url_parsed = parse_url($request->fullUrl());
					$path = '/'.$request->cookie('user_language');
					$path .= !empty($url_parsed['path']) ? $url_parsed['path'] : '/';
					$path .= !empty($url_parsed['query']) ? "?{$url_parsed['query']}" : '';
					return redirect(URL::toRaw($path));
				}
			}
		}
		return $next($request);
	}

}