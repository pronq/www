<?php

namespace App\Http\Middleware;

use Closure;
use Lang;
use Request;

class AddFrameAncestorsPolicy
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		$path = trim(Lang::pathWithoutLang($request->path()), '');
		$frameAncestors = true;

		// check if current route is excluded
		if (is_file(public_path('../resources/views/frame_ancestors_exclusions.php'))) {
			$exclusions = include public_path('../resources/views/frame_ancestors_exclusions.php');
			foreach($exclusions as $index => $pattern){
				if(substr($pattern, 0, 1) !== '/'){
					$pattern = '/'.$pattern;
				}
				if(substr($pattern, -1) === '/'){
					$pattern = substr($pattern, 0, -1);;
				}
				elseif(substr($pattern, -1) === '*'){
					$pattern = str_replace('/*','.*',$pattern);
				}
				$pattern = str_replace('/','\/',$pattern);

				if(preg_match('/^'.$pattern.'$/', $path)){
					$frameAncestors = false;
				}
			}
		}
		$response = $next($request);
		if($frameAncestors){
			// add CSP frame-ancestors header to the response
			$response = $response->header('Content-Security-Policy', "frame-ancestors 'self' ".env('FRAME_ANCESTORS_SOURCES').';');
		}
		return $response;
    }
}
