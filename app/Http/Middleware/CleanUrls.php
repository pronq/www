<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use URL;

class CleanUrls {
	
	
	public function __construct() {
		$this->dirty_paths = Config::get('dirty_urls.paths');
	}
	
	
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixedg
     */
    public function handle($request, Closure $next) {
		
		// only handle get requests
		if ($request->method() != 'GET') {
			return $next($request);
		}
		
		$url = $request->getPathInfo();
		
		// don't handle paths from config/dirty_paths.php
		foreach ($this->dirty_paths as $dp) {
			$pattern = $dp; 
			if (preg_match($pattern, $url)) {
				return $next($request);
			}
		}
		
		$original_path = $request->getPathInfo();
		$new_path = preg_replace('/(\/|^)index$/', '$1', preg_replace('/\.(html|php)$/', '', $original_path));
		
		if (!preg_match('/(\/|^)$/', $new_path)) {
			if (!preg_match('/\.\w+$/',$new_path)) {
				$new_path .= '/';
			}
		}
		
		if ($new_path != $original_path) {
			if ($query = $request->getQueryString()) {
				$new_path .= '?'.$query;
			}
			return redirect(URL::to($new_path), 301);
		}
		// Don't allow pages to have /index.php/ at the beginning of their URL
		if ( stripos(URL::to($new_path),'/index.php/') !== false ) {
			$url = preg_replace('/\/index.php\//','/',URL::to($new_path));
			if ($query = $request->getQueryString()) {
				$url .= '?'.$query;
			}
			return redirect($url, 301);
		}

		return $next($request);
    }
	
}
