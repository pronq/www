<?php

namespace App\Helpers;

use Lang;
use URL;

class DocumentationHelper {

	public static function get_basedir() {
		return public_path('../resources/translations/content/' . Lang::getLangConfig("default") . '/documentation');
	}

	// set the html file that we're looking for ("documentation")
	public static function get_documentation_listings($type) {
		$list = Lang::parseTranslationFile("content::documentation/${type}/documentation");
		return $list;
	}

	public static function get_resources_doc_path() {	
		return public_path('../resources/translations/content/' . Lang::currentLang() . '/documentation');
	}

	public static function get_english_resources_doc_path() {	
		return public_path('../resources/translations/content/en-us/documentation');
	}
	
	public static function get_previous_element($dom_n) {	
		$psib = $dom_n ? $dom_n->previousSibling : NULL;

		while($psib && $psib->nodeType != XML_ELEMENT_NODE) {
			$psib = $psib->previousSibling;
		}
		
		return $psib;
	}
	
	//(cgraham) return path string representing a possible legacy doc version page from supportline
	//All these will be named 'newbury-page.html'
	//Version pages may have links to localized content but we don't have language specific
	//product or version pages. If a language is selected on the site we will show the en-us content
	//while preserving the site's language setting
	public static function get_version_page_path($path) {	
		$path = Lang::pathWithoutLang($path);
		$file = DocumentationHelper::get_english_resources_doc_path() . $path;//English only for now!
		return $file;
	}
	
	private static function get_hmf_product_name($path,$doc_path,$is_product_page) {	
		$parts = explode("/",$path);
		array_pop($parts);//remove html page
		
		if(!$is_product_page)
			array_pop($parts);//and the version folder if neccessary
			
		$json_file = implode("/",$parts) . "/product.json";
		
		if (file_exists($json_file)) {
			$str = file_get_contents($json_file);
			$json = json_decode($str, true);
			return $json['name'];
		}
		
		return "Product";
	}

	//$version_title is empty ? productpage : versionpage
	public static function get_hmf_breadcrumbs($path,$doc_path,$version_title) {
		$is_product_page = empty($version_title);//handle product and version pages
		$doc_parts = explode("/",$doc_path);
		$prod = DocumentationHelper::get_hmf_product_name($path,$doc_path,$is_product_page);
		
		$breadcrumbs = [
			["name" => "Support & Services", "url" => "/support-and-services/"],
			["name" => "Documentation", "url" => "/documentation/"]
		];
		
		if($is_product_page)
			$breadcrumbs[] = ["name" => $prod ?: "Product"];
		else {
			$breadcrumbs[] = ["name" => $prod ?: "Product", "url" => '/documentation/' . array_shift($doc_parts) . '/'];
			$breadcrumbs[] = ["name" => $version_title];
    }
  		
		return $breadcrumbs;
	}

/*
	We're using legacy microfocus doc product version pages as is moved over from supportline.
	This processes the body content of those pages to make them function and look a
	little better after they're moved onto microfocus.com.
	
	Had also planned to move all deliverable content (.pdf, .chm, .jar ...) from supportline to microfocus.com.
	However, supportline is a Windows server and we found all sorts of case inconsistencies amongst links,
	paths and filenames. So we're leaving that stuff on supportline for now. In here we change relative
	links to content on supportline to full paths so they'll work from microfocus.com.
	
	Return string of html body content from the supplied html file in $path.
*/	
	public static function get_hmf_product_body_innerhtml($path,$doc_path) {
		$ret = array();
		$html = '';
		$page_title = '';
		
		if (file_exists($path)) {
			$doc = new \DOMDocument();
			libxml_use_internal_errors(true);
			$html = mb_convert_encoding(file_get_contents($path),'HTML-ENTITIES','UTF-8');
			
			if (!empty($html) && $doc->loadHTML($html)) {
				$h1 = $doc->getElementsByTagName('h1')->item(0);
				
				if($h1) {
					$page_title =  $h1 ? preg_replace('/ Documentation$/', '', trim($h1->textContent)) : "Product";
					$h1->parentNode->removeChild($h1);
				}
				
				$tblone = $doc->getElementsByTagName('table')->item(0);
				
				if($tblone) {
					$psib = DocumentationHelper::get_previous_element($tblone);
					
					if(!$psib || strcasecmp($psib->tagName,"h3")) { //checking for an existing <h3>
						$new_h3 = $doc->createDocumentFragment();
						$new_h3->appendXML('<h3>Product Documentation</h3>');
						$tblone->parentNode->insertBefore($new_h3,$tblone);
					}
				}
				
				$images = $doc->getElementsByTagName('img');
				
				foreach ($images as $image) { //have a new location for these old references
					$img_parts = explode("/",$image->getAttribute('src'));
					$img_path = array_pop($img_parts);
					$image->setAttribute('src', "/documentation/_images/" . $img_path);
				}
				
				$anchors = $doc->getElementsByTagName('a');
				
				foreach ($anchors as $anchor) {
					$href = $anchor->getAttribute('href');
					//on supportline, all deliverable content is located under 'books'
					if(starts_with(strtolower($href),"../../books/")) { 
						$href = str_ireplace("../../books/","../books/",$href);
					}
					
					if(starts_with(strtolower($href),"../books/")) {
						$href = str_ireplace("../books/","https://supportline.microfocus.com/documentation/books/",$href);
						$anchor->setAttribute('href',$href);
					}
					else if(stripos($href,"/chmhelp.htm") !== false) { //fix a commonly used oddball case
						$anchor->setAttribute('href',"http://supportline.microfocus.com/documentation/CHMHelp.htm");
					}
					
					//a couple unique cases where images should have been used but weren't
					if(strcasecmp(trim($anchor->textContent),"view") === 0) {
						$anchor->nodeValue = "";
						$anchorcnt = $doc->createDocumentFragment();
						$anchorcnt->appendXML('<img src="/documentation/_images/html.png" alt="HTML" border="0"/>');
						$anchor->appendChild($anchorcnt);
					}
					else if(strcasecmp(trim($anchor->textContent),"download") === 0 && ends_with($href,'.jar')) {
						$anchor->nodeValue = "";
						$anchorcnt = $doc->createDocumentFragment();
						$anchorcnt->appendXML('<img src="/documentation/_images/jar.png" alt="JAR" border="0"/>');
						$anchor->appendChild($anchorcnt);
					}
				}				
				
				$html = '<div id = "heritage-mf" class="container">';
				$body = $doc->getElementsByTagName('body')->item(0);
				$owner = $body->ownerDocument;
				$bodyNodes = $body->childNodes;

				foreach ($bodyNodes as $node ) {
					$html .= $owner->saveHTML($node);
				}

				$html .= '</div>';
				$breadcrumbs = DocumentationHelper::get_hmf_breadcrumbs($path,$doc_path,$page_title ?: "Version");
				
				$ret = ["breadcrumbs" => $breadcrumbs,
					"page_title" => $page_title,
					"product_name" => $page_title,
					"meta_title" => $page_title . ' - Documentation',
					"body" => $html
				];
			}
		}
		
		return $ret;
	}
}
