<?php

namespace App\Helpers;

use Request;
use Session;
use URL;
use Lang;

/* See also Form Config options in /resources/form_config/example.php
 * debug a form config with /formlookup/{form_config}/
 *  Main translation file in includes::formbuilder
 *  Main blade includes::form/form
 *	JS in public/assets/js/formbuilder.js
 *  Usually invoked through Asset::includeForm() and Asset::formBuilderTransport()
 */

class FormBuilder {

	protected $translate;
	protected $Language;

	public function __construct() {
		// Because old configs and files all use $this->Language->translate(), this allows for backwards compatability
		$this->translate = Lang::parseTranslationFile('includes::formbuilder');
		$this->Language = $this;

	}

	/*  Display a form. Usually invoked through Asset::includeForm()
	 *   @input: if string, it's the path to config inside resources/form_config/
	 *           if array, merges result with all parent form_configs
	 *   @output: Echos form to screen. Uses includes::form/form.blade.php
	 *				each field wrapped by includes::form/field, then displayed with includes::form/field/type
	 */
	public function display_form($form_config_name = 'default', $bu = 'novell') {
		if ( is_array($form_config_name) ) {
			$parent_config = $this->get_config($form_config_name['form_template']);
			$form_config = $this->merge_arrays($parent_config,$form_config_name);
		} else {
			$form_config = $this->get_config($form_config_name);
		}

		// make sure the config is valid (throw errors otherwise)
		$config_path = '';//FORM_CONFIGS.DS.$form_config_name_path.'.php';
		$config_validation_result = true;//$this->Form->validate_config($form_config);

		$form_translate = $this->translate;
		$form_config['hidden_form_fields']['language'] = $form_config['hidden_form_fields']['Language'] = Lang::currentLang();

		$displayable_form_fields = $this->get_displayable_form_fields($form_config);

		// Hidden field management:
		$config_key = $this->store_config_cache($form_config);
		$displayable_hidden_fields = $this->get_editable_hidden_fields($form_config);
		$displayable_hidden_fields['form_config'] = $config_key;

		$view = Lang::view('includes::form/form')->with(compact('form_config', 'displayable_form_fields', 'displayable_hidden_fields', 'config_validation_result', 'bu', 'form_translate'));
		return $view->__toString();

	}

	/* Flattens the array set by avail_form_fields, only using fields inside used_form_fields
	 */
	public function get_displayable_form_fields($merged_config) {
		$displayable_form_fields = array();

		foreach ($merged_config['used_form_fields'] as $field_key) {
		if(empty($field_key))
              continue;

		if (is_array($field_key)) {
				$displayable_form_fields[$field_key['title']] = array(
					'is_field_wrapper' => true,
					'fields' => array()
				);
				foreach ($field_key['fields'] as $sub_field_key) {
					$field_data = $merged_config['avail_form_fields'][$sub_field_key];
					$displayable_form_fields[$field_key['title']]['fields'][$sub_field_key] = $field_data;
				}
			} else {
				$field_data = $merged_config['avail_form_fields'][$field_key];

				$displayable_form_fields[$field_key] = $field_data;
			}
		}

		return $displayable_form_fields;
	}

	/* Returns array of hidden fields and their values that need to be included on the form
	 */
	public function get_editable_hidden_fields($merged_config) {
		$editable_hidden_fields = array();

		foreach ($merged_config['hidden_form_fields'] as $field_key => $field_value) {
		if( empty($merged_config['editable_hidden_fields'][$field_key]) )
              continue;

			$editable_hidden_fields[$field_key] = $field_value;
		}

		return $editable_hidden_fields;
	}

	/* Builds a final merged config array using the name of a config template from resources/form_config/
	 */
	public function get_config($config_name, $parents = array()) {
		$form_config = $this->load_file($config_name);
		$form_config = $this->parse_dynamic_values($form_config);

		if( !empty($form_config['form_template']) ) {
			if( in_array($form_config['form_template'],$parents) ) {
				throw new \ErrorException('Form config is recursive: '.$config_name ." in (".implode(', ',$parents).')');
			}

			$parents[] = $config_name;
			$parent_config = $this->get_config($form_config['form_template'], $parents);

			$form_config = $this->merge_arrays($parent_config, $form_config);
		}

		return $form_config;
	}

	/* Read the file from disk. Returns unmodified array.
	 */
	private function load_file($config_name) {
		$config_path = public_path('../resources/form_config/' . str_replace('|', '/', $config_name) . '.php');
		if (!file_exists($config_path)) {
			throw new \ErrorException('Form config does not exist: '.$config_name);
		}
		$translate = $this->translate;
		require($config_path);
		return $form_config;
	}

	// So legacy form configs work ( due to $this->Language->translate($var) ):
	public function translate($var) {
		return !empty($this->translate[$var]) ? $this->translate[$var] : '';
	}

	// $child values override $parent values
	private function merge_arrays($parent, $child) {
		// Don't merge numeric arrays:
		if ( array_keys($parent) === range(0, count($parent) - 1) ) {
			return $child;
		}

		// inject each child value into the parent array
		foreach ($child as $key => $value) {
			if (array_key_exists($key, $parent) && is_array($value)) {
				$parent[$key] = $this->merge_arrays($parent[$key], $child[$key]);
			} else {
				$parent[$key] = $value;
			}
		}

		return $parent;
	}

	// Converts Language and Environment dependent values in config array
	private function parse_dynamic_values($arr) {
		if (!is_array($arr)) {
			return $arr;
		}

		$lang_code = Lang::currentCode();
		if (isset($arr['en-us'])) {
			if (isset($arr[$lang_code])) {
				$arr = $arr[$lang_code];
			} else {
				$arr = $arr['en-us'];
			}
		}
        if ( isset($arr['stage']) && isset($arr['live']) ) {
			if ( isset($arr[env('APP_ENV')]) ) {
				$arr = $arr[env('APP_ENV')];
			} else {
				$arr = $arr['stage'];
			}
        }

		if (!is_array($arr)) {
			return $arr;
		}


		foreach($arr as $arr_key => $arr_val) {
			$arr[$arr_key] = $this->parse_dynamic_values($arr_val);
		}

		return $arr;
	}

	// Takes a final config array, stores it in the storage directory
	// returns the key to look it up later with load_config_cache()
	private function store_config_cache($config) {
		$config = json_encode($config);
		$md5 = md5($config);
		if (!is_dir(storage_path('forms'))) {
			mkdir(storage_path('forms'), 0775, true);
		}
		file_put_contents(storage_path('forms/'.$md5.'.json'), $config, LOCK_EX);
		return $md5;
	}

	// Grabbed the cached config array. Also resets the language
	public function load_config_cache($key) {
		$config = json_decode(file_get_contents(storage_path('forms/'.$key.'.json')),true);
		if(!empty($config['hidden_form_fields']['language'])) {
			Lang::setCurrentLang($config['hidden_form_fields']['language']);
			$this->translate = Lang::parseTranslationFile('includes::formbuilder');
		}
		return $config;
	}

	/* takes the posted data, and returns an array of issues for the user
	 */
	public function validate_form_data($data,$form_config) {
		$validate_results = array();

		$fields_to_check = array();
		// get all displayed fields
		foreach ($form_config['used_form_fields'] as $field_key) {
		if(empty($field_key))
              continue;

		if (is_array($field_key)) {
				foreach ($field_key['fields'] as $sub_field_key) {
					$fields_to_check[] = $form_config['avail_form_fields'][$sub_field_key];
				}
			} else {
				$fields_to_check[] = $form_config['avail_form_fields'][$field_key];
			}
		}
		foreach ( $fields_to_check as $config ) {
			if (isset($config['columns']) && $config['columns'] == 2) {
				if (isset($config['first'])) {
					$this->check_required_and_validate($config['first'], $data, $validate_results);
				}
				if (isset($config['last'])) {
					$this->check_required_and_validate($config['last'], $data, $validate_results);
				}
			} else {
				$this->check_required_and_validate($config, $data, $validate_results);
			}
		}

		return $validate_results;
	}

	// used in validate_form_data()
	private function check_required_and_validate($field_data, $form_data, &$validate_results) {
		if (!isset($field_data['required'])) {
			$field_data['required'] = 'visible';
		}
		if ($field_data['required'] == 'visible') {
			if ( in_array( $field_data['field_name'], explode('|',$form_data['visible_fields']) ) ) {
				$field_data['required'] = true;
			}
		}
		if ($field_data['required'] === true) {
			if (empty($form_data[$field_data['field_name']])) {
				if($field_data['field_name'] == 'w_eula' || $field_data['field_name'] == 'w_optin'){
					$validate_results[$field_data['field_name']] = "Please read and agree to the terms of service";
				}
				else{
					$validate_results[$field_data['field_name']] = sprintf($this->translate('field_is_required'), $field_data['label']);
				}
			}
		}

		if (!empty($field_data['validate'])) {
			foreach ($field_data['validate'] as $validate_name => $validate_data) {
				$field_value = $form_data[$field_data['field_name']];
				if ( $validate_name == 'email' ) {
					if ( !filter_var($field_value, FILTER_VALIDATE_EMAIL) ) {
						$validate_results[$field_data['field_name']] = $this->translate('invalid_email_address');
					}
				} else
				if ( $validate_name == 'confirm_email' ) {
					if (!empty($form_data['email']) && $form_data['email'] !== $field_value) {
						$validate_results[$field_data['field_name']] = $this->translate('mismatch_confirm_email_address');
					}
				} 
			}
		}
	}

	/* Combine user data ($_POST in $data), hidden fields (in $form_config), and demandbase
	 *  Returns an array ready to pass on to eloqua etc
	 */
	public function build_submit_data($data,$form_config) {
		$submit_data = array();

		// Add in hidden fields
		foreach ( $form_config['editable_hidden_fields'] as $key => $editable ) {
			if ( $editable == true && isset($data[$key]) ) {
				$submit_data[$key] = $data[$key];
			}
		}

		// Add in displayed fields
		$displayable_form_fields = $this->get_displayable_form_fields($form_config);
		foreach ($displayable_form_fields as $field_data) {

			if (isset($field_data['columns']) && $field_data['columns'] == 2) {
				if (isset($field_data['first'])) {
					$key = $field_data['first']['field_name'];
					if ( isset($data[$key]) ) {
						$submit_data[$key] = $data[$key];
					}
				}
				if (isset($field_data['last'])) {
					$key = $field_data['last']['field_name'];
					if ( isset($data[$key]) ) {
						$submit_data[$key] = $data[$key];
					}
				}
			} else {
				$key = $field_data['field_name'];
				if ( isset($data[$key]) ) {
					$submit_data[$key] = $data[$key];
				}
			}

		}

		// add in config hidden fields
		foreach ( $form_config['hidden_form_fields'] as $key => $value ) {
			if ( empty($submit_data[$key]) ) {
				$submit_data[$key] = $value;
			}
		}

		// add in demandbase data
		$demandbase_obj = \App\Helpers\Demandbase::getInstance();
		$demandbase_data = $demandbase_obj->getUserData()->getData(true);
		if ( isset($demandbase_data['error']) ) {
			$current_ip = $_SERVER['REMOTE_ADDR'];

			if (function_exists('apache_request_headers')) {
				$headers = apache_request_headers();

				if (isset($headers["X-Forwarded-For"])) {
					$x_forwarded_for_arr = explode(',', $headers["X-Forwarded-For"]);
					$current_ip = trim($x_forwarded_for_arr[0]);
				}
			}
			$submit_data['db_ip'] = $current_ip;
		} else {
			foreach ( $demandbase_data as $key => $value ) {
				if ( is_array($value) ) {
					foreach ( $value as $subkey => $subvalue ) {
						$submit_data['db_'.$key.'_'.$subkey] = $subvalue;
					}
				} else {
					$submit_data['db_'.$key] = $value;
				}
			}
		}

		return $submit_data;
	}

	public function postToEloqua($submit_data,$form_config) {
		$url = env('ELOQUA_POST_URL') ?? 'https://secure.eloqua.com/e/f2.aspx';
		if (isset($form_config['global_settings']['post_action'])) {
			$url = $form_config['global_settings']['post_action'];
		}
		$timeout = 10;

		$post_string = http_build_query($submit_data);


		$parts = parse_url($url);

		$fp = fsockopen($parts['host'], isset($parts['port']) ? $parts['port'] : 80, $errno, $errstr, $timeout);

		$out = "POST " . $parts['path'] . " HTTP/1.1\r\n";
		$out.= "Host: " . $parts['host'] . "\r\n";
		$out.= "Content-Type: application/x-www-form-urlencoded\r\n";
		$out.= "Content-Length: " . strlen($post_string) . "\r\n";
		$out.= "Connection: Close\r\n\r\n";
		if (isset($post_string)) {
			$out .= $post_string;
		}

		fwrite($fp, $out);
		fclose($fp);

		return true; // this async call is ignoring output of call.
	}


}