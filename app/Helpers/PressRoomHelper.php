<?php

namespace App\Helpers;

use Lang;
use URL;

class PressRoomHelper {
	
	public static function get_basedir() {
		return public_path('../resources/translations/content/' . Lang::getLangConfig("default") . '/about/press-room');
	}
	
	/* read through the directories, build the listing from English then translate into current language
	 * $type should be one of the following:
	 *		'in-the-news',
	 *		'opinion',
	 *		'article'
	 * $limit :	if 0 then it will return all results
	 *			otherwise will return the limit results
	 */
	public static function get_press_listings($type, $limit=0) {
		
		$basedir = PressRoomHelper::get_basedir();
		
		// sort function for sorting on date descending
		$sort = function($a,$b) {
			$adate = strtotime($a['article_date']);
			$bdate = strtotime($b['article_date']);
			return $adate == $bdate ? 0 : ($adate < $bdate ? 1 : -1);
		};
		
		$list = array();
		if ($type == 'article') {
			foreach(scandir($basedir."/${type}") as $dir) {
				if(strpos($dir,'.')===false) {
					$list[$dir] = array();
					foreach(scandir($basedir."/${type}/".$dir) as $file) {
						if(preg_match('/^(.+)\.html$/',$file,$m)) {
							$path = "about/press-room/${type}/".$dir.'/'.$m[1];
							if ($translate = Lang::parseTranslationFile('content::'.$path)) {
								$translate['article_link'] = '/'.$path;
								$list[$dir][] = $translate;
							}
						}
					}
					usort($list[$dir], $sort);
				}
			}
			krsort($list);
		}
		else {
			foreach(scandir($basedir."/${type}") as $file) {
				if(preg_match('/^(.+)\.html$/',$file,$m)) {
					$list[] = Lang::parseTranslationFile("content::about/press-room/${type}/".$m[1]);
				}
			}
			usort($list, $sort);
		}
		if (!empty($limit)) {
			return array_slice($list, 0, $limit);
		}
		else {
			return $list;
		}
	}
	
	/*
	 * Make news section for home page and press room page
	 */
	public static function get_homepage_news_section($limit=3) {
		$articles = PressRoomHelper::get_press_listings('article', $limit);
		$news = PressRoomHelper::get_press_listings('in-the-news', $limit);
		
		// colors for display
		$colors = ['turquiose','blue-primary','blue'];
		
		$news_section = [];
		
		// add articles to $news_section
		$i = 0;
		foreach ($articles as $article) {

			foreach ($article as $a) {
				if ($i < $limit) {
					$tmp = array(
						'news_color' => $colors[$i % 3],
						'news_category' => 'Recent Releases',
						'news_date' => $a['article_date'],
						'news_link' => URL::to($a['article_link']),
						'news_content' => $a['article_title'],
					);
					$i++;
					$news_section[] = $tmp;
				}
				else {
					break;
				}
			}
		}
		
		// reset $i for news 
		$i = 0;
		foreach ($news as $n) {
			$tmp = array(
				'news_color' => $colors[$i % 3],
				'news_category' => 'News',
				'news_date' => $n['article_date'],
				'news_link' => $n['article_link'],
				'news_content' => $n['article_title'],
			);
			$i++;
			$news_section[] = $tmp;
		}
		return $news_section;
	}
	
}
