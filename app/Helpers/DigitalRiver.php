<?php

namespace App\Helpers;

//use Request;

class DigitalRiver {

	public function __construct() {
		//$this->client = new EloquaRequest('NovellInc', 'Andrew.Morrill', 'Mynewpassword$4', 'https://secure.eloqua.com/API/REST/1.0');
//		$old_emea_country_list_str = "AT|DE|LI|DZ|BJ|BF|CM|TD|CI|FR|GA|GM|GP|LR|MG|ML|MQ|MR|YT|MC|MA|NE|RE|PM|SN|SL|TG|TN|EH|AL|AD|AO|AM|AZ|BH|BY|BE|BA|BW|BG|BI|CV|CF|KM|HR|CY|CZ|DJ|EG|ER|EE|ET|FO|FI|GE|GH|GI|GR|GL|HU|IS|IQ|IL|IT|JO|KZ|KE|KW|KG|LV|LB|LS|LT|LU|MK|MW|MV|MT|MU|MD|MZ|NA|NL|NG|OM|PL|PT|QA|RO|RU|RW|SH|SM|SA|CS|SC|SK|SI|SO|ZA|ES|SZ|TJ|TZ|TR|TM|UG|UA|AE|UZ|YE|ZM|ZW|IE|GS";
//		$old_emea_country_list_values_arr = explode('|', $old_emea_country_list_str);
//		$this->old_emea_country_list = array_combine($old_emea_country_list_values_arr, $old_emea_country_list_values_arr);
	}

	

//	public static function get_cart_summary() {
//		$cart_summary_url = "https://buy.suse.com/store/suse/DisplayDRCartSummary/Version.2/output.json";
//
//		// Initiate cURL
//		$ch = curl_init();
//		curl_setopt($ch, CURLOPT_URL, $cart_summary_url);
//
//		$cookies = "";
//		foreach ( $_COOKIE as $cookie => $val ) {
//			$cookies .= $cookie . "=" . $val . "; ";
//		}
//
//		//print_r($_COOKIE);
//
//		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
//		curl_setopt($ch, CURLOPT_COOKIE, $cookies);
//
//		// Get result and close cURL
//		$result = curl_exec($ch);
//		curl_close($ch);
//		
//		$result = preg_match('/.*({.*}).*/', $result, $matches);
//		if (!empty($matches[0])) {
//			$result = $matches[0];
//		}
//		$result = preg_replace('/\s+/', '', $result);
//		
//		$decoded = json_decode($result, true);
//		if (isset($decoded['lineItems'])) {
//			return $decoded['lineItems'];
//		}
//		
//		return 0;
//	}

	public static function get_local_pricing_data() {
		$local_product_list_cache_key = 'pricing_local_product_list';
		$pricing_data = array();
		if ( apcu_exists($local_product_list_cache_key) ) {
			$pricing_data = apcu_fetch($local_product_list_cache_key);
		} else {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'http://appserviceinternaltest.provo.novell.com/product_pricing_api/product_pricing_api');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			$result = curl_exec($ch);

			if ( $result === false ) {
				
			} else {
				$pricing_data = json_decode($result, true);
				apcu_store($local_product_list_cache_key, $pricing_data, getenv('CACHE_LENGTH')); // one day
			}
			curl_close($ch);
		}


		return $pricing_data;
	}

	public static function get_country_data($country) {
		$country_data = array();

		$en_nz_countries = array('AS' => 1, 'AS' => 1, 'AI' => 1, 'AQ' => 1, 'AG' => 1, 'AW' => 1, 'BS' => 1, 'BD' => 1, 'BB' => 1, 'BZ' => 1, 'BM' => 1, 'BR' => 1, 'IO' => 1, 'CV' => 1, 'KH' => 1, 'KY' => 1, 'CN' => 1, 'CX' => 1, 'CC' => 1, 'KM' => 1, 'CK' => 1, 'DM' => 1, 'FK' => 1, 'FO' => 1, 'GF' => 1, 'PF' => 1, 'FJ' => 1, 'GP' => 1, 'GU' => 1, 'GN' => 1, 'GW' => 1, 'GY' => 1, 'HT' => 1, 'HM' => 1, 'HK' => 1, 'ID' => 1, 'JM' => 1, 'KI' => 1, 'KR' => 1, 'LA' => 1, 'MO' => 1, 'MY' => 1, 'MV' => 1, 'MH' => 1, 'MQ' => 1, 'MR' => 1, 'MU' => 1, 'FM' => 1, 'MS' => 1, 'MM' => 1, 'AS' => 1, 'NR' => 1, 'AN' => 1, 'NZ' => 1, 'NU' => 1, 'NF' => 1, 'MP' => 1, 'NC' => 1, 'PW' => 1, 'PG' => 1, 'PH' => 1, 'PN' => 1, 'SH' => 1, 'AS' => 1, 'KN' => 1, 'LC' => 1, 'VC' => 1, 'PM' => 1, 'WS' => 1, 'ST' => 1, 'SC' => 1, 'SG' => 1, 'SB' => 1, 'GS' => 1, 'LK' => 1, 'SR' => 1, 'SJ' => 1, 'TW' => 1, 'TH' => 1, 'TP' => 1, 'TO' => 1, 'TK' => 1, 'TT' => 1, 'TC' => 1, 'TV' => 1, 'UM' => 1, 'VU' => 1, 'VE' => 1, 'VN' => 1, 'VG' => 1, 'VI' => 1, 'BT' => 1, 'BN' => 1, 'KZ' => 1, 'KG' => 1, 'NP' => 1, 'MN' => 1, 'TJ' => 1, 'CA' => 1, 'IN' => 1, 'AU' => 1,);
		$en_za_countries = array('AF' => 1, 'AM' => 1, 'AZ' => 1, 'BJ' => 1, 'BW' => 1, 'BF' => 1, 'CM' => 1, 'BI' => 1, 'CF' => 1, 'TD' => 1, 'CG' => 1, 'CD' => 1, 'CI' => 1, 'DJ' => 1, 'EG' => 1, 'ER' => 1, 'ET' => 1, 'GM' => 1, 'GA' => 1, 'GH' => 1, 'IQ' => 1, 'JO' => 1, 'KE' => 1, 'KW' => 1, 'LB' => 1, 'LS' => 1, 'LR' => 1, 'LY' => 1, 'MG' => 1, 'MW' => 1, 'ML' => 1, 'YT' => 1, 'MZ' => 1, 'NE' => 1, 'NG' => 1, 'OM' => 1, 'PK' => 1, 'QA' => 1, 'RE' => 1, 'RW' => 1, 'SA' => 1, 'SN' => 1, 'SL' => 1, 'SO' => 1, 'ZA' => 1, 'SZ' => 1, 'TG' => 1, 'TM' => 1, 'UG' => 1, 'AE' => 1, 'EH' => 1, 'ZM' => 1, 'ZW' => 1, 'UZ' => 1, 'PS' => 1, 'YE' => 1, 'BH' => 1, 'EH' => 1,);
		$en_gb_countries = array('GB' => 1, 'UK' => 1, 'HU' => 1, 'AT' => 1, 'BE' => 1, 'LI' => 1, 'LU' => 1, 'BY' => 1, 'AM' => 1, 'EE' => 1, 'GE' => 1, 'MD' => 1, 'UA' => 1, 'BA' => 1, 'CH' => 1, 'MT' => 1, 'SM' => 1, 'VA' => 1, 'BG' => 1, 'HR' => 1, 'CY' => 1, 'FI' => 1, 'GE' => 1, 'GD' => 1, 'IE' => 1, 'LV' => 1, 'LT' => 1, 'MK' => 1, 'MD' => 1, 'MC' => 1, 'RO' => 1, 'RS' => 1, 'CS' => 1, 'SK' => 1, 'SI' => 1, 'AL' => 1, 'UA' => 1, 'IS' => 1, 'GL' => 1, 'GI' => 1, 'AD' => 1,);
		$es_mx_countries = array('AR' => 1, 'BO' => 1, 'CL' => 1, 'CO' => 1, 'CR' => 1, 'EC' => 1, 'SV' => 1, 'GI' => 1, 'GT' => 1, 'GQ' => 1, 'HN' => 1, 'NI' => 1, 'PA' => 1, 'PY' => 1, 'PE' => 1, 'PR' => 1, 'DO' => 1, 'UY' => 1, 'VE' => 1, 'MX' => 1, 'DO' => 1);
		$fr_fr_countries = array('FR' => 1, 'MA' => 1, 'TN' => 1, 'DZ' => 1, 'LU' => 1,);


		if ( $country == 'US' ) {
			$country_data['locale'] = 'en_US';
			$country_data['currency'] = 'USD';
			$country_data['oracle_currency'] = 'USD';
			$country_data['in_emea'] = false;
		} else if ( isset($en_nz_countries[$country]) ) { // en_NZ - new zealand
			$country_data['locale'] = 'en_NZ';
			$country_data['currency'] = 'USD';
			$country_data['oracle_currency'] = 'USD';
			$country_data['in_emea'] = false;
		} else if ( isset($en_za_countries[$country]) ) { // en_ZA - south africa
			$country_data['locale'] = 'en_ZA';
			$country_data['currency'] = 'USD';
			$country_data['oracle_currency'] = 'USD';
			$country_data['in_emea'] = true;
		} else if ( isset($en_gb_countries[$country]) ) { // en_GB - united kingdom
			$country_data['locale'] = 'en_GB';
			$country_data['currency'] = 'GBP';
			$country_data['oracle_currency'] = 'GBP';
			$country_data['in_emea'] = true;
		} else if ( $country == 'DE' ) { // germany
			$country_data['locale'] = 'de_DE';
			$country_data['currency'] = 'EUR';
			$country_data['oracle_currency'] = 'EUR';
			$country_data['in_emea'] = true;
		} else if ( $country == 'CZ' ) { // czech republic
			$country_data['locale'] = 'en_GB'; // not sure what this should be
			$country_data['currency'] = 'EUR';
			$country_data['oracle_currency'] = 'EUR';
			$country_data['in_emea'] = true;
		} else if ( $country == 'ES' ) { // spain
			$country_data['locale'] = 'es_ES';
			$country_data['currency'] = 'EUR';
			$country_data['oracle_currency'] = 'EUR';
			$country_data['in_emea'] = true;
		} else if ( isset($es_mx_countries[$country]) ) { // es_MX - mexico
			$country_data['locale'] = 'es_MX';
			$country_data['currency'] = 'MXN';
			$country_data['oracle_currency'] = 'USD';
			$country_data['in_emea'] = false;
		} else if ( isset($fr_fr_countries[$country]) ) { // france
			$country_data['locale'] = 'fr_FR';
			$country_data['currency'] = 'EUR';
			$country_data['oracle_currency'] = 'EUR';
			$country_data['in_emea'] = true;
		} else if ( $country == 'PT' ) { // portugal
			$country_data['locale'] = 'pt_PT';
			$country_data['currency'] = 'EUR';
			$country_data['oracle_currency'] = 'EUR';
			$country_data['in_emea'] = true;
		} else if ( $country == 'DK' ) { // denmark
			$country_data['locale'] = 'da_DK';
			$country_data['currency'] = 'DKK';
			$country_data['oracle_currency'] = 'DKK';
			$country_data['in_emea'] = true;
		} else if ( $country == 'IT' ) { // italy
			$country_data['locale'] = 'it_IT';
			$country_data['currency'] = 'EUR';
			$country_data['oracle_currency'] = 'EUR';
			$country_data['in_emea'] = true;
		} else if ( $country == 'NL' ) { // netherlands
			$country_data['locale'] = 'nl_NL';
			$country_data['currency'] = 'EUR';
			$country_data['oracle_currency'] = 'EUR';
			$country_data['in_emea'] = true;
		} else if ( $country == 'NO' ) { // norway
			$country_data['locale'] = 'no_NO';
			$country_data['currency'] = 'NOK';
			$country_data['oracle_currency'] = 'NOK';
			$country_data['in_emea'] = true;
		} else if ( $country == 'RU' ) { // russia
			$country_data['locale'] = 'ru_RU';
			$country_data['currency'] = 'RUB';
			$country_data['oracle_currency'] = 'EUR';
			$country_data['in_emea'] = true;
		} else if ( $country == 'SE' ) { // sweden
			$country_data['locale'] = 'sv_SE';
			$country_data['currency'] = 'SEK';
			$country_data['oracle_currency'] = 'EUR';
			$country_data['in_emea'] = true;
		} else if ( $country == 'PL' ) { // poland
			$country_data['locale'] = 'pl_PL';
			$country_data['currency'] = 'PLN';
			$country_data['oracle_currency'] = 'EUR';
			$country_data['in_emea'] = true;
		} else if ( $country == 'JP' ) { // japan
			$country_data['locale'] = 'ja_JP';
			$country_data['currency'] = 'JPY';
			$country_data['oracle_currency'] = 'USD';
			$country_data['in_emea'] = false;
		} else if ( $country == 'IL' ) { // israel
			$country_data['locale'] = 'iw_IL';
			$country_data['currency'] = 'ILS';
			$country_data['oracle_currency'] = 'USD';
			$country_data['in_emea'] = true;
		} else if ( $country == 'GR' ) { // greece
			$country_data['locale'] = 'el_GR';
			$country_data['currency'] = 'EUR';
			$country_data['oracle_currency'] = 'EUR';
			$country_data['in_emea'] = true;
		} else if ( $country == 'TR' ) { // turkey
			$country_data['locale'] = 'tr_TR';
			$country_data['currency'] = 'TRY';
			$country_data['oracle_currency'] = 'EUR';
			$country_data['in_emea'] = true;
		} /* else if ($country == 'CN') { // china - go live dec 31st
		  // DREW TODO - need to implement this
		  } else if ($country == 'BR') { // brazil - go live march 31st, 2015
		  // DREW TODO - need to implement this
		  } */

		return $country_data;
	}


	public function get_full_country_list() {
		return array('AF' => 'Afghanistan', 'AX' => 'Åland Islands', 'AL' => 'Albania', 'DZ' => 'Algeria', 'AS' => 'American Samoa', 'AD' => 'Andorra', 'AO' => 'Angola', 'AI' => 'Anguilla', 'AQ' => 'Antarctica', 'AG' => 'Antigua And Barbuda', 'AR' => 'Argentina', 'AM' => 'Armenia', 'AW' => 'Aruba', 'AU' => 'Australia', 'AT' => 'Austria', 'AZ' => 'Azerbaijan', 'BS' => 'Bahamas', 'BH' => 'Bahrain', 'BD' => 'Bangladesh', 'BB' => 'Barbados', 'BY' => 'Belarus', 'BE' => 'Belgium', 'BZ' => 'Belize', 'BJ' => 'Benin', 'BM' => 'Bermuda', 'BT' => 'Bhutan', 'BO' => 'Bolivia, Plurinational State Of', 'BQ' => 'Bonaire, Sint Eustatius And Saba', 'BA' => 'Bosnia And Herzegovina', 'BW' => 'Botswana', 'BV' => 'Bouvet Island', 'BR' => 'Brazil', 'IO' => 'British Indian Ocean Territory', 'BN' => 'Brunei Darussalam', 'BG' => 'Bulgaria', 'BF' => 'Burkina Faso', 'BI' => 'Burundi', 'KH' => 'Cambodia', 'CM' => 'Cameroon', 'CA' => 'Canada', 'CV' => 'Cape Verde', 'KY' => 'Cayman Islands', 'CF' => 'Central African Republic', 'TD' => 'Chad', 'CL' => 'Chile', 'CN' => 'China', 'CX' => 'Christmas Island', 'CC' => 'Cocos (keeling) Islands', 'CO' => 'Colombia', 'KM' => 'Comoros', 'CG' => 'Congo', 'CD' => 'Congo, The Democratic Republic Of The', 'CK' => 'Cook Islands', 'CR' => 'Costa Rica', 'CI' => 'Côte D\'ivoire', 'HR' => 'Croatia', 'CU' => 'Cuba', 'CW' => 'Curaçao', 'CY' => 'Cyprus', 'CZ' => 'Czech Republic', 'DK' => 'Denmark', 'DJ' => 'Djibouti', 'DM' => 'Dominica', 'DO' => 'Dominican Republic', 'EC' => 'Ecuador', 'EG' => 'Egypt', 'SV' => 'El Salvador', 'GQ' => 'Equatorial Guinea', 'ER' => 'Eritrea', 'EE' => 'Estonia', 'ET' => 'Ethiopia', 'FK' => 'Falkland Islands (malvinas)', 'FO' => 'Faroe Islands', 'FJ' => 'Fiji', 'FI' => 'Finland', 'FR' => 'France', 'GF' => 'French Guiana', 'PF' => 'French Polynesia', 'TF' => 'French Southern Territories', 'GA' => 'Gabon', 'GM' => 'Gambia', 'GE' => 'Georgia', 'DE' => 'Germany', 'GH' => 'Ghana', 'GI' => 'Gibraltar', 'GR' => 'Greece', 'GL' => 'Greenland', 'GD' => 'Grenada', 'GP' => 'Guadeloupe', 'GU' => 'Guam', 'GT' => 'Guatemala', 'GG' => 'Guernsey', 'GN' => 'Guinea', 'GW' => 'Guinea-bissau', 'GY' => 'Guyana', 'HT' => 'Haiti', 'HM' => 'Heard Island And Mcdonald Islands', 'VA' => 'Holy See (vatican City State)', 'HN' => 'Honduras', 'HK' => 'Hong Kong', 'HU' => 'Hungary', 'IS' => 'Iceland', 'IN' => 'India', 'ID' => 'Indonesia', 'IR' => 'Iran, Islamic Republic Of', 'IQ' => 'Iraq', 'IE' => 'Ireland', 'IM' => 'Isle Of Man', 'IL' => 'Israel', 'IT' => 'Italy', 'JM' => 'Jamaica', 'JP' => 'Japan', 'JE' => 'Jersey', 'JO' => 'Jordan', 'KZ' => 'Kazakhstan', 'KE' => 'Kenya', 'KI' => 'Kiribati', 'KP' => 'Korea, Democratic People\'s Republic Of', 'KR' => 'Korea, Republic Of', 'KW' => 'Kuwait', 'KG' => 'Kyrgyzstan', 'LA' => 'Lao People\'s Democratic Republic', 'LV' => 'Latvia', 'LB' => 'Lebanon', 'LS' => 'Lesotho', 'LR' => 'Liberia', 'LY' => 'Libya', 'LI' => 'Liechtenstein', 'LT' => 'Lithuania', 'LU' => 'Luxembourg', 'MO' => 'Macao', 'MK' => 'Macedonia, The Former Yugoslav Republic Of', 'MG' => 'Madagascar', 'MW' => 'Malawi', 'MY' => 'Malaysia', 'MV' => 'Maldives', 'ML' => 'Mali', 'MT' => 'Malta', 'MH' => 'Marshall Islands', 'MQ' => 'Martinique', 'MR' => 'Mauritania', 'MU' => 'Mauritius', 'YT' => 'Mayotte', 'MX' => 'Mexico', 'FM' => 'Micronesia, Federated States Of', 'MD' => 'Moldova, Republic Of', 'MC' => 'Monaco', 'MN' => 'Mongolia', 'ME' => 'Montenegro', 'MS' => 'Montserrat', 'MA' => 'Morocco', 'MZ' => 'Mozambique', 'MM' => 'Myanmar', 'NA' => 'Namibia', 'NR' => 'Nauru', 'NP' => 'Nepal', 'NL' => 'Netherlands', 'NC' => 'New Caledonia', 'NZ' => 'New Zealand', 'NI' => 'Nicaragua', 'NE' => 'Niger', 'NG' => 'Nigeria', 'NU' => 'Niue', 'NF' => 'Norfolk Island', 'MP' => 'Northern Mariana Islands', 'NO' => 'Norway', 'OM' => 'Oman', 'PK' => 'Pakistan', 'PW' => 'Palau', 'PS' => 'Palestinian Territory, Occupied', 'PA' => 'Panama', 'PG' => 'Papua New Guinea', 'PY' => 'Paraguay', 'PE' => 'Peru', 'PH' => 'Philippines', 'PN' => 'Pitcairn', 'PL' => 'Poland', 'PT' => 'Portugal', 'PR' => 'Puerto Rico', 'QA' => 'Qatar', 'RE' => 'Réunion', 'RO' => 'Romania', 'RU' => 'Russian Federation', 'RW' => 'Rwanda', 'BL' => 'Saint Barthélemy', 'SH' => 'Saint Helena, Ascension And Tristan Da Cunha', 'KN' => 'Saint Kitts And Nevis', 'LC' => 'Saint Lucia', 'MF' => 'Saint Martin (french Part)', 'PM' => 'Saint Pierre And Miquelon', 'VC' => 'Saint Vincent And The Grenadines', 'WS' => 'Samoa', 'SM' => 'San Marino', 'ST' => 'Sao Tome And Principe', 'SA' => 'Saudi Arabia', 'SN' => 'Senegal', 'RS' => 'Serbia', 'SC' => 'Seychelles', 'SL' => 'Sierra Leone', 'SG' => 'Singapore', 'SX' => 'Sint Maarten (dutch Part)', 'SK' => 'Slovakia', 'SI' => 'Slovenia', 'SB' => 'Solomon Islands', 'SO' => 'Somalia', 'ZA' => 'South Africa', 'GS' => 'South Georgia And The South Sandwich Islands', 'SS' => 'South Sudan', 'ES' => 'Spain', 'LK' => 'Sri Lanka', 'SD' => 'Sudan', 'SR' => 'Suriname', 'SJ' => 'Svalbard And Jan Mayen', 'SZ' => 'Swaziland', 'SE' => 'Sweden', 'CH' => 'Switzerland', 'SY' => 'Syrian Arab Republic', 'TW' => 'Taiwan, Province Of China', 'TJ' => 'Tajikistan', 'TZ' => 'Tanzania, United Republic Of', 'TH' => 'Thailand', 'TL' => 'Timor-leste', 'TG' => 'Togo', 'TK' => 'Tokelau', 'TO' => 'Tonga', 'TT' => 'Trinidad And Tobago', 'TN' => 'Tunisia', 'TR' => 'Turkey', 'TM' => 'Turkmenistan', 'TC' => 'Turks And Caicos Islands', 'TV' => 'Tuvalu', 'UG' => 'Uganda', 'UA' => 'Ukraine', 'AE' => 'United Arab Emirates', 'GB' => 'United Kingdom', 'US' => 'United States', 'UM' => 'United States Minor Outlying Islands', 'UY' => 'Uruguay', 'UZ' => 'Uzbekistan', 'VU' => 'Vanuatu', 'VE' => 'Venezuela, Bolivarian Republic Of', 'VN' => 'Viet Nam', 'VG' => 'Virgin Islands, British', 'VI' => 'Virgin Islands, U.s.', 'WF' => 'Wallis And Futuna', 'EH' => 'Western Sahara', 'YE' => 'Yemen', 'ZM' => 'Zambia', 'ZW' => 'Zimbabwe');
	}


	

	public static function get_product_prices($user_country_code = null, $products = null) {
		$direct_output = false;
		
		$prices_to_return = array();

		if (empty($user_country_code)) {
			$user_country_code = Lang::currentCountryCode();
		}
		$country_data = self::get_country_data($user_country_code);
		$curr_dr_locale = $country_data['locale'];
		$curr_dr_currency = $country_data['currency'];
		$oracle_currency = $country_data['oracle_currency'];
		$in_emea = $country_data['in_emea'];
		

		$choose_country_page_on = false;
//		if ( in_array($curr_dr_locale, array('en_US', 'en_GB', 'en_NZ', 'en_ZA')) ) { // DREW TODO - maybe turn this back on later
//			$choose_country_page_on = true;
//		}


		/////////////////////////////
		// get the pricing list
		$product_list_cache_key = 'pricing_api_product_list';
		if ( apcu_exists($product_list_cache_key) ) {
			$shop_product_list = apcu_fetch($product_list_cache_key);
		} else {
//			require($_SERVER['DOCUMENT_ROOT'] . "/common/inc/product_pricing/pricing_api_product_list.php");
			$shop_product_list = config("shop-products");
			apcu_store($product_list_cache_key, $shop_product_list, getenv('CACHE_LENGTH'));
		}
		
		if (empty($products)) { // DREW TODO - change the awy this works as this is kinda crazy (just grabbing them all)
			$products = $shop_product_list;
		}
		

		////////////////////////////////////////////////////
		// compile the product ID's to request
		if ( empty($products) ) {
			$products = $_POST['products'];
		}
		$product_ids = array();
		$local_ids = array();
		foreach ( $products as $product_name => $product_variations ) {
			foreach ( $product_variations as $product_config => $val ) {
				if ( isset($shop_product_list[$product_name][$product_config]) ) {
					if ( isset($shop_product_list[$product_name][$product_config]['local_id']) ) {
						$local_ids[$shop_product_list[$product_name][$product_config]['local_id']] = compact('product_name', 'product_config');
					} else {
						if ( $in_emea == true ) {
							if ( isset($shop_product_list[$product_name][$product_config]['emea_product_id']) ) {
								$product_ids[$shop_product_list[$product_name][$product_config]['emea_product_id']] = compact('product_name', 'product_config');
							}
						} else {
							if ( isset($shop_product_list[$product_name][$product_config]['product_id']) ) {
								$product_ids[$shop_product_list[$product_name][$product_config]['product_id']] = compact('product_name', 'product_config');
							}
						}
					}
				}
			}
		}
		// setup the data for the api call
		$product_ids_arr = array();
		foreach ( $product_ids as $product_id => $val ) {
			// check if the product_id result is already stored in the cache
			$apcu_cache_key = $product_id . "_" . $curr_dr_locale . "_" . $curr_dr_currency;
			if ( apcu_exists($apcu_cache_key) ) { // only use apc on direct output
				// if data for the product is cached then just add data to the final array
				$product_data = $product_ids[$product_id];
				$prices_to_return[$product_data['product_name']][$product_data['product_config']] = apcu_fetch($apcu_cache_key);
				if ( $direct_output && $choose_country_page_on ) {
					$prices_to_return[$product_data['product_name']][$product_data['product_config']]['url'] = $data_to_return['url'] = "/shop/choose_country.php?p_name={$product_data['product_name']}&pc_name={$product_data['product_config']}";
				}
				$prices_to_return[$product_data['product_name']][$product_data['product_config']]['from_cache'] = true;
			} else {
				// only search for the product below if the data is not cached above
				$product_ids_arr[] = 'productID.' . $product_id;
			}
		}

		///////////////////////////////////////////////////////////////////////////////////////////////
		// setup the data for the local database call, but only if required
		$local_product_ids_arr = array();
		foreach ( $local_ids as $local_id => $val ) {
			// check if the product_id result is already stored in the cache
			$apcu_cache_key = $local_id . "_" . $curr_dr_locale . "_" . $curr_dr_currency . "_" . $oracle_currency;
			if ( apcu_exists($apcu_cache_key) ) {
				// if data for the product is cached then just add data to the final array
				$product_data = $local_ids[$local_id];
				$prices_to_return[$product_data['product_name']][$product_data['product_config']] = apcu_fetch($apcu_cache_key);
				$prices_to_return[$product_data['product_name']][$product_data['product_config']]['from_cache'] = true;
			} else {
				// only search for the product below if the data is not cached above
				$local_product_ids_arr[$local_id] = $val;
			}
		}
		if ( !empty($local_product_ids_arr) ) {
			$pricing_data = self::get_local_pricing_data();

			foreach ( $local_product_ids_arr as $curr_product_id => $product_data ) {
				$chosen_currency = $oracle_currency . " CORP";
				if ( empty($pricing_data['product_pricing_list'][$curr_product_id][$chosen_currency]) ) {
					continue;
				}
				$curr_pricing_data = $pricing_data['product_pricing_list'][$curr_product_id][$chosen_currency];

				if ( isset($curr_pricing_data['PRICE']) ) {
					$apcu_cache_key = $curr_product_id . "_" . $curr_dr_locale . "_" . $curr_dr_currency . "_" . $oracle_currency;
					$data_to_return = array(
						'cache_key' => $apcu_cache_key,
						'from_cache' => false,
						'product_id' => $curr_product_id,
						'price' => $curr_pricing_data['PRICE'],
						'url' => '',
						'html-data-keys' => "data-product-name={$product_data['product_name']} data-product-config={$product_data['product_config']}"
					);
					$prices_to_return[$product_data['product_name']][$product_data['product_config']] = $data_to_return;
					apcu_store($apcu_cache_key, $data_to_return, getenv('CACHE_LENGTH'));
				}
			}
		}


		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// only make the call to DR if we have product ids to search on (there won't be if we had cached data above)
		if ( !empty($product_ids_arr) ) {
			//////////////////////////////////////////////////////////////
			// build the request url
			$product_ids_str = implode('/', $product_ids_arr);
//                        $curr_dr_locale = 'en_US';
//                        $product_ids_str = 'productID.270606800';
			$product_prices_url = "http://buy.suse.com/store/suse/$curr_dr_locale/DisplayDRProductInfo/version.2/$product_ids_str/includeVariations.true/content.displayName+thumbnail+buyLink+price+product.externalReferenceID+product.variation+product.allVariations.size/env.design/currency.$curr_dr_currency/output.json";



			/////////////////////////////////////////////////////////
			// Do the CURL call
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $product_prices_url);
			$cookies = "";
			if ( !empty($_COOKIE) ) {
				$cookies = http_build_query($_COOKIE);
			}
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_COOKIE, $cookies);
			$result_json = curl_exec($ch);
			curl_close($ch);


			//////////////////////////////////////////////////////////////
			// tabulate the results
			$product_price_datas = json_decode(trim(preg_replace("/\/\*(.*)\*\//", '', strip_tags($result_json))), true);
			$product_array = array();
			if ( isset($product_price_datas['productInfo']['product'][0]) ) {
				$product_array = $product_price_datas['productInfo']['product'];
			} else {
				$product_array[] = $product_price_datas['productInfo']['product'];
			}
			foreach ( $product_array as $product_price_data ) {
				// check for error and mark as error
				if ( isset($product_price_data['error']) && isset($product_price_data['productID']) ) {
					$product_data = $product_ids[$product_price_data['productID']];
					$data_to_return = array(
						'product_id' => $product_price_data['productID'],
						'error' => $product_price_data['error']
					);
					$prices_to_return[$product_data['product_name']][$product_data['product_config']] = $data_to_return;
				}
				if ( isset($product_price_data['price']['unitPrice']) && isset($product_price_data['buyLink']['href']) && isset($product_price_data['productID']) ) {
					$product_data = $product_ids[$product_price_data['productID']];
					$apcu_cache_key = $product_price_data['productID'] . "_" . $curr_dr_locale . "_" . $curr_dr_currency;
					$data_to_return = array(
						'cache_key' => $apcu_cache_key,
						'from_cache' => false,
						'product_id' => $product_price_data['productID'],
						'price' => $product_price_data['price']['unitPrice'],
						'url' => $product_price_data['buyLink']['href'] . "/currency.$curr_dr_currency",
						'html-data-keys' => "data-product-name={$product_data['product_name']} data-product-config={$product_data['product_config']}"
					);
					$prices_to_return[$product_data['product_name']][$product_data['product_config']] = $data_to_return;
					apcu_store($apcu_cache_key, $data_to_return, getenv('CACHE_LENGTH'));

					if ( $direct_output && $choose_country_page_on ) {
						$prices_to_return[$product_data['product_name']][$product_data['product_config']]['url'] = "/shop/choose_country.php?p_name={$product_data['product_name']}&pc_name={$product_data['product_config']}";
					}
				}
			}
		}

		if ( $direct_output ) {
			echo json_encode($prices_to_return);
			exit();
		} else {
			return $prices_to_return;
		}
	}

	public static function get_dr_buy_url($curr_dr_locale, $product_ids_str, $curr_dr_currency) {
		$product_prices_url = "http://buy.suse.com/store/suse/$curr_dr_locale/DisplayDRProductInfo/version.2/$product_ids_str/includeVariations.true/content.displayName+thumbnail+buyLink+price+product.externalReferenceID+product.variation+product.allVariations.size/env.design/currency.$curr_dr_currency/output.json";
	}
	
	private function return_subscription_response($response_name, $reference_id, $response_message = 'SUCCESS', $success = true, $auto_retry = true) { 
		if ($success) {
			$success = 'true';
		} else {
			$success = 'false';
		}
		
		echo '<?xml version="1.0" encoding="utf-8" standalone="yes"?>
			<ns2:' . $response_name . ' xmlns:ns2="http://integration.digitalriver.com/SubscriptionService" type="SubscriptionServiceResponse">
				<success>' . $success . '</success>
				<isAutoRetriable>' . $auto_retry . '</isAutoRetriable>
				<referenceID>' . $reference_id . '</referenceID>
				<responseMessage>' . $response_message . '</responseMessage>
			</ns2:' . $response_name . '>
		';
		exit();
	}
	
	public function SubscriptionRenewalSuccessNotificationRequest($data, $log_id) {
		$db = new db_connect();
		$data_str = print_r($data, true);
		$query = "UPDATE `dr_push_api_log` SET `elq_data` = '$data_str' WHERE `id` = '$log_id';";
		$result = $db->query($query);
		
		$response_name = 'SubscriptionRenewalSuccessNotificationRequest';
		$this->return_subscription_response($response_name, $log_id);
	}
	
	public function CancelSubscriptionNotificationRequest($data, $log_id) {
		$db = new db_connect();
		$data_str = print_r($data, true);
		$query = "UPDATE `dr_push_api_log` SET `elq_data` = '$data_str' WHERE `id` = '$log_id';";
		$result = $db->query($query);
		
		$response_name = 'CancelSubscriptionNotificationRequest';
		$this->return_subscription_response($response_name, $log_id);
	}
	
	private function isset_or($variable, $keys_str, $default) {
		$keys = explode("|", $keys_str);
		$total_keys = count($keys);
		if ($total_keys === 5) {
			if ( isset( $variable[$keys[0]][$keys[1]][$keys[2]][$keys[3]][$keys[4]] ) ) {
				if (empty($variable[$keys[0]][$keys[1]][$keys[2]][$keys[3]][$keys[4]])) { return ''; }
				return $variable[$keys[0]][$keys[1]][$keys[2]][$keys[3]][$keys[4]];
			}
		} else if ($total_keys === 4) {
			if ( isset( $variable[$keys[0]][$keys[1]][$keys[2]][$keys[3]] ) ) {
				if (empty($variable[$keys[0]][$keys[1]][$keys[2]][$keys[3]])) { return ''; }
				return $variable[$keys[0]][$keys[1]][$keys[2]][$keys[3]];
			}
		} else if ($total_keys === 3) {
			if ( isset( $variable[$keys[0]][$keys[1]][$keys[2]] ) ) {
				if (empty($variable[$keys[0]][$keys[1]][$keys[2]])) { return ''; }
				return $variable[$keys[0]][$keys[1]][$keys[2]];
			}
		} else if ($total_keys === 2) {
			if ( isset( $variable[$keys[0]][$keys[1]] ) ) {
				if (empty($variable[$keys[0]][$keys[1]])) { return ''; }
				return $variable[$keys[0]][$keys[1]];
			}
		} else if ($total_keys === 1) {
			if ( isset( $variable[$keys[0]] ) ) {
				if (empty($variable[$keys[0]])) { return ''; }
				return $variable[$keys[0]];
			}
		}
		
		return $default;
	}
	
	public function OrderInfo($order_data, $log_id) {
		/////////////////////////////////////////////////////////////////////////////////////
		// Validate the data
		//------------------------------------------------------------
		if (empty($order_data['paymentInfos']['paymentInfo']['billingAddress']['email'])) {
			// need to fail here
		}
		
		
		
		/////////////////////////////////////
		// reformat product data
			if (isset($order_data['lineItems']['item']) && !isset($order_data['lineItems']['item'][0])) {
				$old_data = $order_data['lineItems']['item'];
				unset($order_data['lineItems']['item']);
				$order_data['lineItems']['item'][0] = $old_data;
			}
			foreach ($order_data['lineItems']['item'] as &$lineItem) {
				if (isset($lineItem['lineItemDigitalInfoList']['digitalInfo']) && !isset($lineItem['lineItemDigitalInfoList']['digitalInfo'][0])) {
					$old_data = $lineItem['lineItemDigitalInfoList']['digitalInfo'];
					unset($lineItem['lineItemDigitalInfoList']['digitalInfo']);
					$lineItem['lineItemDigitalInfoList']['digitalInfo'][0] = $old_data;
				}
//				unset($lineItem['productInfo']);
//				unset($lineItem['product']);
//				unset($lineItem['shipToAddress']);
//				unset($lineItem['pricing']);
//				unset($lineItem['lineItemTaxList']);
//				unset($lineItem['currentLineItemState']);
//				unset($lineItem['downloads']);
			}
//			echo json_encode($order_data['paymentInfos']['paymentInfo']['billingAddress']);
//			echo json_encode($order_data);
//			exit();
			
			
			
		


		$common_data = array();
		///////////////////////////////////////
		// global data
		$common_data['optIn'] = $this->isset_or($order_data, 'optIn', 'true');
		
		
		///////////////////////////////////
		// general order information
		$common_data['orderID'] = $this->isset_or($order_data, 'orderID', 'null');
		$common_data['payment_amount'] = $this->isset_or($order_data, "paymentInfos|paymentInfo|paymentAmount|amount", 0);
		$common_data['payment_currency'] = $this->isset_or($order_data, "paymentInfos|paymentInfo|paymentAmount|currencyCode", 0);
		$common_data['subtotal_amount'] = $this->isset_or($order_data, "pricing|subtotal|amount", 0);
		$common_data['subtotal_currency'] = $this->isset_or($order_data, "pricing|subtotal|currencyCode", "USD");
		$common_data['shipping_amount'] = $this->isset_or($order_data, "pricing|shipping|amount", 0);
		$common_data['shipping_currency'] = $this->isset_or($order_data, "pricing|shipping|currencyCode", "USD");
		$common_data['tax_amount'] = $this->isset_or($order_data, "pricing|tax|amount", 0);
		$common_data['tax_currency'] = $this->isset_or($order_data, "pricing|tax|currencyCode", "USD");
		$common_data['testOrder'] = $this->isset_or($order_data, 'testOrder', 'false');
		$common_data['order_date'] = date('Y-m-d H:i:s', strtotime($this->isset_or($order_data, 'submissionDate', '0000-00-00 00:00:00')));
		$common_data['locale'] = $this->isset_or($order_data, 'locale', 'en_US');
		$common_data['payment_method'] = $this->isset_or($order_data, 'paymentInfos|paymentInfo|paymentMethodName', 'null');
		
		
		////////////////////////////////
		// customer data
		$common_data['customerID'] = $this->isset_or($order_data, 'customerID', 'null');
		$common_data['emailAddress'] = $this->isset_or($order_data, 'paymentInfos|paymentInfo|billingAddress|email', 'null');
		$common_data['firstName'] = $this->isset_or($order_data, 'paymentInfos|paymentInfo|customerFirstName', 'null');
		$common_data['lastName'] = $this->isset_or($order_data, 'paymentInfos|paymentInfo|customerLastName', 'null');
		$common_data['loginID'] = $this->isset_or($order_data, 'loginID', 'null');
		
		
		/////////////////////////////////
		// bill to data
		$common_data['billing_first_name'] = $this->isset_or($order_data, 'paymentInfos|paymentInfo|billingAddress|name1', 'null');
		$common_data['billing_last_name'] = $this->isset_or($order_data, 'paymentInfos|paymentInfo|billingAddress|name2', 'null');
		$common_data['billing_company'] = $this->isset_or($order_data, 'paymentInfos|paymentInfo|billingAddress|companyName', 'null');
		$common_data['billing_address'] = $this->isset_or($order_data, 'paymentInfos|paymentInfo|billingAddress|line1', 'null');
		$common_data['billing_city'] = $this->isset_or($order_data, 'paymentInfos|paymentInfo|billingAddress|city', 'null');
		$common_data['billing_state'] = $this->isset_or($order_data, 'paymentInfos|paymentInfo|billingAddress|state', 'null');
		$common_data['billing_postal'] = $this->isset_or($order_data, 'paymentInfos|paymentInfo|billingAddress|postalCode', 'null');
		$common_data['billing_country'] = $this->isset_or($order_data, 'paymentInfos|paymentInfo|billingAddress|country', 'null');
		$common_data['billing_country'] = $this->isset_or($order_data, 'paymentInfos|paymentInfo|billingAddress|phoneNumber', 'null');
		
		
		
		//////////////////////////////////////
		// product data
		$data_sent = array();
		if (isset($order_data['lineItems'])) {
			foreach ($order_data['lineItems']['item'] as $curr_lineItem) {
				if (!empty($curr_lineItem['lineItemDigitalInfoList']['digitalInfo'])) {
					foreach ($curr_lineItem['lineItemDigitalInfoList']['digitalInfo'] as $serialNumber) {
						$data_to_send = $common_data;
						$data_to_send['productID'] = $this->isset_or($curr_lineItem, 'product|productID', 'null');
						$data_to_send['productName'] = $this->isset_or($curr_lineItem, 'productInfo|name', 'null');
						$data_to_send['productSku'] = $this->isset_or($curr_lineItem, 'productInfo|sku', 'null');
						$data_to_send['serialNumber'] = $this->isset_or($serialNumber, 'serialNumber', 'null');
						$data_to_send['unique_key'] = "{$data_to_send['serialNumber']}_{$common_data['orderID']}_{$common_data['emailAddress']}";
						$data_sent[] = $data_to_send;
						$this->set_elq_data($data_to_send, $log_id);
						$this->post_elq_data($data_to_send);
					}
				} else {
					$data_to_send = $common_data;
					$data_to_send['productID'] = $this->isset_or($curr_lineItem, 'product|productID', 'null');
					$data_to_send['productName'] = $this->isset_or($curr_lineItem, 'productInfo|name', 'null');
					$data_to_send['productSku'] = $this->isset_or($curr_lineItem, 'productInfo|sku', 'null');
					$data_to_send['unique_key'] = "{$common_data['orderID']}_{$common_data['emailAddress']}";
					$data_sent[] = $data_to_send;
					$this->set_elq_data($data_to_send, $log_id);
					$this->post_elq_data($data_to_send);
				}
			}
		}

		
		
		
		echo json_encode(array('worked' => true));
		exit();
	}
	
	private function set_elq_data($data, $log_id) {
		$data['elqCookieWrite'] = 0;
		$data['elqFormName'] = 'SUSE_Shop_DL_Integration_Form';
		$data['elqSiteID'] = 1163;
		
		$db = new db_connect();
		$data_str = print_r($data, true);
		$query = "UPDATE `dr_push_api_log` SET `elq_data` = '$data_str' WHERE `id` = '$log_id';";
		$result = $db->query($query);
	}
	
	private function post_elq_data($data) {
		$data['elqCookieWrite'] = 0;
		$data['elqFormName'] = 'SUSE_Shop_DL_Integration_Form';
		$data['elqSiteID'] = 1163;
		
		// now post the form to eloqua
		$post_string = http_build_query($data);
		$url = 'https://secure.eloqua.com/e/f2.aspx';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$curl_result = curl_exec($ch); 
		curl_close($ch);
		
		return $curl_result;
	}

	public function NewSubscriptionNotificationRequest($data, $log_id) {
		////////////////////////////////////////////////////
		// make sure that serial nums piece is array
			if (isset($data['subscriptionInfo']['orderInfo']['lineItemInfo']['lineItemDigitalInfoList']['digitalInfo']) && !isset($data['subscriptionInfo']['orderInfo']['lineItemInfo']['lineItemDigitalInfoList']['digitalInfo'][0])) {
				$old_data = $data['subscriptionInfo']['orderInfo']['lineItemInfo']['lineItemDigitalInfoList']['digitalInfo'];
				unset($data['subscriptionInfo']['orderInfo']['lineItemInfo']['lineItemDigitalInfoList']['digitalInfo']);
				$data['subscriptionInfo']['orderInfo']['lineItemInfo']['lineItemDigitalInfoList']['digitalInfo'][0] = $old_data;
			}
		
		
//		unset($data['subscriptionInfo']['orderInfo']['extendedAttributes']);
//		unset($data['subscriptionInfo']['orderInfo']['lineItemInfo']['productInfo']['extendedAttributes']);
		
		
		$common_data = array();
		$common_data['renewal_date'] = date('Y-m-d H:i:s', strtotime($this->isset_or($data, 'subscriptionInfo|nextRenewalDate', '0000-00-00 00:00:00')));
		$common_data['renewal_date_plus_2_weeks'] = date('Y-m-d H:i:s', strtotime(strtotime('+2 weeks'), $common_data['renewal_date']));
		$common_data['orderID'] = $this->isset_or($data, 'subscriptionInfo|orderInfo|orderID', 'null');
		$common_data['emailAddress'] = $this->isset_or($data, 'subscriptionInfo|originalOrderInfo|paymentInformation|paymentInfo|customerEmail', 'null');
		
		
		$data_sent = array();
		foreach ($data['subscriptionInfo']['orderInfo']['lineItemInfo']['lineItemDigitalInfoList']['digitalInfo'] as $serialNumber)  {
			$data_to_send = $common_data;
			$data_to_send['serialNumber'] = $this->isset_or($serialNumber, 'serialNumber', 'null');
			$data_to_send['unique_key'] = "{$data_to_send['serialNumber']}_{$common_data['orderID']}_{$common_data['emailAddress']}";
			$data_sent[] = $data_to_send;
			$this->set_elq_data($data_to_send, $log_id);
			$this->post_elq_data($data_to_send);
		}
		
		
		
		
		$response_name = 'NewSubscriptionNotificationRequest';
		$this->return_subscription_response($response_name, $log_id);
	}
	
	
	public function dr_push_router() {
		$db = new db_connect();
		try {
			$xml_request_body = file_get_contents('php://input');
			$xml = @simplexml_load_string($xml_request_body);
			if ($xml === false) {
				$call_name = 'unknown';
				$xml_data_arr  = array('blah' => 'testing');
				$serialized_xml_data_arr = serialize($xml_data_arr);
			} else {
				$call_name = $xml->getName();
				$json  = json_encode($xml);
				$xml_data_arr  = json_decode($json, true);
				$serialized_xml_data_arr = serialize($xml_data_arr);
			}

			$data_to_store = compact('call_name', 'xml_data', 'xml_request_body', 'get', 'post', 'server');
			$data_to_store_str = print_r($data_to_store, true);
			$curr_date = date('Y-m-d H:i:s');
			$is_test = (isset($_GET['env']) && $_GET['env'] == 'test') ? 1 : 0;
			
			
			
			$query = "INSERT INTO `dr_push_api_log` (`id`, `is_test`, `log_data`, `call_name`, `serialized_xml_arr`, `created`) VALUES (NULL, '$is_test', '$data_to_store_str', '$call_name', '$serialized_xml_data_arr', '$curr_date');";
			$result = $db->query($query);
			$insert_id = $db->get_insert_id();


			if (method_exists($this, $call_name)) {
				$this->$call_name($xml_data_arr, $insert_id);
			}
		} catch (Exception $ex) {
//			$data_to_send_str = 'exception occured';
//			$query = "UPDATE `dr_push_api_log` SET `elq_data` = '$data_to_send_str';"; //WHERE `id` = '$log_id'
//			$result = $db->query($query);
		}
	} 
	
	public function test_push_router() {
		$url = "https://{$_SERVER['HTTP_HOST']}/common/util/classes/dr_api.php?env=test";
		
		$ch = curl_init();
//		$xml_request_body = file_get_contents('/inet/suse/docroot/common/util/classes/dr_xml/example_dr_order_notification_real.xml');
		$xml_request_body = file_get_contents('/inet/suse/docroot/common/util/classes/dr_xml/example_subscription_info_call_1.xml'); 
//		$xml_request_body = file_get_contents('/inet/suse/docroot/common/util/classes/dr_xml/example_subscription_info_call_2.xml'); 
		
		
		

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_POST, 1);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $xml_request_body);

		//execute post
		$result = curl_exec($ch);
		
		if ($result !== false) {
			print("<pre>" . print_r($result, true) . "</pre>");
			die('curl worked');
		} else {
//			if($errno = curl_errno($ch)) {
//				$error_message = curl_strerror($errno);
//				echo "cURL error ({$errno}):\n {$error_message}";
//			}
			die('curl failed 2');
		}
	}
	
}
