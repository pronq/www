<?php

namespace App\Helpers;

use Cookie;
use App;
use Lang;

class Demandbase {


    private static $instance;
    public $user_data;

    /**
     * Returns the singleton instance of this class.
     *
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    protected function __construct()
    {
    }

    /**
     *  Demandbase API call with clients IP, sets country & lang session vars
     *
     * @returns json encoded Demandbase user data from api call or false
     */
    public function getUserData() {
		// get users ip
		$current_ip = $_SERVER['REMOTE_ADDR'];

		if (function_exists('apache_request_headers')) {
			$headers = apache_request_headers();
			
			if (isset($headers["X-Forwarded-For"])) {
				$x_forwarded_for_arr = explode(',', $headers["X-Forwarded-For"]);
				$current_ip = trim($x_forwarded_for_arr[0]);
			}
		}
//		$current_ip = '80.149.141.3'; // germany //'2620:113:8044:4011:ffff:ffff:ffff:564b'; //'176.74.59.106'; -  // '80.149.141.3'; - not sure // for testing locally
//		$current_ip = '27.127.240.0'; // japan ip
//		$current_ip = '137.65.104.241'; // germany //'2620:113:8044:4011:ffff:ffff:ffff:564b'; //'176.74.59.106'; -  // '80.149.141.3'; - not sure // for testing locally
//      $current_ip = '185.86.151.11'; // uk
//      $current_ip = '137.65.104.235'; // us
        
        $only_chat = isset($_GET['chat']) ? true : false;
		$ip_data_apc_key = "demandbase_data_$current_ip";
//		apcu_clear_cache();
		if (apcu_exists($ip_data_apc_key)) {
			$this->user_data = apcu_fetch($ip_data_apc_key);
			if (is_array($this->user_data)) {
				$this->user_data['from_cache'] = true;
			} else {
				$this->user_data->from_cache = true;
			}
			return response()->json($this->user_data);
		} else {
            $key = env('DEMANDBASE_KEY');

            if(isset($key)) {
                $request_uri = "https://api.ipstack.com/$current_ip?access_key=$key";
                $response = false;
                if($only_chat){
                    $ch = curl_init($request_uri);
                    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, env('DEMANDBASE_API_TIMEOUT'));
                    curl_setopt($ch, CURLOPT_TIMEOUT, env('DEMANDBASE_API_TIMEOUT'));
                    $response = curl_exec($ch);
                    curl_close($ch);
                }

                if($response !== false) {
                    $this->user_data = json_decode((string) $response, true);

                    // Check if the response contains country code
                    if(isset($this->user_data['country_code'])) {
                        $country_code = 'US';
						if (empty($this->user_data['country_code'])) {
                            $this->user_data['country_code'] = $country_code;
                        }
                        else{
                            $country_code = $this->user_data['country_code'];
                        }
                        $this->user_data['registry_country_code'] = $this->user_data['country_code'];
						
                        $countries = Lang::getLangConfig("countryCombinations");
                        foreach($countries as $lang_country => $country_names) {
                            $lang_and_country = explode('|', $lang_country);

                            // if country returned from api call matches the country in the lang config, set session vars & return demandbase data
                            if(isset($lang_and_country[1]) && $country_code == $lang_and_country[1]) {
                                $this->user_data['lang'] = $lang_and_country[0];
                                //Cookie::queue('user_language', $lang_and_country[0], 604800,null, '.microfocus.com', false, false); // store 1 week
                                Cookie::queue('user_country', $country_names[1], 604800,null, '.microfocus.com', false, false); // store 1 week
                                session(['lang' => $lang_and_country[0]]);
                                session(['country_code' => $lang_and_country[1]]);
                                session(['country_name' => $country_names[1]]);
                                session(['english_country' => $country_names[0]]); // for contact us pg
								$this->user_data['from_cache'] = false;
                                $res = $this->user_data;
								apcu_store($ip_data_apc_key, $res, 604800); // store 1 week
                                break;
                            }
                        }
						if(empty($res)) {
							$res = ["error" => '0'];
						}
                    } else {
                        // here the error is handled if the response is different from false and doesn't contain country_code
                        $res = ["error" => '1','message' => 'no country code',"registry_country_code" => 'US'];
                    }
                } else {
                    $res = ["error" => '2','message' => 'no response',"registry_country_code" => 'US'];
                }
            } else {
                $res = ["error" => '3','message' => 'no key',"registry_country_code" => 'US'];
            }

            return response()->json($res);
        }
    }

}