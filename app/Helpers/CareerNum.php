<?php namespace App\Helpers;

class CareerNum {

	/*
	* uses base url to get path to images directory
	*/
	public static function getTotalJobsCount() {
		$url = "https://attachmatehr.silkroad.com/epostings/index.cfm?fuseaction=app.allpositions&company_id=15495&version=6";
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_REFERER, "http://www.novell.com");
		curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		$output = curl_exec($ch);
		curl_close($ch);
		
		preg_match_all("/cssAllJobListPositionHref/", $output, $matches);
		
		$count = count($matches[0]);
		
		if(strlen($count) < 3)
			$count = "0".$count;
		
		$chars = str_split($count);
		
		$i = 0;
		$returnStr = "";
		foreach($chars as $char){
			$returnStr .= '<span class="career_num num_'.$char.'';
			if($i == 2) $returnStr .= ' num_last';
			$returnStr .= '">'.$char.'</span>';
			$i++;
		}
		return $returnStr;
	}	

}

?>
