<?php namespace App\Helpers;

class ProductAPI {

	/*
	* uses base url to get path to images directory
	*/
	public static function getProductInfo($fields) {
		$url = "https://www.suse.com/common/util/classes/dr_api.php?action=get_product_prices";
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_REFERER, "http://www.novell.com");
		curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		
		$output = curl_exec($ch);
		curl_close($ch);
		
		$returnStr = json_decode($output);
		return $returnStr;
	}	

}

?>
