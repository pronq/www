<?php

namespace App\Helpers;

class TrialDownload {

	/*
	 * Sends requests to the trial download web service
	 *
	 * @param  string  $endpoint
	 * @param  array  $post_data
	 * @param  array $headers
	 *
	 * @return array $response
	 */
	public static function sendRequest($endpoint, $post_data = array(), $headers = array( 'Content-Type: application/x-www-form-urlencoded', 'Accept: application/json, text/javascript' )) {

		$url = env('TRIAL_DOWNLOAD_API') . $endpoint;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POST, count($post_data));
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
		$response = curl_exec($ch);
		$request_info = curl_getinfo($ch);
		curl_close($ch);

		$response = json_decode($response, true);

		if(empty($response))
			$response = [];

		$response['http_code'] = $request_info['http_code'];
		$response['url'] = $request_info['url'];
		$response['content_type'] = $request_info['content_type'];

		if(!empty($response['errorMessage'])) {
			$message = 'Response from: ' . $url;
			Logging::info('trial_downloads', 'trial-downloads.log', $message, $response);
		}

		return $response;
	}

	/*
	 *  Calls Trial Download web service. Posts product id, email, & requiresSupport, returns downloadCode to pass to Eloqua to build download link in email to customer
	 *
	 * @return array $response
	 */
	public static function createTrial($submit_data) {

		$response = [];
		$post_data = [];
		$post_data['productID'] = filter_var($submit_data['product_id'], FILTER_SANITIZE_STRING);
		$post_data['emailAddress'] = filter_var($submit_data['email'], FILTER_SANITIZE_EMAIL);
		$post_data['requiresSupport'] = !empty($submit_data['requiresSupport']) ? true : false;
		$post_data['firstName'] = filter_var($submit_data['firstName'], FILTER_SANITIZE_STRING);
		$post_data['lastName'] = filter_var($submit_data['lastName'], FILTER_SANITIZE_STRING);

		if(empty($submit_data['eventId']) || empty($submit_data['eventName'])) {

			$response['errorMessage'] = 'eventId or eventName hidden fields in form template are missing.';
			Logging::info('trial_downloads', 'trial-downloads.log', $response['errorMessage']);

		} else if(!empty($post_data['productID']) && !empty($post_data['emailAddress']) && !empty($post_data['firstName']) && !empty($post_data['lastName'])) {

			$response = self::sendRequest('CreateTrial', $post_data);

			if (!empty($response)) {

				if($response['returnCode'] == 'OK' && !empty($response['data']['downloadCode'])) {

					//Log the email for the trial request. ideally Triggered after sending lead to Eloqua, but we really just need the downloadCode to make this call
					self::sendRequest('LogEmail', array('downloadCode' => $response['data']['downloadCode']));

				} else {
					// this will get logged above in sendRequest
					// $response['errorMessage'] will have more details on the error
				}

			} else {
				$response['errorMessage'] = 'No response from the api';
				Logging::info('trial_downloads', 'trial-downloads.log', $response['errorMessage']);
			}
		} else {
			$response['errorMessage'] = 'No email or product id provided.'; //this should never be the case
			Logging::info('trial_downloads', 'trial-downloads.log', $response['errorMessage']);
		}

		return $response;
	}

	/*
	 * Gets the download page trial info
	 *
	 * @param  string  $download_code
	 *
	 * @return array  $response
	 */
	public static function getFiles($download_code) {
		$response = self::sendRequest('GetFiles', array('downloadCode' => $download_code));
		if (empty($response)) {
			// no response from api
			$response['errorMessage'] = 'No response from the api';
			Logging::info('trial_downloads', 'trial-downloads.log', $response['errorMessage']);
		}

		return $response;
	}

	/*
	 * Makes LogDownload api call when a file is clicked to download
	 */
	public static function logDownload() {

		$response = self::sendRequest('LogDownload', $_POST);
		if (!empty($response)) {
			if($response['returnCode'] !== 'OK') {
				// errors get logged above
			}
		} else {
			// no response from api
			$response['errorMessage'] = 'No response from calling LogDownload';
			Logging::info('trial_downloads', 'trial-downloads.log', 'Request url: ' . Request::fullUrl() . ' - ' . $response['errorMessage']);
		}

		return $response;
	}

}
