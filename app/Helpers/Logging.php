<?php namespace App\Helpers;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Logging {

    public static function info($channel, $logFile, $message, $context = array()) {
        $log = new Logger($channel);
        $log->pushHandler(new StreamHandler(storage_path('logs/'.$logFile), Logger::INFO));
        $log->pushProcessor(new \Monolog\Processor\WebProcessor);

        /*$browserHanlder = new \Monolog\Handler\BrowserConsoleHandler(\Monolog\Logger::INFO);
        $log->pushHandler($browserHanlder);*/

        if(!empty($context)) {
            $log->info($message, $context);
        } else {
            $log->info($message);
        }
    }
}
