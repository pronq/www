<?php

namespace App\Helpers;

use Lang;
use Config;
use Request;
use File;
use URL;
use App\Helpers\FormBuilder;

abstract class Asset {

	public static $assets;

	/*
	 * Return the HTML for our CSS link tags
	 */


	public static function getCss($tag, $route_only = false) {
		$output = '';
		// If not live, or if on IE < 10 (which can't handle the minifying)
		if (Config::get('app.debug') || $route_only || (isset($_SERVER["HTTP_USER_AGENT"]) && preg_match("/MSIE \d\./i", $_SERVER["HTTP_USER_AGENT"]))) {
			foreach (Asset::assetList('css', $tag, Request::path(), $route_only) as $file) {
				$output .= '<link rel="stylesheet" href="' . url('assets/css/' . $file) . '">' . "\r\n";
			}
		} else {
			$assets = Asset::assetList('css', $tag, Request::path());
			$output = '<link rel="stylesheet" href="' . url( '/min/?b=assets/css&f=' . implode(',', $assets) ) . '&v=' . env('CSS_JS_VERSION') . '">';
		}
		return $output;
	}

	/*
	 * Return the HTML for our JS script tags
	 */

	public static function getJs($tag, $route_only = false, $no_jquery = false) {
		$output = '';
		if (Config::get('app.debug') || $route_only) {
			foreach (Asset::assetList('js', $tag, Request::path(), $route_only) as $file) {
				if ($no_jquery && strpos($file, 'jquery') > -1 ) {
					continue;
				}
				$output .= '<script src="' . url('assets/js/' . $file) . '"></script>' . "\r\n";
			}
		} else {
			$assets = Asset::assetList('js', $tag, Request::path());
			if ($no_jquery) {
				$index = 0;
				foreach ($assets as $asset) {
					if (strpos($asset, 'jquery') > -1) {
						unset($assets[$index]);
					}
					$index++;
				}
			}
			$output = '<script type="text/javascript" src="' . url( '/min/?b=assets/js&f=' . implode(',', $assets) )  . '&v=' . env('CSS_JS_VERSION') . '"></script>';
		}
		if (!$route_only) {
			$output = Asset::getJsConfig() . PHP_EOL . $output;
		}
		return $output;
	}

	/**
	 * out a script tag that contains our config data
	 */
	public static function getJsConfig() {

	}

	/*
	 * Read the asset list from the manifest file and build a list of files to include
	 */

	public static function assetList($type, $tag, $route = '', $route_only = false) {
		$manifest = json_decode(File::get(public_path('assets/' . $type . '-manifest.json')), true);
		$list = array();
		$route = trim($route, '/');
		foreach ($manifest[$tag] as $route_group => $files_arr) {
			$within_route = false;
			foreach (explode('|', $route_group) as $each_route) {
				if (preg_match('/^(\w\w\-\w\w\/)?' . str_replace('/', '\/', $each_route) . '$/', $route)) {
					$within_route = true;
				}
			}
			if (($route_group == 'core' && !$route_only) || $within_route) {
				foreach ($files_arr as $file) {
					$list[] = $file;
				}
			}
		}
		return $list;
	}


	public static function includeForm($form_path) {
		$form = new FormBuilder();
		echo $form->display_form($form_path);
		return;
	}


	/* /form-builder-transport/
	 *  Handles all validation and routing for all Form Builder forms
	 */
	public static function formBuilderTransport() {
		$form = new FormBuilder();

		// Validation and error handling

		if ( empty($_POST['form_config']) ) {
			return self::transportError('Missing Form Config');
		}

		$data = $_POST;
		$form_config = $form->load_config_cache($data['form_config']);
		if ( empty($form_config) ) {
			return self::transportError('Invalid Form Config');
		}
		unset($data['form_config']);

		// optin all submits by default except when w_optin is set to 'no'
		if (isset($data['w_optin_default']) && $data['w_optin_default']) {
			$data['w_optin'] = 'yes';
			unset($data['w_optin_default']);
		}
		
		$errors = $form->validate_form_data($data,$form_config);
		if ( !empty($errors) ) {
			return self::transportError($errors);
		}

		// Merge user data with hidden config data and demandbase data
		$submit_data = $form->build_submit_data($data,$form_config);

		// trial downloads
		if(isset($submit_data['w_eula']) && $submit_data['w_eula'] == 'yes') {
			$submit_data['w_export_agreement'] = 'yes';
		}

		if(!empty($submit_data['createTrial'])) {

			$result = TrialDownload::createTrial($submit_data);

			if(!empty($result['data']['downloadCode'])) {
				// need product url & download url for Eloqua email
				$request_path = preg_replace('/\?.+$/', '', $submit_data['w_form_submit_url']); // remove GET params
				$request_path = preg_replace('/\.html$/', '', $request_path);
				$submit_data['ty_download_link'] = rtrim($request_path, '/') . '/download?downloadCode=' . $result['data']['downloadCode'];

				if(!isset($submit_data['ty_product_information_url']))
					$submit_data['ty_product_information_url'] = URL::to(preg_replace('/\/trial/', '', $request_path));

				if(!isset($submit_data['ty_how_to_buy_url']))
					$submit_data['ty_how_to_buy_url'] = URL::to(preg_replace('/\/trial/', '/how-to-buy', $request_path));

				if(isset($result['data']['serialNumber'])) {
					$submit_data['w_trial_serial_number'] = $result['data']['serialNumber'];
				}
			} else {
				return self::transportError(!empty($result['errorMessage']) ? $result['errorMessage'] : 'There was an error processing your request.');
			}
		}

		$form->postToEloqua($submit_data,$form_config);

		// Return Success Message
		$json_response = array();
		$json_response['success'] = true;

		// Note: this js_function stuff probably isn't used anywhere anymore
		if (isset($form_config['global_settings']['after_validate_js_function'])) {
			$json_response['after_validate_js_function'] = $form_config['global_settings']['after_validate_js_function'];
		}

		if (isset($submit_data['thankyouPage'])) {
			$json_response['thankyouPage'] = $submit_data['thankyouPage'];
		}

		$json_response['inline_thankyou_html'] = false;
		if (!empty($form_config['global_settings']['inline_thankyou']['name']) && !empty($form_config['global_settings']['inline_thankyou']['replace_id'])) {
			$merged_thankyou = $form_config['global_settings']['inline_thankyou']['name'];
			$json_response['inline_thankyou_html'] = Lang::view('includes::form/thankyou/'.$merged_thankyou)->render();
			$json_response['inline_thankyou_replace_id'] = $form_config['global_settings']['inline_thankyou']['replace_id'];
		}


		$response = response()->json($json_response)
			->setCallback(Request::input('callback'));
		if(isset($submit_data['c_email'])) {
			$response->withCookie(cookie()->forever('mf_email', $submit_data['c_email']));
		}
		return $response;

	}

	/* Shorthand to format errors to return to user. Used for formBuilderTransport()
	 *   single error: return transportError('Invalid form')
	 *   field errors: return transportError(['firstName'=>'required','lastName'=>'required'])
	 */
	protected static function transportError($error) {
		$json_response = array();
		$json_response['success'] = false;

		if ( is_array($error) ) {
			$json_response['validation_results'] = $error;
		} else {
			$json_response['message'] = $error;
		}

		return response()->json($json_response)
			->setCallback(Request::input('callback'));

	}


	public static function breadcrumbs($crumbs) {
		echo '<div itemscope itemtype="http://schema.org/BreadcrumbList">'."\n";
		$position = 1;
		foreach($crumbs as $item) {
			if(isset($item['url'])) {
				?>
				<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a href="<?php echo URL::to($item['url']); ?>" itemprop="item">
			<span itemprop="name"><?php echo $item['name']; ?></span>
		</a>
		<meta itemprop="position" content="<?php echo $position++; ?>" />
	</span>
			<?php
			} else {
				echo $item['name'];
			}
		}
		echo '</div>'."\n";
	}


}
