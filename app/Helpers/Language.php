<?php

namespace App\Helpers;

use Request;
use Session;
use URL;
use Config;

class Language {

	protected $currentLang;
	protected $defaultLang;
	protected $allLanguages;
	
	public function __construct() {
		
		$this->defaultLang = $this->getLangConfig("default");
		$this->currentLang = $this->defaultLang;
        $this->setCurrentLang($this->pathLang());
	}
	
	public function getLangConfig($setting) {
		if ( $setting == "default" && isset($this->defaultLang) ) return $this->defaultLang;
		$site = getenv('SITE');
		return config("lang.$site.$setting");
	}

	public function view($path) {
		if(strpos($path,'::') === false) {
			$space = "content";
		} else {
			list($space,$path) = explode('::',$path);
		}
		
		$lang = $this->currentLang();
		$en_view_path = "$space::" . $this->defaultLang . "/" . $path;
		$lang_view_path = "$space::$lang/$path";

		if ( view()->exists($lang_view_path) ) {
			return view($lang_view_path);
		} else if ( view()->exists($en_view_path) ) {
			return view($en_view_path);
		}
		
		return \App::abort(404);
	}
	
	/*
	 * get our currently set language code
	 */
	public function currentLang() {
		return $this->currentLang;
	}

	/*
	 * set the language code of the currently displayed page 
	 *   when the requested language isn't available, set back to english.
	 */
	public function setCurrentLang($lang) {
		if ( $this->isAnyLang($lang) ) {
			$this->currentLang = $lang;
		}

		return $this;
	}
	
	public function isAnyLang($lang) {
		if(empty($this->allLanguages)) {
			$this->allLanguages = array();
			foreach(scandir(public_path('../resources/translations/content/')) as $file) {
				if(preg_match('/^\w\w\-\w\w$/',$file)) {
					$this->allLanguages[$file] = $file;
				}
			}
		}
		return isset($this->allLanguages[$lang]);
	}
	
	/*
	 * check if in the primary language list (in the language menu)
	 */
	public function isMainLang($lang_code) {
		$langs = $this->getLangConfig("languages");
		return isset($langs[$lang_code]);
	}
	
	public function pathLang($prefix = '') {
		$pathLang = Request::segment(1);


		if ( !empty($pathLang) && $this->isAnyLang($pathLang) ) {
			return $prefix . $pathLang;
		}
		return '';
	}
	
	public function pathWithoutLang($path) {
		$path = addslashes($path);
		$url_parsed = parse_url($path);
		
		if ( isset($url_parsed['path']) ) {
			$path = $url_parsed['path'];
			
			$trimmed_path = trim($path, '/');
			list ( $lang ) = explode('/', $trimmed_path);
					
			if ( !empty($lang) && $this->isAnyLang($lang) ) {
				$path = "/" . substr($trimmed_path, strlen($lang) + 1);
			} else {
				$path = "/" . $trimmed_path;
			}
		} else {
			$path = "/";
		}
		
		if (isset($url_parsed['query'])) { 
			$path .= "?{$url_parsed['query']}";
		}

		return $path;
	}

	/*
	 *	 Removes lang from url's that should never need a language
	 * 	@see langless_paths.php config, VerifyLanguage.php, BaseService.php
	 */
	public function isLanglessPath($lang, $url = null) {

		// get the host and path
		$url_prev = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
				if(isset($_SERVER['HTTP_REFERER'])){
				$url_prev = urldecode($_SERVER['HTTP_REFERER']);
		}
		//$url_parsed = parse_url(URL::previous());
		$url_parsed = parse_url($url_prev);
		$scheme = isset($url_parsed['scheme']) ? $url_parsed['scheme'] : 'https';
		$host = $url_parsed['host'];
		$path = isset($url_parsed['path']) ? $url_parsed['path'] : '/';
		if (!empty($url_parsed['query'])) {
			$path .= "?{$url_parsed['query']}";
		}

		// override path if we have it set by url parameter
		if ( !is_null($url) ) {
			$previous_url_parsed = parse_url($url);
			$scheme = isset($previous_url_parsed['scheme']) ? $previous_url_parsed['scheme'] : $scheme;
			$path = isset($previous_url_parsed['path']) ? $previous_url_parsed['path'] : '/';
			if (!empty($previous_url_parsed['query'])) {
				$path .= "?{$previous_url_parsed['query']}";
			}
		}

		$path_without_lang = self::pathWithoutLang($path);		

		// paths that should never need a language ie: selfreg
		$use_langless_path = $use_langless_hosts = false;
		$langless_path = Config::get('langless_paths.paths');
		$langless_hosts = Config::get('langless_paths.hosts');
		foreach ($langless_path as $p => $match_type) {
			switch ($match_type) {
				case 'regex':
					$pattern = $p;
					if (preg_match($pattern, $path)) {
						$use_langless_path = true;
					}
					break;

				case 'strpos':
					if (strpos($path, $p) > -1) {
						$use_langless_path = true;
					}
					break;
			}
		}
		foreach ($langless_hosts as $h) {
			if (strpos($host, $h) > -1) {
				$use_langless_hosts = true;
			}
		}

		if ($use_langless_path || $use_langless_hosts) {
			$new_url = "$scheme://" . $host . $path_without_lang;
			return array('url' => $new_url, 'langless_path' => true);
		}
		else {
			$new_url = "$scheme://" . $host . $lang . $path_without_lang;
			return array('url' => $new_url, 'langless_path' => false);
		}

	}
	
	/*
	 * Custom function to make include files check for current language
	 * this function isn't called directly, but rather through the custom @include blade function found within ViewServiceProvider
	 */

	public function parseInclude($path, $data = []) {
		if(strpos($path,'::') === false) {
			$path = "includes::".$path;
		}
		
		return $this->view($path)->with($data)->render();
	}


	/*
	 * Load the necessary translation file based on our current language
	 */

	public function parseTranslationFile() {
		$numargs = func_num_args();
		if ($numargs < 1) {
			return array();
		}
		$args_list = func_get_args();

		$trans_arr = [];
		foreach ($args_list as $view) {

			if(strpos($view, "errors/") === 0) {

				list($namespace,$path) = explode('/',$view);

				$this->setCurrentLang($this->pathLang());
			} else if (strpos($view, '::')) {

				list($namespace,$path) = explode('::',$view);
			}
			else {
				$namespace = 'content';
				$path = $view;
			}

			$path = $this->pathWithoutLang($path);

			
			// load language specific file if it exists
			$file = public_path('../resources/translations/' . $namespace . '/' . $this->currentLang() . $path . '.html');

			// if it doesn't exist, load our default language trans file instead
			if ( !file_exists($file) ) {
				$file = public_path('../resources/translations/' . $namespace . '/' . $this->defaultLang . $path . '.html');
				if ( !file_exists($file) ) {
					continue;
				}
				$this->setCurrentLang($this->defaultLang);
			}

			$doc = new \DOMDocument();
			libxml_use_internal_errors(true);
			$contents = mb_convert_encoding(file_get_contents($file),'HTML-ENTITIES','UTF-8');
			// allow use of {{ URL::to('/path/to/url') }}
			$contents = preg_replace_callback('/\{(\{|!!)\s*URL::(\w+)\((\'|")([^\'"]+)\3\)\s*(\}|!!)\}/',function($matches) {
				$function = $matches[2];
				return URL::$function($matches[4]);
			},$contents);
			
			if (!empty($contents)) {
				$doc->loadHTML($contents);
				$parentDivs = $doc->getElementsByTagName('body')->item(0)->childNodes;
				foreach ( $parentDivs as $item ) {
					if( $item->nodeType !== 1 || $item->getAttribute('id') == '') {
						continue;
					}

					$id = $item->getAttribute('id');
					$trans_arr[$id] = $this->parseTranslationDiv($item,$doc);
				}
			}
		}

		return $trans_arr;
	}
	
	/*
	 * Helper function for parseTranslationFile
	 */

	private function parseTranslationDiv($div,$doc) {
		
		// allow nested array for repeating elements
		if($div->getAttribute('data-type') && $div->getAttribute('data-type') === 'array') {
			$return = array();

			foreach($div->childNodes as $arrayItem) {
				if( $arrayItem->nodeType !== 1 ) {
					continue;
				}
				
				$sub_array = array();
				foreach($arrayItem->childNodes as $dataNode) {
					if( $dataNode->nodeType !== 1 || $dataNode->getAttribute('id') == '') {
						continue;
					}
					
					$id = $dataNode->getAttribute('id');
					$sub_array[$id] = $this->parseTranslationDiv($dataNode,$doc);
				}
				$return[] = $sub_array;
			}
			return $return;
		} else {
			if($div->getAttribute('data-content')) {
				return $div->getAttribute('data-content');
			} else {
				$text = "";
				foreach($div->childNodes as $child) {
					$text .= $doc->saveHTML($child);
				}
				return trim($text);
			}
		}
		
	}

	public function getFileLastModDate($path){
		// load language specific file if it exists
		$file = public_path('../resources/translations/content/' . $this->currentLang() . $path . '.html');
		$indexFile = public_path('../resources/translations/content/' . $this->currentLang() . $path . '/index.html');
		$trialFile = public_path('../resources/translations/content/' . $this->currentLang() . $path . '-download/index.html');
		$lastModTime = date("Y/m/d", mktime(0, 0, 0, 01, 01, 2015));
		// if it doesn't exist, load our default language trans file instead
		if ( !file_exists($file) ) {
			$file = public_path('../resources/translations/content/' . $this->defaultLang . $path . '.html');
		}

		if ( !file_exists($indexFile) ) {
			$indexFile = public_path('../resources/translations/content/' . $this->defaultLang . $path . '/index.html');
		}

		if (file_exists($file) ) {
			$lastModTime = date("Y/m/d",filemtime($file));
		}
		else if (file_exists($indexFile) ) {
			$lastModTime = date("Y/m/d",filemtime($indexFile));
		}
		else if (file_exists($trialFile) ) {
			$lastModTime = date("Y/m/d",filemtime($trialFile));
		}
		return $lastModTime;
	}
	
	
	/////////////////// LEGACY METHODS /////////////////////
	
	public function currentCountryCode() {
		$country = session()->has('country_code') ? session('country_code') : 'US'; // config("lang.$site.languages.$default_lang_code")
		return $country;
	}
	
	public function onValidPath() { return $this->pathLang(); }
	public function currentCode() { return $this->currentLang(); }
	
	public function listFull($exclude_current = false) {
		$lang_arr = $this->getLangConfig("languages");
		if ( $exclude_current ) {
			unset($lang_arr[$this->currentLang()]);
		}
		return $lang_arr;
	}
	
	public function listAllCountriesByLang($lang) {
		$countries = $this->getLangConfig("countryCombinations");
		$found_countries = array();
		foreach($countries as $key => $country) {
			$comboKeys = explode("|",$key);
			if (isset($comboKeys[0]) && $comboKeys[0] == $lang) {
				$found_countries[$comboKeys[1]] = $country;
			}
		}
		
		return $found_countries;
	}

	public function getAllLangPathsByEnPath($en_path) {
		$langs = $this->getLangConfig("languages");
		$paths = array();
		$paths[] = $en_path;
		foreach ( $langs as $lang_code => $lang_name ) {
			if ( $lang_code == $this->getLangConfig("default") ) {
				continue;
			}

			$paths[] = "/" . $lang_code . $en_path;
		}
		return $paths;
	}
	
	public function getLangLessPathByAnyPath($path) {
		return $this->pathWithoutLang($path);
	}

}
