<?php
namespace App\Helpers;

use App;
use App\Facades\Lang;
use App\Helpers\Docrep;
use App\Helpers\MediaBin;
use Request;

class Resources {

    /*
     * Pulls solution, type, & product filters from Docrep for the resources page
     *
     */
    public function getFilters() {

        if(!\App::environment('test','dev') && empty($_GET['clear_view_cache']) && apcu_exists('resources_filters')) {
            $filters = apcu_fetch('resources_filters');
        } else {
            
            if(env('DOCSOURCE') == "docrep") {
                // use docrep
                $query["taxonomy"] = "true";
                $rawFilters = Docrep::sendRequest($query);
            }
            else {
                $rawFilters = MediaBin::getAvailableFilters();
                if(isset($rawFilters->MBAllowedValuesDataModel)) {
                    $rawFilters = $rawFilters->MBAllowedValuesDataModel;
                    if(isset($rawFilters->allowedValues)) {
                        $rawFilters = $rawFilters->allowedValues;
                    }
                }               
            }
            
            //var_dump($rawFilters);
            
            $filters = [];
            // create associate array with url string as keys
            foreach($rawFilters as $filterType => $filterData) {


                if($filterType != "solution_product" && $filterType != "product_solutions"){
                    foreach($filterData as $filter) {
                        $filterReplacements = array('&' => '%26', ' ' => '-', '/' => '-', ',' => '-comma');
                        if(gettype(str_replace(array_keys($filterReplacements), array_values($filterReplacements), $filter)) != "array")
                        $filterKey = strtolower(str_replace(array_keys($filterReplacements), array_values($filterReplacements), $filter));
                        // else{
                        //     //dd($filterData);
                        //     echo "array ";
                        // }
                        if(env('DOCSOURCE') == "docrep") {
                            $filters[$filterType][] = ['key' => $filterKey, 'value' => $filter];
                        }
                        else {
                            if($filterType == "Format") {
                                $filterType = "type";
                            }
                            $filters[strtolower($filterType)][] = ['key' => $filterKey, 'value' => $filter];
                        }
                    }
                }
                else{
                    $filters[$filterType] = $rawFilters[$filterType];
                }
            }
            
            //var_dump($filters);
            
            $filt_values = array_values($filters['solution']);

            asort($filters['solution']);

            apcu_store( 'resources_filters', $filters, getenv('CACHE_LENGTH'));
        }

        return $filters;
    }
    
    public function convertToMediaBinLocale($key) {
        switch ($key) {
            case "ar-sa":
                return "Arabic (Saudi Arabia)";
                break;
            case "zh-tw":
                return "Chinese (Traditional)";
                break;
            case "zh-cn":
                return "Chinese (Simplified)";
                break;
            case "cs-cz":
                return "Czech";
                break;
            case "da-da":
                return "Danish";
                break;
            case "nl-nl":
                return "Dutch";
                break;
            case "en-us":
                return "English (US)";
                break;
            case "en-gb":
                return "English (EE)";
                break;
            case "en-au":
                return "English (AP)";
                break;
            case "fi-fi":
                return "Finnish";
                break;
            case "fr-ca":
                return "French (Canada)";
                break;
            case "fr-fr":
                return "French";
                break;
            case "de-de":
                return "German";
                break;
            case "el-gr":
                return "Greek";
                break;
            case "hu-hu":
                return "Hungarian";
                break;
            case "it-it":
                return "Italian";
                break;
            case "ja-jp":
                return "Japanese";
                break;
            case "ko-kr":
                return "Korean";
                break;
            case "no-no":
                return "Norwegian";
                break;
            case "pl-pl":
                return "Polish";
                break;
            case "pt-br":
                return "Portuguese (Brazil)";
                break;
            case "pt-pt":
                return "Portuguese (Portugal)";
                break;
            case "ru-ru":
                return "Russian";
                break;
            case "sl-sl":
                return "Slovenian";
                break;
            case "es-es":
                return "Spanish (International)";
                break;
            case "es-mx":
                return "Spanish (LA)";
                break;
            case "sv-se":
                return "Swedish";
                break;
            case "th-th":
                return "Thai";
                break;
            case "tr-tr":
                return "Turkish";
                break;
        }
    }

    public function convertFromMediaBinLocale($key) {
        switch ($key) {
            case "Arabic (Saudi Arabia)":
                return "ar_SA";
                break;
            case "Chinese (Traditional)":
                return "zh_TW";
                break;
            case "Chinese (Simplified)":
                return "zh_CN";
                break;
            case "Czech":
                return "cs_CZ";
                break;
            case "Danish":
                return "da_DA";
                break;
            case "Dutch":
                return "nl_NL";
                break;
            case "English (US)":
                return "en_US";
                break;
            case "English (EE)":
                return "en_GB";
                break;
            case "English (AP)":
                return "en_AU";
                break;
            case "Finnish":
                return "fi_FI";
                break;
            case "French (Canada)":
                return "fr_CA";
                break;
            case "French":
                return "fr_FR";
                break;
            case "German":
                return "de_DE";
                break;
            case "Greek":
                return "el_GR";
                break;
            case "Hungarian":
                return "hu_HU";
                break;
            case "Italian":
                return "it_IT";
                break;
            case "Japanese":
                return "ja_JP";
                break;
            case "Korean":
                return "ko_KR";
                break;
            case "Norwegian":
                return "no_NO";
                break;
            case "Polish":
                return "pl_PL";
                break;
            case "Portuguese (Brazil)":
                return "pt_BR";
                break;
            case "Portuguese (Portugal)":
                return "pt_PT";
                break;
            case "Russian":
                return "ru_RU";
                break;
            case "Slovenian":
                return "sl_SL";
                break;
            case "Spanish (International)":
                return "es_ES";
                break;
            case "Spanish (LA)":
                return "es_MX";
                break;
            case "Swedish":
                return "sv_SE";
                break;
            case "Thai":
                return "th_TH";
                break;
            case "Turkish":
                return "tr_TR";
                break;
        }
    }


    /*
     * Pulls resources either from the cache, or by $query from the Docrep api. Returns an array of Resource objects
     *
     * @param Array $query - arguments to pass to Docrep
     * @return Array $resources
     */
    public function getAll($query) {
        $resources = [];
        if(!\App::environment('test','dev') && empty($_GET['clear_view_cache']) && apcu_exists('resources_'.Lang::currentLang()) && empty($query)) {
            $resources = apcu_fetch('resources_'.Lang::currentLang());
        } else {

            // if on a lang path pull the translated resource, if none docrep returns the english version by default, if no english the first asset it can find will be returned (could be any lang)
            $current_lang = Lang::pathLang();
            if(!empty($current_lang)) $query["language"] = $current_lang;
            
            // $query["product"] may be a single string, or an array of strings
            if(isset($query["product"])) {
                if (is_array($query["product"])) {
                    foreach($query["product"] as $key => $value) {
                        $query["product"][$key] = $this->normalizeFilter($value)[0];
                    }
                } else {
                    $query["product"] = $this->normalizeFilter($query["product"]);
                }
            }
            if(isset($query["solution"])) $query["solution"] = $this->normalizeFilter($query["solution"]);
            if(empty($query)) $query = array("unique_id" => array()); // required for docrep
            
            $query['show_expired'] = env('SHOW_EXPIRED_ASSETS') ?? false;
            
            if(env('DOCSOURCE') == "docrep") {
                // use docrep
                $response = Docrep::sendRequest($query);
            }
            else {
                // use mediabin
                if($query == null) {
                    $query = array(
                        'Language' => array("English (US)"),
                        "Primary Asset" => array(1)
                    );
                }
                else if(!isset($query["Language"]) && !isset($query["language"])) {
                    $query["Language"] = array("English (US)");
                    $query["Primary Asset"] = array(1);
                }
                else if(isset($query["Language"]) && sizeof($query["Language"]) == 1 && in_array("English (US)", $query["Language"])) {
                    $query["Primary Asset"] = array(1);
                }

                $query['Expiration Date'] = ["{Op{GreaterThan}}{time{". date('n/j/Y g:i:s A') ."}}"];
                unset($query['show_expired']);
                
                if(isset($query["format"])) {
                    $query["Format"] = $query["format"];
                    unset($query['format']);
                }

                if(isset($query["product"])) {
                    $query["Product"] = $query["product"];
                    unset($query['product']);
                }

                if(isset($query["solution"]) && sizeof($query["solution"]) > 0) {
                    echo sizeof($query["solution"]);
                    $query["Solution"] = $query["solution"];
                    unset($query['solution']);
                }
                else {
                    unset($query['solution']);
                }
                
                // remove suse items
                $query["Business Unit"] = array("{Op{Inequality}}SUSE");

                if(isset($query["language"])) {
                    $query["Language"][] = $this->convertToMediaBinLocale($query["language"]);
                    unset($query['language']);
                }

                if(isset($query["keyword"])) {
                    if(strpos($query["keyword"], '"') === 0 || strpos($query["keyword"], '"') > 0) {
                        $query["Keywords"][] = str_replace('"', '', $query["keyword"]);
                    }
                    else {
                        $query["Keywords"] = explode(" ",$query["keyword"]);
                    }
                    unset($query['keyword']);
                }

                if(isset($query["recent"])) {
                    unset($query['recent']);
                }
                
                $query["Hidden Flag"] = array("No");        
                
                //var_dump($query);

                $MBquery = array(
                    'pageNum' => 1,
                    'pageSize' => 1000,
                    'returnItems' => array("Title","Publish Date","Expiration Date","Format","Audience","Thumbnail","Gate","Description","Production URL","Language","Part Number"),
                    'searchItems' => $query,
                    'sortOn' => Request::input('sortOn', 'Title'),
                    'sortAscending' => Request::input('sortAscending', "true")
                );

                $response = MediaBin::searchMetadata($MBquery);
            }

            //var_dump($response);
            
            if(!empty($response)) {
                $resourcesArr = [];
                foreach($response as $id => $item) {
                    //var_dump($item);
                    if(env('DOCSOURCE') == "mediabin") {
                        $itemArr = array(
                            'id' => $item->assetId,
                            'publish_date' => $item->returnData->{'Publish Date'},
                            'expiration_date' => $item->returnData->{'Expiration Date'},
                            'description' => $item->returnData->{'Description'},
                            'type' => $item->returnData->Format,
                            'url' => $item->returnData->{"Production URL"}                      );
                        
                        if(isset($item->returnData->{'Thumbnail'})) {
                            $itemArr['thumbnail'] = $item->returnData->{'Thumbnail'};
                        }

                        if(isset($item->returnData->{'Gate'})) {
                            $itemArr['gate'] = $item->returnData->{'Gate'};
                        }

                        if(isset($item->returnData->Title)) {
                            $itemArr["title"]["en-us"] = $item->returnData->Title;
                        }
                        else {
                            $itemArr["title"]["en-us"] = "NO TITLE";
                        }
                        //var_dump($itemArr);
                        $resourceObj = new Resource($itemArr);
                        $resourceObj->id = $id;
                        $resourcesArr[$item->assetId] = $resourceObj;
                    }
                    else {
                        if(!empty($id) && !isset($item['hidden'])) {
                            $resourceObj = new Resource($item);
                            $resourceObj->id = $id;
                            $resourcesArr[$id] = $resourceObj;
                        }
                    }
                }
                // $resources = collect($resourcesArr)->sortBy('title', SORT_NATURAL|SORT_FLAG_CASE)->groupBy('type')->collapse();
                $resources = collect($resourcesArr)->sortBy('title', SORT_NATURAL|SORT_FLAG_CASE)->groupBy('type')->toArray(); // returns a collection (array) of Resources objects keyed by Type
                if(isset($resources['Webcast'])) $resources['Webinar'] = $resources['Webcast']; unset($resources['Webcast']); // docrep has Webinar as Webcast
                    ksort($resources);


                if ( empty($query) ) {
                    apcu_store('resources_'.Lang::currentLang(), $resources, getenv('CACHE_LENGTH'));
                }
            }
        }

        return $resources;
    }

    /*
     * Pulls a resource by id either from the cache, or from the Docrep api. Returns a single Resource object.
     *
     * @param String $resource_id
     * @return Object $resource
     */
    public function getById($resource_id) {

        $resource = [];
        $resourceArr = [];
        $cache_key = 'resource_'.Lang::currentLang() . '_' . $resource_id;

        if(!\App::environment('test','dev') && empty($_GET['clear_view_cache']) && apcu_exists($cache_key)) {
            $resource = apcu_fetch($cache_key);
        } else {
            $query = array("unique_id" => array($resource_id));

            // if on a lang path pull the translated resource, if none docrep returns the english version by default
            $current_lang = Lang::pathLang();
            if($current_lang !== false) $query["language"] = $current_lang;

            if(env('DOCSOURCE') == "docrep") {
                // use docrep
                $resourceArr = Docrep::sendRequest($query);
            }
            else {
                $MBquery = array(
                    'pageNum' => 1,
                    'pageSize' => 1000,
                    'returnItems' => array("Title","Publish Date","Expiration Date","Format","Audience","Thumbnail","Gate","Description","Production URL","Language","Part Number"),
                    'searchItems' => array("Unique ID" => array($resource_id)),
                    'sortOn' => Request::input('sortOn', 'Title'),
                    'sortAscending' => Request::input('sortAscending', "true")
                );

                $response = MediaBin::searchMetadata($MBquery);
                
                if(!empty($response)) {
                    
                    foreach($response as $id => $item) {
                        $itemArr = array(
                            'id' => $item->assetId,
                            'publish_date' => $item->returnData->{'Publish Date'},
                            'expiration_date' => $item->returnData->{'Expiration Date'},
                            'description' => $item->returnData->{'Description'},
                            'type' => $item->returnData->Format,
                            'url' => $item->returnData->{"Production URL"}                      );
                        
                        if(isset($item->returnData->{'Thumbnail'})) {
                            $itemArr['thumbnail'] = $item->returnData->{'Thumbnail'};
                        }

                        if(isset($item->returnData->{'Gate'})) {
                            $itemArr['gate'] = $item->returnData->{'Gate'};
                        }

                        if(isset($item->returnData->Title)) {
                            $itemArr["title"]["en-us"] = $item->returnData->Title;
                        }
                        else {
                            $itemArr["title"]["en-us"] = "NO TITLE";
                        }
                        $resourceArr[$item->assetId] = $itemArr;
                    }
    
                }
            }

            if(!empty($resourceArr) && isset($resourceArr[$resource_id])) {
                $resource = new Resource($resourceArr[$resource_id]);
                $resource->id = $resource_id;

                apcu_store($cache_key, $resource, getenv('CACHE_LENGTH'));
            }
            else {
                $resource['expired_id'] = $resource_id;
            }
        }

        return $resource;
    }

    /*
     * Takes a piped string of resource ids and creates a numerically indexed array of Resource objects from the Docrep api.
     *
     * @param Array $resource_ids
     * @return Array $resources
     */
    public function getByIds($resource_ids) {

        $resources = [];
        $resourceArr = [];

        if(!empty($resource_ids)) {
            $query = array("unique_id" => explode('|',$resource_ids));
            //dd($query);
            // if on a lang path pull the translated resource, if none docrep returns the english version by default
            $current_lang = Lang::pathLang();
            if($current_lang !== false) $query["language"] = $current_lang;

            if(env('DOCSOURCE') == "docrep") {
                // use docrep
                $resourceArr = Docrep::sendRequest($query);
            }
            else {
                
                //var_dump($current_lang);
                
                $query = array("Unique ID" => explode('|',$resource_ids));

                if($current_lang == "" || $current_lang == "en-us") {
                    $query["Language"] = array("English (US)");
                    $query["Primary Asset"] = array(1);
                }
                else {
                    $query["Language"] = array($this->convertToMediaBinLocale($current_lang));
                }
                
                $MBquery = array(
                    'pageNum' => 1,
                    'pageSize' => 1000,
                    'returnItems' => array("Title","Publish Date","Expiration Date","Format","Audience","Thumbnail","Gate","Description","Production URL","Language","Part Number"),
                    'searchItems' => $query,
                    'sortOn' => Request::input('sortOn', 'Title'),
                    'sortAscending' => Request::input('sortAscending', "true")
                );
                
                //echo json_encode($MBquery);
                //exit;             

                $response = MediaBin::searchMetadata($MBquery);
                
                //echo sizeof($response);
                
                if(!empty($response)) {
                    
                    foreach($response as $id => $item) {
                        $itemArr = array(
                            'id' => $item->assetId,
                            'publish_date' => $item->returnData->{'Publish Date'},
                            'expiration_date' => $item->returnData->{'Expiration Date'},
                            'description' => $item->returnData->{'Description'},
                            'type' => $item->returnData->Format,
                            'url' => $item->returnData->{"Production URL"}                      );
                        
                        if(isset($item->returnData->{'Thumbnail'})) {
                            $itemArr['thumbnail'] = $item->returnData->{'Thumbnail'};
                        }

                        if(isset($item->returnData->{'Gate'})) {
                            $itemArr['gate'] = $item->returnData->{'Gate'};
                        }

                        if(isset($item->returnData->Title)) {
                            $itemArr["title"]["en-us"] = $item->returnData->Title;
                        }
                        else {
                            $itemArr["title"]["en-us"] = "NO TITLE";
                        }
                        $resourceArr[$item->assetId] = $itemArr;
                    }
    
                }
            }
            
            //echo sizeof($resourceArr);

            if(!empty($resourceArr)) {
                $resourcesArr = [];
                foreach($resourceArr as $id => $properties) {
                    //echo "<br>".$id."<br>";
                    if(!empty($id)) {
                        $resourceObj = new Resource($properties);
                        $resourceObj->id = $id;
                        $resourcesArr[] = $resourceObj;
                    }
                }
                
                //echo sizeof($resourcesArr);

                $resources = collect($resourcesArr)->sortBy('title')->toArray();
                
                //var_dump($resources);
            }
        }
        
        //var_dump($resources);

        return $resources;
    }

    private function normalizeFilter($filter) {
        $filterParts = ucwords(str_replace('-', ' ', $filter));
        $filterReplacements = array(
            "%26" => "&",
            " Comma" => ',',
            "And" => "and",
            "Dot Net" => ".Net",
            "For" => "for",
            "Acu4gl" => "Acu4GL",
            "Acubench" => "AcuBench",
            "Acucobol Gt" => "ACUCOBOL-GT",
            "Acuconnect" => "AcuConnect",
            "Acuserver" => "AcuServer",
            "Accurev" => "AccuRev",
            "Accusync" => "AccuSync",
            "Acuxdbc" => "AcuXDBC",
            "Cobol" => "COBOL",
            'It' => 'IT',
            "Corba" => "CORBA",
            "Devpartner" => "DevPartner",
            "Extend" => "extend",
            "Extra! Xtreme" => "Extra! X-treme",
            "Faq" => "FAQ",
            "Freessl For Orbacus" => "FreeSSL for Orbacus",
            "Gitcentric" => "GitCentric",
            "Instantsql" => "InstantSQL",
            "Jacorb" => "JacORB",
            "Net Express Server Express" => "Net Express/Server Express",
            "Openfusion" => "OpenFusion",
            "Qaload" => "QALoad",
            "Rm COBOL" => "RM/COBOL",
            "Rm Infoexpress" => "RM/InfoExpress",
            "Rtorb Java Edition" => "RTOrb Java Edition",
            "Rtorb Ada Edition" => "RTOrb Ada Edition",
            "Silk Webmeter" => "Silk WebMeter",
            "Ssl" => "SSL",
            "Starteam" => "StarTeam",
            "Starteam Agile" => "StarTeam Agile",
            "Tao" => "TAO",
            "Testpartner" => "TestPartner",
            "Visibroker" => "VisiBroker",
            "Wow" => "WOW");

        return array(str_replace(array_keys($filterReplacements), array_values($filterReplacements), $filterParts));
    }

}

class Resource {

    public function __construct(Array $properties=array()){
        foreach($properties as $key => $value){
            $this->{$key} = $value;
        }
        $this->isExpired();
        $this->setTitle();
        $this->setGateUrl();
        $this->setLinkText();
    }

    // set title based on current lang, defaults to english or the first lang available if current lang isn't available
    public function setTitle() {
        $keys = array_keys($this->title);

        if(isset($this->title[Lang::currentLang()])) {
            $this->title = trim($this->title[Lang::currentLang()]);
        } elseif(isset($this->title['en-us'])) {
            $this->title = trim($this->title['en-us']);
        } elseif(isset($this->title[$keys[0]])) {
            $this->title = trim($this->title[$keys[0]]);
        } else {
            $this->title = "";
        }
        if(isset($this->expired) && $this->expired){
            $this->original_title = $this->title;
            $this->title = $this->title." (This asset is no longer available)";
        }
    }


    // Because we show the english resource if the translated resource doesn't exist, we need to add the lang code to the gate page url using the path_language
    public function setGateUrl() {

        if(!empty($this->gate)) {
            // check if there's a lang code in the gate url already, if there is one, don't add one below. For example an asset may only exist in French and the gate url has /fr-fr in it already so we wouldn't add the lang code below
            if( preg_match('/^\/(\w\w\-\w\w)/', $this->gate) == false && !empty($this->path_language) && $this->path_language != Lang::getLangConfig('default') && strpos($this->gate, 'http') === false) {
                $this->gate = '/' . $this->path_language . $this->gate;
            }
        }
    }

    public function setLinkText() {
        // use the /content/{lang}/includes.html to pull in translation text
        $translate = Lang::parseTranslationFile('content::includes');

        if(isset($this->expired) && $this->expired){
            $this->link_text = "Request asset";
        }
        else{
            if(isset($this->type) && ($this->type == 'Video' || $this->type == 'Webinar')) {
                $this->link_text = isset($translate['watch_now']) ? $translate['watch_now'] : 'Watch now';
            } else {
                $this->link_text = isset($translate['view_now']) ? $translate['view_now'] : 'View now';
            }
        }
    }

    public function getImage() {
        if(isset($this->thumbnail)) {
            // if there's a thumbnail use that, else use icon
            $thumbnail = '<img class="resource-image" src="/assets/images'.$this->thumbnail.'" alt="' . $this->title . '" />';
            return $thumbnail;
        }

        switch($this->type) {
            case 'Article':
                $icon = 'article';
                break;
            case 'Audio':
                $icon = 'article';
                break;
            case 'Brief':
                $icon = 'brief';
                break;
            case 'Brochure':
                $icon = 'brochure';
                break;
            case 'Case Study':
                $icon = 'customerStory';
                break;
            case 'Data Sheet':
                $icon = 'dataSheet';
                break;
            case 'FAQ':
                $icon = 'faq';
                break;
            case 'Flyer':
                $icon = 'flyer';
                break;
            case 'Guide':
                $icon = 'guide';
                break;
            case 'Planning Document':
                $icon = 'plans';
                break;
            case 'Success Story':
                $icon = 'customerStory';
                break;
            case 'Video':
                $icon = 'webinar';
                break;
            case 'Webinar':
                $icon = 'webinar';
                break;
            case 'Webcast':
                $icon = 'webinar';
                break;
            case 'White Paper':
                $icon = 'whitepaper';
                break;
            default:
                $icon = 'pdf';
                break;
        }
        $icon = '<span class="resource-icon icon-'.$icon.'"></span>';
        return $icon;
    }

    public function getUrl() {
        $originUrl = Request::fullUrl();
        if(!empty($this->gate)) {
            return $this->gate;
        } else {
            return $this->url;
        }
    }

    public function isExpired() {
        if(env('DOCSOURCE') == "mediabin") {
            return false;
        }
        $this->expired = false;
        if($this->expiration_date < date("Y-m-d")){
            $this->expired = true;
        }
        return  $this->expired;
    }
}