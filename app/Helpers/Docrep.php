<?php

namespace App\Helpers;

/**
 * REST client for the Docrep API.
 */
class Docrep
{
    public static function sendRequest($data = array("unique_id" => array())) {

        $url = env('DOCREP_API');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        // set headers
        $headers = array('Content-type: application/json');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // return transfer as string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , env('DOCREP_API_TIMEOUT'));
        curl_setopt($ch, CURLOPT_TIMEOUT, env('DOCREP_API_TIMEOUT')); //timeout in seconds

        $response = curl_exec($ch);

        // store the response info including the HTTP status
        // 400 and 500 status codes indicate an error
        $responseInfo = curl_getinfo($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if ($httpCode > 400)
        {
            $response = '';
        }

        return json_decode($response, true);

    }
}
