<?php

	namespace App\Helpers;

	class GeoPhone {
		static function getphone() {
			/**
			 * Load SUSE phone numbers from XML file according to session info
			 */
			$geo = session()->get("country_code");
			if(!$geo) {
				$geo = "US";
			}

			$filepath = base_path();
			$file = $filepath . "/suse/public_html/data/company/geophone.xml";
			$data = simplexml_load_file($file);
			
			if ($data) {
				$global_sales = $data->GLOBAL->sales->local;
				$global_support = $data->GLOBAL->support->local;
				$global_training = $data->GLOBAL->training->local;
				global $salesPhone;
				global $supportPhone;
				$salesPhone = "";
				$supportPhone = "";

				function getPhoneNumberForNode($region, $geo, $data, $salesPhone, $supportPhone) {
					global $salesPhone;
					global $supportPhone;
					$node = $data->GLOBAL->{$region}->{$geo};
					$parent = $data->GLOBAL->{$region};
					$global = $data->GLOBAL;
					if(isset($node->sales->local)) {
						$salesPhone = $node->sales->local;
					}
					else if(isset($parent->sales->local)){
						$salesPhone = $parent->sales->local;
					}
					else {
						$salesPhone = $global->sales->local;
					}
					if(isset($node->support->local)) {
						$supportPhone = $node->support->local;
					}
					else if(isset($parent->support->local)){
						$supportPhone = $parent->support->local;
					}
					else {
						$supportPhone = $global->support->local;
					}
				}

				if(isset($data->GLOBAL->ASPREGN->{$geo})) {
					getPhoneNumberForNode("ASPREGN", $geo, $data, $salesPhone, $supportPhone);
				}
				else if(isset($data->GLOBAL->EURREGN->{$geo})) {
					getPhoneNumberForNode("EURREGN", $geo, $data, $salesPhone, $supportPhone);
				}
				else if(isset($data->GLOBAL->NAMREGN->{$geo})) {
					getPhoneNumberForNode("NAMREGN", $geo, $data, $salesPhone, $supportPhone);
				}
				else if(isset($data->GLOBAL->LATAMER->{$geo})) {
					getPhoneNumberForNode("LATAMER", $geo, $data, $salesPhone, $supportPhone);
				}

				// In case no phone numbers are found
				if($salesPhone == "") {
					$salesPhone = $global_sales;
				}
				if($supportPhone == "") {
					$supportPhone = $global_support;
				}


				$theNumbers = ["sales" => $salesPhone, "support" => $supportPhone];
				return $theNumbers;			
			}
		}
	}
?>
