<?php

namespace App\Helpers;

class MediaBin {

	/* getVersion()
	 * Get the MediaBin version. This can be used to verify that both MediaBin and the
	 * Gateway are operational as well as to check the version.
	 * 
	 * @return
	 * 	{
	 * 	 "MBDataModel": {
	 * 	 "code": 200,
	 * 	 "errorCode": "SUCCESS",
	 * 	 "message": "Success",
	 * 	 "version": "8.3.0.111235",
	 * 	 "currentUser": null,
	 * 	 "connectedUsers": null,
	 * 	 "domains": null,
	 * 	 "metadataItemsReturned": null,
	 * 	 "authCode": null,
	 * 	 "results": [],
	 * 	 "pagerID": null,
	 * 	 "pageNumber": 0,
	 * 	 "pageSize": 0,
	 * 	 "objectCount": 0,
	 * 	 "totalPages": 0,
	 * 	 "totalObjects": 0,
	 * 	 "loggedOff": false
	 * 	 }
	 * 	}
	 */
	public static function getVersion() {
		return static::fetch(env('MEDIABIN').'/mbgateway/1_0/getVersion.json',[]);
	}
	
	/* clearCache()
	 * Clears the metadata search cache. When metadata searches are done, the search info is
	 * cached to improve performance on subsequent searches. This function clears that cache.
	 * 
	 * @return
	 * {
	 *  "MBDataModel": {
	 *  "code": 200,
	 *  "errorCode": "SUCCESS",
	 *  "message": "Success",
	 *  "version": "8.3.0.111235",
	 *  "currentUser": null,
	 *  "connectedUsers": null,
	 *  "domains": null,
	 *  "metadataItemsReturned": null,
	 *  "authCode": null,
	 *  "results": [],
	 *  "pagerID": null,
	 *  "pageNumber": 0,
	 *  "pageSize": 0,
	 *  "objectCount": 0,
	 *  "totalPages": 0,
	 *  "totalObjects": 0,
	 *  "loggedOff": false
	 *  }
	 * }
	 */
	public static function clearCache() {
		return static::fetch(env('MEDIABIN').'/mbgateway/1_0/clearCache.json',[]);
	}

	/* getAvailableFilters()
	 * Retrieve the allowed values for the metadata names specified in the call.
	 * 
	 * @return
	 * 	{
		    "MBAllowedValuesDataModel": {
		        "code": 200,
		        "errorCode": "SUCCESS",
		        "message": "Success",
		        "allowedValues": {
		            "Business Unit": [
		                "Attachmate",
		                "Borland",
		                "Micro Focus",
		                "NetIQ",
		                "Novell",
		                "SUSE"
		            ],
		            "Asset Type": [
		                "3D",
		                "Adobe Illustrator",
		                "Adobe InDesign",
		                "BMP",
		                "CAD",
		                "Camera Raw",
		                "Corel Draw",
		                "DNG",
		                "Flash",
		                "Flash Project",
		                "Flash Video",
		                "GIF",
		                "ICC Profile",
		                "JPEG",
		                "Macromedia Freehand",
		                "MS Excel Document",
		                "MS Office Document",
		                "MS PowerPoint Document",
		                "MS Visio Document",
		                "MS Word Document",
		                "Multimedia file",
		                "Non-Image",
		                "PDF",
		                "PhotoCD",
		                "Photoshop",
		                "Photoshop EPS",
		                "Placeholder",
		                "Plain Text",
		                "PNG",
		                "QuickTime",
		                "STiNG",
		                "SVG",
		                "TARGA",
		                "Text",
		                "TIFF",
		                "Vector EPS",
		                "Virage VDF",
		                "XML"
		            ]
		        }
		    }
		}
	 */
	public static function getAvailableFilters() {
		$query = array("metadataNames" => ["Product","Format","Solution","Business Unit","Asset Type"]);
		return json_decode(static::fetch(env('MEDIABIN').'/mbgateway/1_0/mbGetAllowedMetadataValues.json',$query));
	}

	/* searchMetadata($query)
	 * Search for and retrieve metadata for MediaBin assets. The items searched
	 * for and the items returned can be specified.
	 * 
	 * @param $query 
	 * {
	 *  "pageNum":"1",
	 *  "pageSize":"4",
	 *  "returnItems":[
	 *  "Product","Name","Title","Description","Production URL"
	 *  ],
	 *  "searchItems":{
	 *  "Product":"GroupWise",
	 *  "Format":"Guide"
	 *  }
	 * }	
	 * 
	 * @return 	
	 * {
	 *  "MBDataModel": {
	 *  "code": 200,
	 *  "errorCode": "SUCCESS",
	 *  "message": "Success",
	 *  "version": "",
	 *  "currentUser": null,
	 *  "connectedUsers": null,
	 *  "domains": null,
	 *  "metadataItemsReturned": [
	 *  "Product",
	 *  "Name",
	 *  "Title",
	 *  "Description",
	 *  "Production URL"
	 *  ],
	 *  "authCode": null,
	 *  "results": [
	 *  {
	 *  "returnData": {
	 *  "Name": "groupwise_2014_feature_enhancement_summary.pdf",
	 *  "Description": "GroupWise 2014 Feature Enhancement Summary is the latest major
	 * upgrade to GroupWise. What follows is a summary of the major enhancements available in
	 * Windermere.",
	 *  "Product": "GroupWise",
	 *  "Title": "GroupWise 2014 Feature Enhancement Summary Guide",
	 *  "Production URL":
	 * "https://www.microfocus.com/media/guide/groupwise_2014_feature_enhancement_summary.pdf"
	 *  }
	 *  },
	 *  {
	 *  "returnData": {
	 *  "Name": "groupwise_2014_feature_enhancement_summary_ee.pdf",
	 *  "Description": "GroupWise 2014 Feature Enhancement Summary is the latest major
	 * upgrade to GroupWise. What follows is a summary of the major enhancements available in
	 * Windermere.",
	 *  "Product": "GroupWise",
	 *  "Title": "GroupWise 2014 Feature Enhancement Summary Guide",
	 *  "Production URL":
	 * "https://www.microfocus.com/media/guide/groupwise_2014_feature_enhancement_summary_ee.pdf
	 * "
	 *  }
	 *  },
	 *  {
	 *  "returnData": {
	 *  "Name": "groupwise_2014_feature_enhancement_summary_du.pdf",
	 *  "Description": "Het overzicht van verbeteringen in functionaliteit in GroupWise 2014 is
	 * de nieuwste grote upgrade van GroupWise. Dit document bevat een beknopt overzicht van de
	 * belangrijkste verbeteringen in functionaliteit in Windermere.",
	 *  "Product": "GroupWise",
	 *  "Title": "Handleiding met de verbeteringen in functionaliteit in GroupWise 2014",
	 *  "Production URL":
	 * "https://www.microfocus.com/media/guide/groupwise_2014_feature_enhancement_summary_du.pdf
	 * "
	 *  }
	 *  },
	 *  {
	 *  "returnData": {
	 *  "Name": "groupwise_2014_feature_enhancement_summary_de.pdf",
	 *  "Description": "Der Überblick über die Funktionsverbesserungen von GroupWise 2014
	 * ist das letzte größere Upgrade von GroupWise. Im Folgenden geben wir Ihnen einen Überblick über
	 * die wichtigsten Verbesserungen, die über Windermere verfügbar sind.",
	 *  "Product": "GroupWise",
	 *  "Title": "GroupWise 2014: Zusammenfassung zu Funktionsverbesserungen –
	 * Leitfaden",
	 *  "Production URL":
	 * "https://www.microfocus.com/media/guide/groupwise_2014_feature_enhancement_summary_de.pdf
	 * "
	 *  }
	 *  }
	 *  ],
	 *  "pagerID": "691b9828-9a5c-41ed-b2a4-3c459c639dd5",
	 *  "pageNumber": 1,
	 *  "pageSize": 4,
	 *  "objectCount": 4,
	 *  "totalPages": 2,
	 *  "totalObjects": 8,
	 *  "loggedOff": false
	 *  }
	 * }
	 */
    public static function searchMetadata($query, $includePartner = false) {
        $fetch = null;
        // Force pull of only partner and public items
        if(isset($query["searchItems"]["Audience"])) {
            // remove employee flag
            $removeAudiences = array("Employee");
            $query["searchItems"]["Audience"] = array_diff($query["searchItems"]["Audience"], $removeAudiences);
        }
        else if($includePartner) {
            $query["searchItems"]["Audience"] = array("Partner","Public");
        }
        else {
            $query["searchItems"]["Audience"] = array("Public");
        }
        //var_dump(env('MEDIABIN').'/mbgateway/1_0/mbSearchByMetadata.json');
        //echo "<br><br>";
        //exit;
        
        // pull from media bin
        //var_dump($query);
        //echo json_encode($query);
        //echo "<br><br>";
        //exit;
        
        $fetch = static::fetch(env('MEDIABIN').'/mbgateway/1_0/mbSearchByMetadata.json',$query);

        $results = json_decode($fetch);

        //var_dump($results);
        //echo "<br><br>";

        if(empty($results)) {
            dd($fetch);
        }

        if(isset($results->MBDataModel)) {
	        $results = $results->MBDataModel;
        }

        if(isset($results->results)) {
	        $results = $results->results;
        }

        return $results;
    }

    public static function searchMetadataCSV($query) {
	    $results = searchMetadata($query);

        $output = array ();
        // prevent errors, make sure every requested variable has been set:
        foreach ( $results->results as $result ) {
            if (property_exists($result->title, "en-us")) {
                array_push($output, array($result->{'assetId'}, utf8_encode($result->title->{"en-us"}), $result->{'publish_date'}, $result->type, "employee"));
            } else {
                //dd($result->title); // offending title was japanese-only, decided to exclude.  Adam Gilroy 12/4/2018
            }
        }

        ob_start();
        $df = fopen("php://output", 'w');
        fputs( $df, "\xEF\xBB\xBF" ); // UTF-8 BOM !!!!!
        fputcsv($df, array('Document Title','Date','Format','Audience'));
        foreach ($output as $row) {
            fputcsv($df, $row);
        }
        fclose($df);

        return ob_get_clean();
    }

	public static function searchContainer($query) {
		//var_dump($query);
        if(env('DOCSOURCE') == "docrep") {
            // pull from docrep
            $fetch = static::fetch(env('DOCREP_API_SMRL'),$query);
			$results = json_decode($fetch);
            //var_dump($fetch);
            //exit;
			return $results;
        }
        else {
            // pull from media bin
			$fetch = static::fetch(env('MEDIABIN').'/mbgateway/1_0/mbSearchContainerByAssetId.json',$query);
			$results = json_decode($fetch);
			foreach ( $results->MBAssocDataModel->results as $result ) {
				foreach ( $query['returnItems'] as $expected ) {
					if ( empty($result->associatedAssets->{$expected}) ) {
						$result->associatedAssets->{$expected} = '';
					}
				}
			}
			return $results->MBAssocDataModel;
        }
	}

    public static function getTokenizedUrl($targetUrl) {
        $dlt_username = "emarketing";
        $dlt_password = "n0v3ll";

        $targetUrl = str_replace("https://intra.microfocus.net/media/inc/playcdnvideo.php?url=","",$targetUrl);
        $targetUrl = str_replace("https://www.microfocus.com/partner/media/inc/playcdnvideo.php?url=","",$targetUrl);
        $targetUrl = str_replace("https://cdn.microfocus.com","",$targetUrl);
        $tokenizerUrl = "http://appserviceinternal.provo.novell.com/dltoken/generate/filepath/20/" . $targetUrl . ".json";

        $ch = curl_init( $tokenizerUrl );
        curl_setopt( $ch, CURLOPT_POST, 0);
        curl_setopt( $ch, CURLOPT_USERPWD, "$dlt_username:$dlt_password");
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec( $ch );
        $json_response = json_decode($response);

        return $targetUrl . "?" . $json_response->pathDataModel->token;
    }
	
	private static function fetch($url,$query) {
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
		if ( empty($query) ) {
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
				'Content-Type: application/json'
			));
		} else {
			$query = json_encode($query);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
				'Content-Type: application/json',                                                                                
				'Content-Length: ' . strlen($query)
			));
		}

		$result_data = curl_exec($ch); 
		if ($result_data === false) {
			//dd($ch);
			return false;
		} 

		$httpCode = curl_getinfo($ch); 
		curl_close($ch); 
		
		return $result_data;
	}
	

}
