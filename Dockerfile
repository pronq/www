FROM ubuntu:16.04
#hoping to change ubuntu to SLES
MAINTAINER Tyler Merrell <tyler.merrell@microfocus.com>

LABEL 	Discription="This is going to be the new www.microfocus.com local enviornment."\
		License="Apache License 2.0" \
		Usage="docker run -d -p [HOST WWW PORT NUMBER (usually 80)]:80 -p [HOST DB PORT NUMBER (usually 443)]:443 -v [HOST WWW DOCUMENT ROOT]:/var/www/html -v [HOST DB DOCUMENT ROOT]:/var/lib/mysql fauria/lamp" \
		Version="1.0"

RUN apt-get update --fix-missing
RUN apt-get upgrade -y

RUN apt-get install php -y
RUN apt-get install php-apcu -y
RUN apt-get install php-curl -y
RUN apt-get install php-mcrypt -y
RUN apt-get install php-mysql -y
RUN apt-get install php7.0-xml -y

RUN apt-get install apache2 libapache2-mod-php7.0 -y

RUN apt-get install git nodejs npm composer nano tree vim curl ftp -y

VOLUME /var/www/laravel

EXPOSE 80
EXPOSE 443