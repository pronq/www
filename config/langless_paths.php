<?php

/* 
 * paths that should never need a language code added
 * ie: selfreg
 * bug: 1014061
 */

return [
	'paths' => array(
		'selfreg' => 'strpos',
		'/(^\/[a-z]{2}-[a-z]{2})?\/ondemand/' => 'regex',
		// 'get-all-countries', // suse only
		'e.aspx' => 'strpos',
		'/(^\/[a-z]{2}-[a-z]{2})?\/lifecycle/' => 'regex',
		'/(^\/[a-z]{2}-[a-z]{2})?\/training/' => 'regex',
	),
	'hosts' => array(
		'download',
		'training',
		'forums',
		'blog',
	)
];
