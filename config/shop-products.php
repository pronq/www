<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [
	'chameleon' => array(
		'green_1' => array(
			'product_id' => '258826700',
			'emea_product_id' => '270601900',
		),
	),
	'desktop' => array(
		'basic_1year' => array(
			'product_id' => '259213400',
			'emea_product_id' => '270604400',
		),
		'basic_3year' => array(
			'product_id' => '260558500',
			'emea_product_id' => '270606600',
		),
		'standard_1year' => array(
			'product_id' => '259221000',
			'emea_product_id' => '270604600',
		),
		'standard_3year' => array(
			'product_id' => '260558700',
			'emea_product_id' => '270606700',
		),
		'priority_1year' => array(
			'product_id' => '259220900',
			'emea_product_id' => '270604500',
		),
		'priority_3year' => array(
			'product_id' => '260558900',
			'emea_product_id' => '270606800',
		),
	),
	'lifecycle_management' => array(
		'lifecycle_2socket_2virt_1year' => array(
			'product_id' => '306772000',
			'emea_product_id' => '307358700',
		),
		'lifecycle_2socket_2virt_3year' => array(
			'product_id' => '306775200',
			'emea_product_id' => '309735200',
		),
		'monitoring_2socket_2virt_1year' => array(
			'product_id' => '306892400',
			'emea_product_id' => '309739900',
		),
		'monitoring_2socket_2virt_3year' => array(
			'product_id' => '310165100',
			'emea_product_id' => '306892600',
		),
		'lifecycle_2socket_unlimited_1year' => array(
			'product_id' => '306892700',
			'emea_product_id' => '310172900',
		),
		'lifecycle_2socket_unlimited_3year' => array(
			'product_id' => '306892800',
			'emea_product_id' => '310173000',
		),
		'monitoring_2socket_unlimited_1year' => array(
			'product_id' => '306892900',
			'emea_product_id' => '310173100',
		),
		'monitoring_2socket_unlimited_3year' => array(
			'product_id' => '306893000',
			'emea_product_id' => '310173200',
		),
	),
	'ondemand_training' => array(
		'1user_cla_1year' => array(
			'product_id' => '277594300',
			'emea_product_id' => '277597400',
		),
		'1user_clp_1year' => array(
			'product_id' => '277595100',
			'emea_product_id' => '277598200',
		),
		'1user_cle_1year' => array(
			'product_id' => '277594700',
			'emea_product_id' => '277597800',
		),
		'1user_suse_library_1year' => array(
			'product_id' => '277593600',
			'emea_product_id' => '277596700',
		),
		'1user_full_library_1year' => array(
			'product_id' => '277595500',
			'emea_product_id' => '277598600',
		),
		'5user_cla_1year' => array(
			'product_id' => '277594400',
			'emea_product_id' => '277597500',
		),
		'5user_clp_1year' => array(
			'product_id' => '277595200',
			'emea_product_id' => '277598300',
		),
		'5user_cle_1year' => array(
			'product_id' => '277594800',
			'emea_product_id' => '277597900',
		),
		'5user_suse_library_1year' => array(
			'product_id' => '277594000',
			'emea_product_id' => '277597100',
		),
		'5user_full_library_1year' => array(
			'product_id' => '277595600',
			'emea_product_id' => '277598700',
		),
		'10user_cla_1year' => array(
			'product_id' => '277594500',
			'emea_product_id' => '277597600',
		),
		'10user_clp_1year' => array(
			'product_id' => '277595300',
			'emea_product_id' => '277598400',
		),
		'10user_cle_1year' => array(
			'product_id' => '277594900',
			'emea_product_id' => '277598000',
		),
		'10user_suse_library_1year' => array(
			'product_id' => '277594100',
			'emea_product_id' => '277597200',
		),
		'10user_full_library_1year' => array(
			'product_id' => '277595700',
			'emea_product_id' => '277598800',
		),
		'25user_cla_1year' => array(
			'product_id' => '277594600',
			'emea_product_id' => '277597700',
		),
		'25user_clp_1year' => array(
			'product_id' => '277595400',
			'emea_product_id' => '277598500',
		),
		'25user_cle_1year' => array(
			'product_id' => '277595000',
			'emea_product_id' => '277598100',
		),
		'25user_suse_library_1year' => array(
			'product_id' => '277594200',
			'emea_product_id' => '277597300',
		),
		'25user_full_library_1year' => array(
			'product_id' => '277595800',
			'emea_product_id' => '277598900',
		),
		'suse_linux12_1user' => array(
			'product_id' => '313414200',
			'emea_product_id' => '313421100',
		),
		'suse_linux12_5user' => array(
			'product_id' => '313415100',
			'emea_product_id' => '313421400',
		),
		'suse_linux12_10user' => array(
			'product_id' => '313415200',
			'emea_product_id' => '313421300',
		),
		'suse_linux12_25user' => array(
			'product_id' => '313415300',
			'emea_product_id' => '313421200',
		),
		'suse_linux11_1user' => array(
			'product_id' => '313414300',
			'emea_product_id' => '313420700',
		),
		'suse_linux11_5user' => array(
			'product_id' => '313414800',
			'emea_product_id' => '313421000',
		),
		'suse_linux11_10user' => array(
			'product_id' => '313415000',
			'emea_product_id' => '313420900',
		),
		'suse_linux11_25user' => array(
			'product_id' => '313414900',
			'emea_product_id' => '313420800',
		),
		'suse_linux10_1user' => array(
			'product_id' => '313414000',
			'emea_product_id' => '313420300',
		),
		'suse_linux10_5user' => array(
			'product_id' => '313414500',
			'emea_product_id' => '313420400',
		),
		'suse_linux10_10user' => array(
			'product_id' => '313414700',
			'emea_product_id' => '313420600',
		),
		'suse_linux10_25user' => array(
			'product_id' => '313414600',
			'emea_product_id' => '313420500',
		),
		'management_extensions_1user' => array(
			'product_id' => '313414100',
			'emea_product_id' => '313421500',
		),
		'management_extensions_5user' => array(
			'product_id' => '313415500',
			'emea_product_id' => '313421800',
		),
		'management_extensions_10user' => array(
			'product_id' => '313415600',
			'emea_product_id' => '313421700',
		),
		'management_extensions_25user' => array(
			'product_id' => '313415700',
			'emea_product_id' => '313421600',
		),
	),
	'sles' => array(
		'basic_1year_x86_physical_2socket' => array(
			'product_id' => '259209400',
			'emea_product_id' => '270599600',
		),
		'basic_1year_x86_physical_4socket' => array(
			'product_id' => '259211300',
			'emea_product_id' => '270603800',
		),
		'basic_1year_x86_virtual_2socket' => array(
			'product_id' => '259208600',
			'emea_product_id' => '270602100',
		),
		'basic_1year_x86_virtual_4socket' => array(
			'product_id' => '259210400',
			'emea_product_id' => '270603200',
		),
		'basic_3year_x86_physical_2socket' => array(
			'product_id' => '259209900',
			'emea_product_id' => '270603100',
		),
		'basic_3year_x86_physical_4socket' => array(
			'product_id' => '259211600',
			'emea_product_id' => '270604100',
		),
		'basic_3year_x86_virtual_2socket' => array(
			'product_id' => '259209300',
			'emea_product_id' => '270602600',
		),
		'basic_3year_x86_virtual_4socket' => array(
			'product_id' => '259211000',
			'emea_product_id' => '270603500',
		),
		'basic_5year_x86_physical_2socket' => array(
			'product_id' => '259735000',
			'emea_product_id' => '270605400',
		),
		'basic_5year_x86_physical_4socket' => array(
			'product_id' => '259735600',
			'emea_product_id' => '270606000',
		),
		'basic_5year_x86_virtual_2socket' => array(
			'product_id' => '259735100',
			'emea_product_id' => '270605500',
		),
		'basic_5year_x86_virtual_4socket' => array(
			'product_id' => '259735700',
			'emea_product_id' => '270606100',
		),
		'standard_1year_x86_physical_2socket' => array(
			'product_id' => '259209800',
			'emea_product_id' => '270603000',
		),
		'standard_1year_x86_physical_4socket' => array(
			'product_id' => '259211400',
			'emea_product_id' => '270603900',
		),
		'standard_1year_x86_virtual_2socket' => array(
			'product_id' => '259208900',
			'emea_product_id' => '270602300',
		),
		'standard_1year_x86_virtual_4socket' => array(
			'product_id' => '259210500',
			'emea_product_id' => '270603300',
		),
		'standard_3year_x86_physical_2socket' => array(
			'product_id' => '259209600',
			'emea_product_id' => '270602800',
		),
		'standard_3year_x86_physical_4socket' => array(
			'product_id' => '259211700',
			'emea_product_id' => '270604200',
		),
		'standard_3year_x86_virtual_2socket' => array(
			'product_id' => '259208700',
			'emea_product_id' => '270602200',
		),
		'standard_3year_x86_virtual_4socket' => array(
			'product_id' => '259211100',
			'emea_product_id' => '270603600',
		),
		'standard_5year_x86_physical_2socket' => array(
			'product_id' => '259735400',
			'emea_product_id' => '270605800',
		),
		'standard_5year_x86_physical_4socket' => array(
			'product_id' => '259736000',
			'emea_product_id' => '270606400',
		),
		'standard_5year_x86_virtual_2socket' => array(
			'product_id' => '259735500',
			'emea_product_id' => '270605900',
		),
		'standard_5year_x86_virtual_4socket' => array(
			'product_id' => '259736100',
			'emea_product_id' => '270606500',
		),
		'standard_power_1year' => array(
			'product_id' => '258826200',
			'emea_product_id' => '270601600',
		),
		'standard_power_3year' => array(
			'product_id' => '260665400',
			'emea_product_id' => '270608300',
		),
		'priority_1year_x86_physical_2socket' => array(
			'product_id' => '259209500',
			'emea_product_id' => '270602700',
		),
		'priority_1year_x86_physical_4socket' => array(
			'product_id' => '259211500',
			'emea_product_id' => '270604000',
		),
		'priority_1year_x86_virtual_2socket' => array(
			'product_id' => '259209000',
			'emea_product_id' => '270602400',
		),
		'priority_1year_x86_virtual_4socket' => array(
			'product_id' => '259210600',
			'emea_product_id' => '270603400',
		),
		'priority_3year_x86_physical_2socket' => array(
			'product_id' => '259209700',
			'emea_product_id' => '270602900',
		),
		'priority_3year_x86_physical_4socket' => array(
			'product_id' => '259211800',
			'emea_product_id' => '270604300',
		),
		'priority_3year_x86_virtual_2socket' => array(
			'product_id' => '259209100',
			'emea_product_id' => '270602500',
		),
		'priority_3year_x86_virtual_4socket' => array(
			'product_id' => '259211200',
			'emea_product_id' => '270603700',
		),
		'priority_5year_x86_physical_2socket' => array(
			'product_id' => '259735200',
			'emea_product_id' => '270605600',
		),
		'priority_5year_x86_physical_4socket' => array(
			'product_id' => '259735800',
			'emea_product_id' => '270606200',
		),
		'priority_5year_x86_virtual_2socket' => array(
			'product_id' => '259735300',
			'emea_product_id' => '270605700',
		),
		'priority_5year_x86_virtual_4socket' => array(
			'product_id' => '259735900',
			'emea_product_id' => '270606300',
		),
		'priority_power_1year' => array(
			'product_id' => '258826300',
			'emea_product_id' => '270601700',
		),
		'priority_power_3year' => array(
			'product_id' => '260665500',
			'emea_product_id' => '270608400',
		),
	),
	'power' => array(
//		'basic_power_1year' => array(
//			'product_id' => '258826400',
//			'emea_product_id' => '270601800',
//		),
//		'basic_power_3year' => array(
//			'product_id' => '260665300',
//			'emea_product_id' => '270608200',
//		),
		'standard_power_1year' => array(
			'product_id' => '258826200',
			'emea_product_id' => '270601600',
		),
		'standard_power_3year' => array(
			'product_id' => '260665400',
			'emea_product_id' => '270608300',
		),
		'priority_power_1year' => array(
			'product_id' => '258826300',
			'emea_product_id' => '270601700',
		),
		'priority_power_3year' => array(
			'product_id' => '260665500',
			'emea_product_id' => '270608400',
		),
		'virtual_standard_power_1year' => array(
			'product_id' => '327094200',
			'emea_product_id' => '327039700',
		),
		'virtual_priority_power_1year' => array(
			'product_id' => '327094500',
			'emea_product_id' => '327043800',
		),
		'virtual_standard_power_3year' => array(
			'product_id' => '327094700',
			'emea_product_id' => '327094000',
		),
		'virtual_priority_power_3year' => array(
			'product_id' => '327095500',
			'emea_product_id' => '327094100',
		),
	),
	'sles_high_availability' => array(
		'ha_2sockets_1year' => array(
			'product_id' => '259226400',
			'emea_product_id' => '270604700',
		),
		'ha_2sockets_3year' => array(
			'product_id' => '259226500',
			'emea_product_id' => '270604800',
		),
		'ha_2sockets_5year' => array(
			'product_id' => '260620000',
			'emea_product_id' => '270607100',
		),
		'ha_4sockets_1year' => array(
			'product_id' => '259226600',
			'emea_product_id' => '270604900',
		),
		'ha_4sockets_3year' => array(
			'product_id' => '259226700',
			'emea_product_id' => '270605000',
		),
		'ha_4sockets_5year' => array(
			'product_id' => '260620100',
			'emea_product_id' => '270607200',
		),
		'ha_8sockets_1year' => array(
			'product_id' => '260625700',
			'emea_product_id' => '270607800',
		),
		'ha_8sockets_3year' => array(
			'product_id' => '260625600',
			'emea_product_id' => '270607700',
		),
		'ha_8sockets_5year' => array(
			'product_id' => '260620200',
			'emea_product_id' => '270607300',
		),
		'geo_2sockets_1year' => array(
			'product_id' => '259734900',
			'emea_product_id' => '270605300',
		),
		'geo_2sockets_3year' => array(
			'product_id' => '258812800',
			'emea_product_id' => '270601300',
		),
		'geo_2sockets_5year' => array(
			'product_id' => '260619800',
			'emea_product_id' => '270606900',
		),
		'geo_4sockets_1year' => array(
			'product_id' => '258812700',
			'emea_product_id' => '270601200',
		),
		'geo_4sockets_3year' => array(
			'product_id' => '258812600',
			'emea_product_id' => '270601100',
		),
		'geo_4sockets_5year' => array(
			'product_id' => '260625300',
			'emea_product_id' => '270607400',
		),
		'geo_8sockets_1year' => array(
			'product_id' => '260625500',
			'emea_product_id' => '270607600',
		),
		'geo_8sockets_3year' => array(
			'product_id' => '260625400',
			'emea_product_id' => '270607500',
		),
		'geo_8sockets_5year' => array(
			'product_id' => '260619900',
			'emea_product_id' => '270607000',
		),
	),
	'sles_pointofservice' => array(
		'client_basic_1year' => array(
			'local_id' => '874-005325',
		),
		'client_basic_3year' => array(
			'local_id' => '874-005326',
		),
		'client_basic_5year' => array(
			'local_id' => '874-006368',
		),
		'client_standard_1year' => array(
			'local_id' => '877-003415',
		),
		'client_standard_3year' => array(
			'local_id' => '877-003416',
		),
		'client_standard_5year' => array(
			'local_id' => '874-006371',
		),
		'client_priority_1year' => array(
			'local_id' => '877-003417',
		),
		'client_priority_3year' => array(
			'local_id' => '877-003418',
		),
		'client_priority_5year' => array(
			'local_id' => '874-006374',
		),
		'branch_basic_1year' => array(
			'local_id' => '874-005327',
		),
		'branch_basic_3year' => array(
			'local_id' => '874-005328',
		),
		'branch_basic_5year' => array(
			'local_id' => '874-006369',
		),
		'branch_standard_1year' => array(
			'local_id' => '877-003419',
		),
		'branch_standard_3year' => array(
			'local_id' => '877-003420',
		),
		'branch_standard_5year' => array(
			'local_id' => '874-006372',
		),
		'branch_priority_1year' => array(
			'local_id' => '877-003421',
		),
		'branch_priority_3year' => array(
			'local_id' => '877-003422',
		),
		'branch_priority_5year' => array(
			'local_id' => '874-006375',
		),
		'admin_basic_1year' => array(
			'local_id' => '874-005329',
		),
		'admin_basic_3year' => array(
			'local_id' => '874-005330',
		),
		'admin_basic_5year' => array(
			'local_id' => '874-006370',
		),
		'admin_standard_1year' => array(
			'local_id' => '877-003423',
		),
		'admin_standard_3year' => array(
			'local_id' => '877-003424',
		),
		'admin_standard_5year' => array(
			'local_id' => '874-006373',
		),
		'admin_priority_1year' => array(
			'local_id' => '877-003425',
		),
		'admin_priority_3year' => array(
			'local_id' => '877-003426',
		),
		'admin_priority_5year' => array(
			'local_id' => '874-006376',
		),
	),
	'sles_realtime' => array(
		'1year' => array(
			'product_id' => '258826700',
			'emea_product_id' => '270601900',
		),
		'3year' => array(
			'product_id' => '258826800',
			'emea_product_id' => '270602000',
		),
	),
	'sles_sap' => array(
//		'physical_slepow_1year' => array(
//			'local_id' => '877-003425',
//		),
//		'physical_slepow_3year' => array(
//			'local_id' => '877-003426',
//		),
//		'physical_slepow_5year' => array(
//			'local_id' => '874-006376',
//		),
		'physical_2socket_1year' => array(
			'local_id' => '874-006905',
		),
		'physical_2socket_3year' => array(
			'local_id' => '874-006907',
		),
		'physical_2socket_5year' => array(
			'local_id' => '874-006909',
		),
		'power_2socket_3year' => array(
			'local_id' => '874-007131',
		),
//		'physical_4socket_1year' => array(
//			'local_id' => '874-006263',
//		),
//		'physical_4socket_3year' => array(
//			'local_id' => '874-006278',
//		),
//		'physical_4socket_5year' => array(
//			'local_id' => '874-006323',
//		),
//		'physical_8socket_1year' => array(
//			'local_id' => '874-006268',
//		),
//		'physical_8socket_3year' => array(
//			'local_id' => '874-006283',
//		),
//		'physical_8socket_5year' => array(
//			'local_id' => '874-006328',
//		),
		'virtual_2socket_1year' => array(
			'local_id' => '874-006906',
		),
		'virtual_2socket_3year' => array(
			'local_id' => '874-006908',
		),
		'virtual_2socket_5year' => array(
			'local_id' => '874-006910',
		),
//		'virtual_4socket_1year' => array(
//			'local_id' => '874-006293',
//		),
//		'virtual_4socket_3year' => array(
//			'local_id' => '874-006308',
//		),
//		'virtual_4socket_5year' => array(
//			'local_id' => '874-006338',
//		),
//		'virtual_8socket_1year' => array(
//			'local_id' => '874-006298',
//		),
//		'virtual_8socket_3year' => array(
//			'local_id' => '874-006313',
//		),
//		'virtual_8socket_5year' => array(
//			'local_id' => '874-006343',
//		),
	),
	'sles_systemz' => array(
		'enterprise_1_basic_1year' => array(
			'local_id' => '874-005009',
		),
		'enterprise_1_basic_3year' => array(
			'local_id' => '874-005011',
		),
//		'enterprise_1_basic_5year' => array(
//			'local_id' => 'not_done_yet',
//		),
		'enterprise_2_5_basic_1year' => array(
			'local_id' => '874-005715',
		),
		'enterprise_2_5_basic_3year' => array(
			'local_id' => '874-005716',
		),
		'enterprise_2_5_basic_5year' => array(
			'local_id' => '874-005717',
		),
		'enterprise_6_11_basic_1year' => array(
			'local_id' => '874-005718',
		),
		'enterprise_6_11_basic_3year' => array(
			'local_id' => '874-005719',
		),
		'enterprise_6_11_basic_5year' => array(
			'local_id' => '874-005720',
		),
		'enterprise_12_25_basic_1year' => array(
			'local_id' => '874-005721',
		),
		'enterprise_12_25_basic_3year' => array(
			'local_id' => '874-005722',
		),
		'enterprise_12_25_basic_5year' => array(
			'local_id' => '874-005723',
		),
		'enterprise_26_53_basic_1year' => array(
			'local_id' => '874-005724',
		),
		'enterprise_26_53_basic_3year' => array(
			'local_id' => '874-005725',
		),
		'enterprise_26_53_basic_5year' => array(
			'local_id' => '874-005726',
		),
		'enterprise_54plus_basic_1year' => array(
			'local_id' => '874-005727',
		),
		'enterprise_54plus_basic_3year' => array(
			'local_id' => '874-005728',
		),
		'enterprise_54plus_basic_5year' => array(
			'local_id' => '874-005729',
		),
		'enterprise_1_standard_1year' => array(
			'local_id' => '874-005050',
		),
		'enterprise_1_standard_3year' => array(
			'local_id' => '874-005053',
		),
//		'enterprise_1_standard_5year' => array(
//			'local_id' => 'not_done_yet',
//		),
		'enterprise_2_5_standard_1year' => array(
			'local_id' => '874-005730',
		),
		'enterprise_2_5_standard_3year' => array(
			'local_id' => '874-005731',
		),
		'enterprise_2_5_standard_5year' => array(
			'local_id' => '874-005732',
		),
		'enterprise_6_11_standard_1year' => array(
			'local_id' => '874-005733',
		),
		'enterprise_6_11_standard_3year' => array(
			'local_id' => '874-005734',
		),
		'enterprise_6_11_standard_5year' => array(
			'local_id' => '874-005735',
		),
		'enterprise_12_25_standard_1year' => array(
			'local_id' => '874-005736',
		),
		'enterprise_12_25_standard_3year' => array(
			'local_id' => '874-005737',
		),
		'enterprise_12_25_standard_5year' => array(
			'local_id' => '874-005738',
		),
		'enterprise_26_53_standard_1year' => array(
			'local_id' => '874-005739',
		),
		'enterprise_26_53_standard_3year' => array(
			'local_id' => '874-005740',
		),
		'enterprise_26_53_standard_5year' => array(
			'local_id' => '874-005741',
		),
		'enterprise_54plus_standard_1year' => array(
			'local_id' => '874-005742',
		),
		'enterprise_54plus_standard_3year' => array(
			'local_id' => '874-005743',
		),
		'enterprise_54plus_standard_5year' => array(
			'local_id' => '874-005744',
		),
		'enterprise_1_priority_1year' => array(
			'local_id' => '874-005051',
		),
		'enterprise_1_priority_3year' => array(
			'local_id' => '874-005054',
		),
//		'enterprise_1_priority_5year' => array(
//			'local_id' => 'not_done_yet',
//		),
		'enterprise_2_5_priority_1year' => array(
			'local_id' => '874-005745',
		),
		'enterprise_2_5_priority_3year' => array(
			'local_id' => '874-005746',
		),
		'enterprise_2_5_priority_5year' => array(
			'local_id' => '874-005747',
		),
		'enterprise_6_11_priority_1year' => array(
			'local_id' => '874-005748',
		),
		'enterprise_6_11_priority_3year' => array(
			'local_id' => '874-005749',
		),
		'enterprise_6_11_priority_5year' => array(
			'local_id' => '874-005750',
		),
		'enterprise_12_25_priority_1year' => array(
			'local_id' => '874-005751',
		),
		'enterprise_12_25_priority_3year' => array(
			'local_id' => '874-005752',
		),
		'enterprise_12_25_priority_5year' => array(
			'local_id' => '874-005753',
		),
		'enterprise_26_53_priority_1year' => array(
			'local_id' => '874-005754',
		),
		'enterprise_26_53_priority_3year' => array(
			'local_id' => '874-005755',
		),
		'enterprise_26_53_priority_5year' => array(
			'local_id' => '874-005756',
		),
		'enterprise_54plus_priority_1year' => array(
			'local_id' => '874-005757',
		),
		'enterprise_54plus_priority_3year' => array(
			'local_id' => '874-005758',
		),
		'enterprise_54plus_priority_5year' => array(
			'local_id' => '874-005759',
		),
		'business_1_basic_1year' => array(
			'local_id' => '874-006661',
		),
		'business_1_basic_3year' => array(
			'local_id' => '874-006662',
		),
		'business_1_basic_5year' => array(
			'local_id' => '874-006663',
		),
		'business_2_5_basic_1year' => array(
			'local_id' => '874-006664',
		),
		'business_2_5_basic_3year' => array(
			'local_id' => '874-006665',
		),
		'business_2_5_basic_5year' => array(
			'local_id' => '874-006666',
		),
		'business_6_11_basic_1year' => array(
			'local_id' => '874-006667',
		),
		'business_6_11_basic_3year' => array(
			'local_id' => '874-006668',
		),
		'business_6_11_basic_5year' => array(
			'local_id' => '874-006669',
		),
		'business_12plus_basic_1year' => array(
			'local_id' => '874-006670',
		),
		'business_12plus_basic_3year' => array(
			'local_id' => '874-006671',
		),
		'business_12plus_basic_5year' => array(
			'local_id' => '874-006672',
		),
		'business_1_standard_1year' => array(
			'local_id' => '874-006673',
		),
		'business_1_standard_3year' => array(
			'local_id' => '874-006674',
		),
		'business_1_standard_5year' => array(
			'local_id' => '874-006675',
		),
		'business_2_5_standard_1year' => array(
			'local_id' => '874-006676',
		),
		'business_2_5_standard_3year' => array(
			'local_id' => '874-006677',
		),
		'business_2_5_standard_5year' => array(
			'local_id' => '874-006678',
		),
		'business_6_11_standard_1year' => array(
			'local_id' => '874-006679',
		),
		'business_6_11_standard_3year' => array(
			'local_id' => '874-006680',
		),
		'business_6_11_standard_5year' => array(
			'local_id' => '874-006681',
		),
		'business_12plus_standard_1year' => array(
			'local_id' => '874-006682',
		),
		'business_12plus_standard_3year' => array(
			'local_id' => '874-006683',
		),
		'business_12plus_standard_5year' => array(
			'local_id' => '874-006684',
		),
		'business_1_priority_1year' => array(
			'local_id' => '874-006685',
		),
		'business_1_priority_3year' => array(
			'local_id' => '874-006686',
		),
		'business_1_priority_5year' => array(
			'local_id' => '874-006687',
		),
		'business_2_5_priority_1year' => array(
			'local_id' => '874-006688',
		),
		'business_2_5_priority_3year' => array(
			'local_id' => '874-006689',
		),
		'business_2_5_priority_5year' => array(
			'local_id' => '874-006690',
		),
		'business_6_11_priority_1year' => array(
			'local_id' => '874-006691',
		),
		'business_6_11_priority_3year' => array(
			'local_id' => '874-006692',
		),
		'business_6_11_priority_5year' => array(
			'local_id' => '874-006693',
		),
		'business_12plus_priority_1year' => array(
			'local_id' => '874-006694',
		),
		'business_12plus_priority_3year' => array(
			'local_id' => '874-006695',
		),
		'business_12plus_priority_5year' => array(
			'local_id' => '874-006696',
		),
	),
	'sles_virtual_driver_pack' => array(
		'up_to_4_1year' => array(
			'local_id' => '874-005213',
		),
		'up_to_4_3year' => array(
			'local_id' => '874-005214',
		),
		'unlimited_1year' => array(
			'local_id' => '874-005215',
		),
		'unlimited_3year' => array(
			'local_id' => '874-005216',
		),
	),
	'suse_cloud' => array(
		'control_node_plus_administration_server' => array(
			'local_id' => '874-006385',
		),
		'additional_control_node' => array(
			'local_id' => '874-006387',
		),
		'compute_node' => array(
			'local_id' => '874-006389',
		),
		'compute_node_microsoft_hyper_v' => array(
			'local_id' => '874-006698',
		),
		'storage_node' => array(
			'local_id' => '874-007099',
		),
	),
	'suse_management_pack' => array(
		'1year' => array(
			'product_id' => '290866200',
			'emea_product_id' => '290872700',
		),
	),
];