<?php

return array(

	/**
     * Default locale: this will be the default for your application. 
     * Is to be supposed that all strings are written in this language.
     */
    'locale' => 'en_US',
    /**
     * Supported locales: An array containing all allowed languages
     */
    'supported-locales' => array(
        'en_US',
        'es_ES',
        'de_DE',
    ),
    /**
     * Default charset encoding.
     */
    'encoding' => 'UTF-8',
    /**
     * -----------------------------------------------------------------------
     * All standard configuration ends here. The following values 
     * are only for special cases.
     * -----------------------------------------------------------------------
     * */
    /**
     * Base translation directory path (don't use trailing slash)
     */
    'translations-path' => '../resources/lang',
    /**
     * Fallback locale: When default locale is not available
     */
    'fallback-locale' => 'en_US',
    /**
     * Default domain used for translations: It is the file name for .po and .mo files
     */
    'domain' => 'messages',
    /**
     * Project name: is used on .po header files 
     */
    'project' => 'MultilanguageLaravelApplication',
    /**
     * Translator contact data (used on .po headers too)
     */
    'translator' => 'James Translator <james@translations.colm>',
    /**
     * Paths where PoEdit will search recursively for strings to translate. 
     * All paths are relative to app/ (don't use trailing slash).
     *
     * Remember to call artisan gettext:update after change this.
     */
//  'source-paths' => array(
//      'Http/Controllers',
//      '../resources/views',
//      'Commands'
//  ),


    /**
     * Multi-domain directory paths. If you want the translations in 
     * different files, just wrap your paths into a domain name. 
     * Paths on top-level will be associated to the default domain file, 
     * for example:
     */
    'source-paths' => array(
        'suse' => array(
            '../suse/public_html/views/content/en',
        ),
        'susecon' => array(
            '../susecon/public_html/views/content/en',
        ),
    ),
    /**
     * Sync laravel: A flag that determines if the laravel built-in locale must 
     * be changed when you call LaravelGettext::setLocale.
     */
    'sync-laravel' => true,


    /**
     * xgettext configuration
     */
    'xgettext'         => array(
        /**
         * In case your xgettext binary/executable is not called 'xgettext', you can change it here.
         * The default value is [xgettext]
         */
        'binary'           => "xgettext",
        /**
         * In case the path to your xgettext binary is not in your PATH, you can use the following
         * config option to manually set it. This path will then be added to the xgettext command.
         * If you leave this empty, it will be assumed that the xgettext binary is in the PATH of the OS user executing the command
         * Example:  /usr/bin
         *
         * Please do NOT add a trailing slash
         * The default value is []
         */
        'binary_path'      => "",
        /**
         * sets the script language for the xgettext command
         * you should never have to change this option
         * the default is [PHP]
         */
        'language'         => 'PHP',
        /**
         * determines which docbloc comments are included as a remarks for translators
         * the default is [TRANSLATORS]
         */
        'comments'         => 'TRANSLATORS',
        /**
         * force creation of .po(t) file, even if no strings were found
         * the default is [true]
         */
        'force_po'         => true,
        /**
         * input folder where php files to be scanned are located
         * the path is relative to the /app folder and should not contain any leading or trailing slashes
         * the default is [storage/l4gettext]
         */
        'input_folder'     => 'storage/l4gettext',
        /**
         * provide any additional input folders that you would like to have scanned and processed
         * the main 'input_folder' directove is commonly used for your (compiled) templates folder containing
         * the majority of your translation strings, but you can use this config option to set
         * additional folders, for example one containing your error messages embedded in your class file.
         *
         * this directive looks for all .php files and the folder is relative to the applications' root [/]
         * folder, using laravel's base_path() function. Files in these directories will NOT be compiled
         * before extracting the the language strings
         *
         * the default is an an empty [array()]
         */
        'additional_input_folders' => array(),
        /**
         * output folder where the po(t) file will be stored
         * this path is relative to the /app/storage folder and should not contain leading or trailing slashes
         * the default is [l4gettext]
         */
        'output_folder'    => 'laravel-gettext',
        /**
         * set the encoding of the files
         * if left empty, ASCII will be used
         * the default is [utf8]
         */
        'from_code'        => 'utf8',
        /**
         * copright holder
         * this will be added to the .po(t) file to ensure your texts remain your property
         */
        'copyright_holder' => 'laravel-gettext',
        /**
         * the package name that will be included in the .po(t) file
         */
        'package_name'     => 'laravel-gettext',
        /**
         * the package version that will be included in the .po(t) file
         */
        'package_version'  => 'v1.0.0',
        /**
         * the email address that will be included in the .po(t) file
         */
        'email_address'    => 'devinlewis@gmail.com',
        /**
         * specify the keywords used to scan the source files for strings
         * you can add your own shorthand/alternative keywords here which will then be scanned by xgettext
         * make sure the corresponding php function exists when you do
         * the defaults are [_, gettext, dgettext:2, dcgettext:2, ngettext:1,2, dngettext:2,3, dcngettext:2,3, _n:1,2]
         */
        'keywords'         => array(
            '_', // shorthand for gettext
            'gettext', // the default php gettext function
            'dgettext:2', // accepts plurals, uses the second argument passed to dgettext as a translation string
            'dcgettext:2', // accepts plurals, uses the second argument passed to dcgettext as a translation string
            'ngettext:1,2', // accepts plurals, uses the first and second argument passed to ngettext as a translation string
            'dngettext:2,3', // accepts plurals, used the second and third argument passed to dngettext as a translation string
            'dcngettext:2,3', // accepts plurals, used the second and third argument passed to dcngettext as a translation string
            '_n:1,2', // a custom l4gettext shorthand for ngettext (supports plurals)
        ),
    ),

);
