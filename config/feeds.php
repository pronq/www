<?php
return [
	'app_delivery_testing_feed' => 'https://blog.microfocus.com/feed/rss/', // need access
	'business_continuity_feed' => 'https://www.netiq.com/communities/cool-solutions/category/business-continuity/feed/',
	'collaboration_feed' => 'https://www.novell.com/communities/coolsolutions/category/collaboration_news/feed/', // has images
	'cobol_feed' => 'http://community.microfocus.com/microfocus/cobol/b/weblog/rss', //http://community.microfocus.com/microfocus/cobol/rss.aspx', // need access
	'data_center_management_feed' => 'https://www.netiq.com/communities/cool-solutions/category/data-center-management-2/feed/', // has images
	'endpoint_management_feed' => 'https://www.novell.com/communities/coolsolutions/category/endpoint_management_news/feed/', // has images
	'file_network_services_feed' => 'https://www.novell.com/communities/coolsolutions/category/filenetworkingservices_news/feed/',  // has images
	'identity_access_management_feed' => 'https://www.netiq.com/communities/cool-solutions/category/identity-access-management-2/feed/', // has images
	'itom_feed' => 'https://www.netiq.com/communities/cool-solutions/category/it-operations-management/feed/', // has images
	'security_management_feed' => 'https://www.netiq.com/communities/cool-solutions/category/security-management/feed/', // has images
	'terminal_emulation_feed' => 'http://feeds.feedburner.com/LegacyModernization', // need access - might need a workaround to get images in
	'workload_migration_disaster_rec_feed' => 'https://www.netiq.com/communities/cool-solutions/category/workload-migration-disaster-recovery/feed/' // has images
];