<?php

/* 
 * paths that should never get cleaned by \App\Http\Middleware\CleanUrls
 * third party apps should not every make it here but adding them as well.
 * paths must be formatted as a regex for urls w/o domain ie:
 * https://www.microfocus.com/media => /^\/media/
 */

return [
	'paths' => array(
		'/^\/media/',
		'/^\/ondemand/',
		'/^\/lifecycle/',
		'/^\/TrainingReg/',
		'/^\/ttp-member/',
		'/^\/demandbase-data/',
		'/\/e.aspx/',
		'/^\/common/',
		'/^\/require-include/',
	),
];
