<?php

namespace App\Http\Controllers;

use Config;
use Request;
use Log;
use Url;

class BaseController extends Controller {
	/*
	  |--------------------------------------------------------------------------
	  | Base Controller
	  |--------------------------------------------------------------------------
	 */

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function home() {
		return view('content::home');
	}

	/*
	 * This function is hit if no other routes match
	 * The purpose of this function is to serve static views
	 * depending on language - if no file is found 404 is returned
	 */

	public function catchAll() {
		$request_path = Request::path();
		if ( Url::onValidLangPath() ) {
			$view_path = "content::" . $request_path;
			$raw_english_path = substr($request_path, 3); // this works because the path will always start with {2_digit_lang}/
		} else {
			$view_path = "content::" . config('lang.default') . "/" . $request_path;
			$raw_english_path = $request_path;
		}
		$en_view_path = "content::" . config('lang.default') . "/" . $raw_english_path;
		$index_view_path = trim($view_path, '/') . "/index";
		$en_index_view_path = trim($en_view_path, '/') . "/index";

		$canonical_view_path = url($request_path, array(), true);
		view()->share('canonical_view_path', $canonical_view_path); // used for the canonical tag

//		Log::info($index_view_path);
//		Log::info($view_path);
//		Log::info($en_index_view_path);
//		Log::info($en_view_path);

		if ( view()->exists($index_view_path) ) {
			return view($index_view_path);
		} else if ( view()->exists($view_path) ) {
			return view($view_path);
		} else if ( view()->exists($en_index_view_path) ) {
			return view($en_index_view_path);
		} else if ( view()->exists($en_view_path) ) {
			return view($en_view_path);
		}



		abort(404);
	}

}
