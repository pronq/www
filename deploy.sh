
if [ -n "$(grep "dev.microfocus.com" /etc/hosts)" ]
    then
    echo "Not eidting hosts file";

else

    echo "Adding dev.microfocus.com to your hosts file";
    
    sudo -- sh -c "echo '127.0.0.1       dev.microfocus.com' >> /etc/hosts"
fi

echo Plese enter your Micro Focus username:
read user

cd resources
touch .dev.env
echo "DEMANDBASE_KEY=e63182b62ad10daabfe274587a49c8543d0be627" >> ./.dev.env
echo "SITE_TEST=microfocus_dev" >> ./.dev.env
echo "APP_DEBUG=true" >> ./.dev.env
echo "GLOBAL_TEST=dev" >> ./.dev.env
echo "DB_HOST=localhost" >> ./.dev.env
echo "DB_DATABASE=homestead" >> ./.dev.env
echo "DB_USERNAME=homestead" >> ./.dev.env
echo "DB_PASSWORD=secret" >> ./.dev.env
echo "CACHE_DRIVER=file" >> ./.dev.env
echo "SESSION_DRIVER=file" >> ./.dev.env
echo "QUEUE_DRIVER=sync" >> ./.dev.env
echo "MAIL_DRIVER=smtp" >> ./.dev.env
echo "MAIL_HOST=mailtrap.io" >> ./.dev.env
echo "MAIL_PORT=2525" >> ./.dev.env
echo "MAIL_USERNAME=null" >> ./.dev.env
echo "MAIL_PASSWORD=null" >> ./.dev.env
echo "# form builder" >> ./.dev.env
echo "FORMBUILDER_PATH=http://intrafillstage.provo.novell.com/form_builder/index.php" >> ./.dev.env
echo "[rsync options]" >> ./.dev.env
echo RSYNC_USERNAME=$user >> ./.dev.env
echo "RSYNC_SERVER=prvwwwt01.provo.novell.com" >> ./.dev.env
echo "RSYNC_LOCAL_PATH=\"/var/www/laravel\"" >> ./.dev.env
echo "USE_WINSCIP=true" >> ./.dev.env
echo "WINSCP_SESSION=prvwwwt01" >> ./.dev.env

cd ..

docker build --tag microfocus/localenv:2 .
docker run --name microfocus -p 80:80 -p 443:443 -it -d -v $(pwd):/var/www/laravel microfocus/localenv:2

echo "YOUR LOCAL IS NOW BUILT"
echo "We are now changing settings on your local server"
echo "This could take a while!"
docker exec -it microfocus bash -c "

cd /var/www/laravel &&
php teamsite_rsync_pull.php --branch public --exclude='docrep, brandcentral, documentation, media, security' &&
php teamsite_rsync_pull.php --branch resources --exclude='docrep, brandcentral, documentation, media, security' &&

cd / &&

apt-get install php-mbstring -y

chgrp -R www-data /var/www/laravel/public &&
chmod -R 775 /var/www/laravel &&



cd /etc/apache2/sites-available &&

touch laravel.conf &&
echo '<VirtualHost *:80>'  >> /etc/apache2/sites-available/laravel.conf && 
echo '  ServerAdmin webmaster@localhost'  >> /etc/apache2/sites-available/laravel.conf && 
echo '  DocumentRoot /var/www/laravel/public'  >> /etc/apache2/sites-available/laravel.conf && 
echo '  <Directory />'  >> /etc/apache2/sites-available/laravel.conf && 
echo '    #AllowOverride All'  >> /etc/apache2/sites-available/laravel.conf && 
echo '    Options +Indexes +FollowSymLinks +MultiViews'  >> /etc/apache2/sites-available/laravel.conf && 
echo '    Require all granted'  >> /etc/apache2/sites-available/laravel.conf && 
echo '    Order allow,deny'  >> /etc/apache2/sites-available/laravel.conf && 
echo '    Allow from all'  >> /etc/apache2/sites-available/laravel.conf && 
echo '    AllowOverride None'  >> /etc/apache2/sites-available/laravel.conf && 
echo '    <IfModule mod_negotiation.c>'  >> /etc/apache2/sites-available/laravel.conf && 
echo '          Options -MultiViews'  >> /etc/apache2/sites-available/laravel.conf && 
echo '    </IfModule>'  >> /etc/apache2/sites-available/laravel.conf && 
echo '    RewriteEngine On'  >> /etc/apache2/sites-available/laravel.conf && 
echo '    RewriteCond %{REQUEST_FILENAME} !-f'  >> /etc/apache2/sites-available/laravel.conf && 
echo '    RewriteRule ^ index.php [L]'  >> /etc/apache2/sites-available/laravel.conf && 
echo '  </Directory>'  >> /etc/apache2/sites-available/laravel.conf && 
echo '  <Directory /var/www/laravel/public>'  >> /etc/apache2/sites-available/laravel.conf && 
echo '    Options Indexes FollowSymLinks MultiViews'  >> /etc/apache2/sites-available/laravel.conf && 
echo '    AllowOverride All'  >> /etc/apache2/sites-available/laravel.conf && 
echo '    Order allow,deny'  >> /etc/apache2/sites-available/laravel.conf && 
echo '    allow from all'  >> /etc/apache2/sites-available/laravel.conf && 
echo '  </Directory>'  >> /etc/apache2/sites-available/laravel.conf && 
echo '  ErrorLog ${APACHE_LOG_DIR}/error.log'  >> /etc/apache2/sites-available/laravel.conf && 
echo '  CustomLog ${APACHE_LOG_DIR}/access.log combined'  >> /etc/apache2/sites-available/laravel.conf && 
echo '</VirtualHost>'  >> /etc/apache2/sites-available/laravel.conf && 

a2dissite 000-default.conf &&
a2ensite laravel.conf &&
a2enmod rewrite &&
service apache2 restart"

echo 'GO to dev.microfocus.com and you should see the local enviornmnet running.'
#change .htaccess file
#change hosts file