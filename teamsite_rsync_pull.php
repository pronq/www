<?php
// turning off annoying notices
error_reporting(E_ERROR | E_WARNING | E_PARSE );

$version = 0.1;
$title = "Teamsite rsync pull script";

// Uncomment these variables to hard code options
// $username = 'jwalter';
// $server = 'prvwwwt01';
// $local_path_to_laravel_top_level = 'c:\Users\Walt\Desktop';
// $use_winscp = true;
// $winscp_session = 'prvwwwt01';


$is_win == false;
if (strpos(strtolower(PHP_OS), 'winnt') !== false) {
	$is_win = true;
}

$server_path_to_laravel_top_level = '/inet/laravel_microfocus';

$env_file = __DIR__ . "/resources/.dev.env";

if (file_exists($env_file)) {
	$env_args = parse_ini_file($env_file);
}

// get options from .dev.env file
if (isset($env_args['RSYNC_USERNAME']) && !empty($env_args['RSYNC_USERNAME'])) $username = $env_args['RSYNC_USERNAME'];
if (isset($env_args['RSYNC_SERVER']) && !empty($env_args['RSYNC_SERVER'])) $server = $env_args['RSYNC_SERVER'];
if (isset($env_args['RSYNC_LOCAL_PATH']) && !empty($env_args['RSYNC_LOCAL_PATH'])) $local_path_to_laravel_top_level = $env_args['RSYNC_LOCAL_PATH'];
if (isset($env_args['USE_WINSCP']) && !empty($env_args['USE_WINSCP'])) $use_winscp = $env_args['USE_WINSCP'];
if (isset($env_args['WINSCP_SESSION']) && !empty($env_args['WINSCP_SESSION'])) $winscp_session = $env_args['WINSCP_SESSION'];


// set options
$short_opts = 'hu:v';
$long_opts = [
"dry-run",
"branch:",
"option",
"exclude:",
"help",
"user:",
"server:",
"local-dir:",
"version",
"delete-excluded",
"path:",
];

$exclude = [
'less',
'.dev.env*',
'.gitignore',
];


// get arguments from command line
$args = getopt($short_opts, $long_opts);

// show help information
if (isset($args['help']) || isset($args['h'])) {
	usage($title);
	exit();
}

if (isset($args['version']) || isset($args['v'])) {
	echo "$title\n";
	echo "version: $version\n";
	exit();
}

// setting branch
if (!isset($args['branch']) || empty($args['branch']) || ($args['branch'] != 'public' && $args['branch'] != 'resources')) {
	echo "ERROR: branch must be public or resources\n";
	echo "php teamsite_rsync_pull.php --help\n";
	exit();
}
else {
	$branch = $args['branch'];
}

// check for --dry-run
$dry_run = false;
if (isset($args["dry-run"])) {
	$dry_run = true;
}

if (isset($args['user']))  $username = $args['user'];
if (isset($args['u']))  $username = $args['u'];
if (isset($args["server"])) $server = $args['server'];
if (isset($args['local-dir']) && !empty($args['local-dir'])) $local_path_to_laravel_top_level = $args['local-dir'];

$path_str = "";
$win_local_path_str = "";
$linux_local_path_str = "";
if (isset($args['path'])) {
	$path_str = $args['path'].'/';
	$win_local_path_str = '\\'.str_replace("/","\\",$args['path']);
	$linux_local_path_str = '/'.$args['path'];
}

// $winmod = '';
// if ($is_win) $winmod = " --size-only --chmod=ugo=rwX ";
if ($is_win == false) {
	if (isset($args['exclude'])) {
		$exclude = format_arg($args['exclude'], $exclude);
	}
	$exclude_string = '';
	foreach ($exclude as $ex) {
		$exclude_string .= " --exclude '".$ex."'";
	}
	$delete_string = (isset($args['delete-excluded']) ? '--delete-excluded' : '');
	$command = "rsync -auvP $exclude_string $delete_string $username@$server:$server_path_to_laravel_top_level/".strtolower($branch)."/$path_str $local_path_to_laravel_top_level/$branch".$linux_local_path_str;
	if ($dry_run) {
		$command .= " --dry-run";
	}

	echo "Running command: $command\n\n";

	system($command);
}
else if ($is_win && $use_winscp == true) {
	if (isset($args['exclude'])) {
		$exclude = format_arg($args['exclude'], $exclude);
	}
	$exclude_string = implode(',',$exclude);

	$delete_string = (isset($args['delete-excluded']) ? '-delete' : '');

// create a script file for winscp and write commands to it

	$command = "synchronize local $local_path_to_laravel_top_level\\$branch".$win_local_path_str." $server_path_to_laravel_top_level/".strtolower($branch)."/$path_str -filemask=*/|$exclude_string $delete_string";
	if ($dry_run) {
		$command .= " -preview";
	}

	echo "Running command: $command\n\n";

	$script_file = 'winscp_script.txt';
	$fh = fopen('winscp_script.txt', 'c');
	fwrite($fh, "open $winscp_session\n\r");
	fwrite($fh, "$command\n\r");
	fwrite($fh, "exit\n\r");
	fclose($fh);

	passthru("winscp /script=winscp_script.txt");
	unlink($script_file);

}
else if ($is_win && $use_winscp == false) {
	echo "You must be using WinSCP to run this script on Windows";
	exit();
}

/* functions */

// usage for help and h arguments
function usage($title) {
	$mask = "%4.4s%-40s%s";
	echo ("$title\n");
	echo ("This is a php cli script that will rsync the teamsite files to your local laravel development environment.\n");
	echo ("This is only an rsync pull that is run with these switches: -auvP\n");
	echo ("See 'man rsync' for more information\n");
	echo ("command options:\n");
	printf($mask, " ", "--branch='public', --branch=resources", "teamsite branch is a required parameter to specify which directory to sync\n");
	printf($mask, " ", "--path='views'", "syncs down only the specified path\n");
	printf($mask, " ", "--exclude='translations views'", "files or directories to exclude from rsync\n");
	printf($mask, " ", "--server", "server to rsync files from\n");
	printf($mask, " ", "--local-dir", "local directory to sync files to\n");
	printf($mask, " ", "--delete-excluded", "delete local files excluded from the remote pull (files that have been deleted from the remote source).\n");
	printf($mask, " ", "--dry-run", "do a dry-run of the rsync transfer to show affected files\n");
	printf($mask, " ", "-u, --user", "username to authenticate to server\n");
	printf($mask,  " ", '-h, --help', "show this help\n");
	printf($mask,  " ", '-v, --version', "show version\n");
}

// split the argument and add elements to the array
function format_arg($arg, $arr) {
	$ret_string = '';
	$tmp_arr = preg_split("/(\s|,)/", $arg);
	foreach($tmp_arr as $a) {
		array_push($arr, $a);
	}
	return $arr;
}


// disable output buffering for real time console output
// doesn't work on all systems
function disable_ob() {
// Turn off output buffering
	ini_set('output_buffering', 'off');

// Turn off PHP output compuression
	ini_set('zlib.output_compression', false);

// Implicitly flush the buffers
	ini_set('implicit_flush', true);
	ob_implicit_flush(true);

// Clear, and turn off output buffering
	while (ob_get_level() > 0) {
// Get the current level
		$level = ob_get_level();

// End the buffering
		ob_end_clean();

// If the current level has not changed, abort
		if (ob_get_level() == $level) break;

	}
	if (function_exists('apache_setenv')) {
		apache_setenv('no-gzip', 1);
		apache_setenv('dont-vary', 1);
	}
}
